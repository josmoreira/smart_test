function($scope,$rootScope,$http) {
  /* widget controller */
  var c = this;

	//event listeners
	$rootScope.$on('sendDataStructure', function(event,data) {
		$scope.data = data[0];
		$scope.currentMacroIndex = data[1];
		$scope.currentCatIndex = data[2];
		$scope.currentSubIndex = data[3];
  });

	$rootScope.$on('incrementMacro', function(event, data) {
		$scope.currentMacroIndex++;
	});

	$rootScope.$on('incrementCat', function(event, data) {
		$scope.currentCatIndex++;
	});

	$rootScope.$on('incrementSub', function(event, data) {
		$scope.currentSubIndex++;
	});

	$rootScope.$on('decrementMacro', function(event, data) {
		$scope.currentMacroIndex--;
	});

	$rootScope.$on('decrementCat', function(event, data) {
		$scope.currentCatIndex--;
	});

	$rootScope.$on('decrementSub', function(event, data) {
		$scope.currentSubIndex--;
	});

	$rootScope.$on('resetCat', function(event, data) {
		$scope.currentCatIndex = 0;
	});

	$rootScope.$on('resetSub', function(event, data) {
		$scope.currentSubIndex = 0;
	});

	$rootScope.$on('getLastCat', function(event, data) {
		$scope.currentCatIndex = $scope.data.competencies[$scope.currentMacroIndex].cats.length-1;
	});

	$rootScope.$on('getLastSub', function(event, data) {
		if ($scope.data.competencies[$scope.currentMacroIndex].numOfHierarchies === 3) {
			$scope.currentSubIndex = $scope.data.competencies[$scope.currentMacroIndex].cats[$scope.currentCatIndex].subs.length-1;
		} else { //numOfHierarchies === 2
			$scope.currentSubIndex = $scope.data.competencies[$scope.currentMacroIndex].subs.length-1;
		}
	});

}

//# sourceURL=onboardingside.js
