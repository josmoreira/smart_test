(function() {

  data.instanceURL = gs.getProperty('glide.servlet.uri');
  data.currentUserID = gs.getUserID();
  data.currentUsername = gs.getUserName();
  data.currentFullName = gs.getUserDisplayName();
  data.competenciesNames = [];
  data.competenciesObjects = {};
  data.projects = [];
  data.careers = {};
  data.projDict = {};
  data.requestMaster = {};
  data.statusChoices = {};

  data.filterOptionsProjs = {};

  var gr_proj_possible_req_status = new GlideRecord('u_conf_scheduling_homepage_filters');
  gr_proj_possible_req_status.addEncodedQuery('u_active!=0');
  gr_proj_possible_req_status.orderBy('u_order');
  gr_proj_possible_req_status.query();
  while (gr_proj_possible_req_status.next()) {
    var proj_status_val = gr_proj_possible_req_status.u_project_value.getDisplayValue();
    if (!data.filterOptionsProjs[proj_status_val]) {
      data.filterOptionsProjs[proj_status_val] = {
        value: proj_status_val,
        label: gr_proj_possible_req_status.u_project_label.getDisplayValue(),
        requestFilters: []
      };
    }
    data.filterOptionsProjs[proj_status_val].requestFilters.push({
      value: gr_proj_possible_req_status.u_request_value.getDisplayValue(),
      label: gr_proj_possible_req_status.u_request_label.getDisplayValue(),
      order: gr_proj_possible_req_status.u_order.getDisplayValue()
    });
  }

  // Get careers, professional categories and employee type
  var gr_career = new GlideRecord('u_lov_professional_category');
  gr_career.addActiveQuery();
  gr_career.orderBy('u_order');
  gr_career.query();
  while (gr_career.next()) {

    var career = gr_career.u_career.getDisplayValue();
    var employee_type = gr_career.u_employee_type.getDisplayValue();

    var professional_category = {
      label: gr_career.u_professional_category.getDisplayValue(),
      sys_id: gr_career.getUniqueValue()
    };

    data.careers[career] = data.careers[career] || {};
    data.careers[career][employee_type] = data.careers[career][employee_type] || [];


    data.careers[career][employee_type].push(professional_category);
  }

  var gr_competencies = new GlideRecord('u_competency');
  gr_competencies.addActiveQuery();
  gr_competencies.orderBy('u_competency_macro_type');
  gr_competencies.query();

  while (gr_competencies.next()) {
    if (gr_competencies.u_competency_macro_type.getDisplayValue() === 'Country preferences') {
      continue;
    }
    var compName = gr_competencies.name.getDisplayValue();
    data.competenciesNames.push(compName);

    data.competenciesObjects[compName] = {
      cat: gr_competencies.u_competency_macro_type.getDisplayValue(),
      sys_id: gr_competencies.getUniqueValue(),
      name: gr_competencies.name.getDisplayValue()
    };
  }

  //build the dictionary of label:value for the request status
  var gr = new GlideRecord('sc_req_item');
  var choiceList = j2js(gr.getElement('u_status').getChoices());
  console.log(choiceList);
  var cl = new ChoiceList('sc_req_item', 'u_status');
  req_status = {};
  data.req_status = [];
  for (var i = 0; i < choiceList.length; i++) {
    req_status[cl.getLabel(choiceList[i])] = choiceList[i];
  }

  var gr_projects = new GlideRecord('pm_project');
  gr_projects.addActiveQuery();
  gr_projects.addQuery('u_project_managers', 'CONTAINS', data.currentUserID);
  gr_projects.orderBy('sys_created_on');
  gr_projects.query();
  var projIndex = 0;

  while (gr_projects.next()) {
    var projectId = gr_projects.getUniqueValue();
    var projectObj = {
      sys_id: projectId,
      name: gr_projects.short_description.getDisplayValue(),
      description: gr_projects.description.getDisplayValue(),
      engagements: [],
      status: {
        label: gr_projects.u_project_status.getDisplayValue(),
        value: gr_projects.u_project_status.getRefRecord().u_project_status_value.getDisplayValue()
      },
      type: gr_projects.u_type.getDisplayValue(),
      outsourcing: gr_projects.u_allow_outsourcing.getDisplayValue(),
      managers: gr_projects.u_project_managers.split(','),
      geography: [],
      requests: [],
      mandatoryComps: {
        technology: [],
        business: [],
        languages: [],
        clientKnowledge: []
      },
      optionalComps: {
        technology: [],
        business: [],
        languages: [],
        clientKnowledge: []
      },
      startDate: gr_projects.u_start_date.getDisplayValue(),
      endDate: gr_projects.u_end_date.getDisplayValue()
    };

    var mandComps = gr_projects.u_mandatory_competencies.split(',');
    for (var m in mandComps) {
      var grComps = new GlideRecord('u_competency');
      grComps.addActiveQuery();
      grComps.addQuery('sys_id', mandComps[m]);
      grComps.query();
      while (grComps.next()) {
        var compName = grComps.name.getDisplayValue();
        var compCat = data.competenciesObjects[compName].cat;
        if (compCat == 'Technology') {
          projectObj.mandatoryComps.technology.push(data.competenciesObjects[compName]);
        } else if (compCat == 'Business') {
          projectObj.mandatoryComps.business.push(data.competenciesObjects[compName]);
        } else if (compCat == 'Languages') {
          projectObj.mandatoryComps.languages.push(data.competenciesObjects[compName]);
        } else if (compCat == 'Client Knowledge') {
          projectObj.mandatoryComps.clientKnowledge.push(data.competenciesObjects[compName]);
        }
      }
    }

    var optComps = gr_projects.u_optional_competencies.split(',');
    for (var m in optComps) {
      var grComps = new GlideRecord('u_competency');
      grComps.addActiveQuery();
      grComps.addQuery('sys_id', optComps[m]);
      grComps.query();
      while (grComps.next()) {
        var compName = grComps.name.getDisplayValue();
        var compCat = data.competenciesObjects[compName].cat;
        if (compCat == 'Technology') {
          projectObj.optionalComps.technology.push(data.competenciesObjects[compName]);
        } else if (compCat == 'Business') {
          projectObj.optionalComps.business.push(data.competenciesObjects[compName]);
        } else if (compCat == 'Languages') {
          projectObj.optionalComps.languages.push(data.competenciesObjects[compName]);
        } else if (compCat == 'Client Knowledge') {
          projectObj.optionalComps.clientKnowledge.push(data.competenciesObjects[compName]);
        }
      }
    }

    var geographiesIds = gr_projects.u_geography.split(',');
    for (var i in geographiesIds) {
      var gr_geographies = new GlideRecord('u_conf_countries_iso_3166');
      gr_geographies.addQuery('sys_id', geographiesIds[i]);
      gr_geographies.query();

      while (gr_geographies.next()) {
        var geoObj = {
          sys_id: gr_geographies.getUniqueValue(),
          location: gr_geographies.u_country_name.getDisplayValue()
        };

        if (gr_geographies.u_city_name.getDisplayValue()) {
          geoObj.location = geoObj.location + ' - ' + gr_geographies.u_city_name.getDisplayValue();
        }

        projectObj.geography.push(geoObj);
      }
    }

    var engagementsIds = gr_projects.u_engagements.split(',');
    for (var i in engagementsIds) {
      var gr_engagements = new GlideRecord('u_engagement');
      gr_engagements.addQuery('sys_id', engagementsIds[i]);
      gr_engagements.query();

      while (gr_engagements.next()) {
        var engObj = {
          sys_id: gr_engagements.getUniqueValue(),
          engNumber: gr_engagements.u_engagement_number.getDisplayValue() + ' - ' + gr_engagements.getDisplayValue()
        };
        projectObj.engagements.push(engObj);
      }
    }

    var gr_drafts = new GlideRecord('sc_request');
    gr_drafts.addQuery('u_next_project', projectId);
    gr_drafts.addQuery('u_smart_main_process', 'scheduling');
    gr_drafts.addQuery('sys_created_by', data.currentUsername);
    gr_drafts.addQuery('u_status', 'draft'); //'draft'
    gr_drafts.orderBy('sys_created_on');
    gr_drafts.query();
    while (gr_drafts.next()) {
      var requestId = gr_drafts.getUniqueValue();
      var draftObj = {
        sys_id: requestId,
        nextProjectId: gr_drafts.getValue('u_next_project'),
        numResources: gr_drafts.u_number_of_resources.getDisplayValue(),
        status: {
          label: gr_drafts.u_status.getDisplayValue(),
          value: req_status[gr_drafts.u_status.getDisplayValue()]
        },
        categoryList: [],
        categoryShow: '',
        role: gr_drafts.u_role.getDisplayValue(),
        responsibilities: gr_drafts.u_role_description.getDisplayValue(),
        st_date: gr_drafts.u_start_date.getDisplayValue(),
        end_date: gr_drafts.u_end_date.getDisplayValue(),
        locationsAdded: [],
        hoursPerWeek: parseInt(gr_drafts.u_hour_week.getDisplayValue()),
        mandatoryComps: {
          technology: [],
          business: [],
          languages: [],
          clientKnowledge: []
        },
        optionalComps: {
          technology: [],
          business: [],
          languages: [],
          clientKnowledge: []
        },
        requesterComments: gr_drafts.u_requester_comments.getDisplayValue() || '',
        daysLeft: '-'
      };

      var mandComps = gr_drafts.u_mandatory_competencies.split(',');
      for (var m in mandComps) {
        var grComps = new GlideRecord('u_competency');
        grComps.addActiveQuery();
        grComps.addQuery('sys_id', mandComps[m]);
        grComps.query();
        while (grComps.next()) {
          var compName = grComps.name.getDisplayValue();
          var compCat = data.competenciesObjects[compName].cat;
          if (compCat == 'Technology') {
            draftObj.mandatoryComps.technology.push(data.competenciesObjects[compName]);
          } else if (compCat == 'Business') {
            draftObj.mandatoryComps.business.push(data.competenciesObjects[compName]);
          } else if (compCat == 'Languages') {
            draftObj.mandatoryComps.languages.push(data.competenciesObjects[compName]);
          } else if (compCat == 'Client Knowledge') {
            draftObj.mandatoryComps.clientKnowledge.push(data.competenciesObjects[compName]);
          }
        }
      }

      var optComps = gr_drafts.u_optional_competencies.split(',');
      for (var m in optComps) {
        var grComps = new GlideRecord('u_competency');
        grComps.addActiveQuery();
        grComps.addQuery('sys_id', optComps[m]);
        grComps.query();
        while (grComps.next()) {
          var compName = grComps.name.getDisplayValue();
          var compCat = data.competenciesObjects[compName].cat;
          if (compCat == 'Technology') {
            draftObj.optionalComps.technology.push(data.competenciesObjects[compName]);
          } else if (compCat == 'Business') {
            draftObj.optionalComps.business.push(data.competenciesObjects[compName]);
          } else if (compCat == 'Languages') {
            draftObj.optionalComps.languages.push(data.competenciesObjects[compName]);
          } else if (compCat == 'Client Knowledge') {
            draftObj.optionalComps.clientKnowledge.push(data.competenciesObjects[compName]);
          }
        }
      }

      var engagement = gr_drafts.u_engagement.getDisplayValue();
      var grEng = new GlideRecord('u_engagement');
      grEng.addQuery('u_engagement_short_name', engagement);
      grEng.query();
      while (grEng.next()) {
        draftObj.engID = grEng.getUniqueValue();
        draftObj.engagement = grEng.u_engagement_number.getDisplayValue() + ' - ' + engagement;
      }

      var draftGeo = gr_drafts.u_geography.split(',');
      for (var i in draftGeo) {
        var grGeo = new GlideRecord('u_conf_countries_iso_3166');
        grGeo.addActiveQuery();
        grGeo.addQuery('sys_id', draftGeo[i]);
        grGeo.query();
        while (grGeo.next()) {
          var locationObj = {
            sys_id: draftGeo[i],
            location: grGeo.u_country_name.getDisplayValue()
          };
          if (grGeo.u_city_name.getDisplayValue()) {
            locationObj.location = locationObj.location + ' - ' + grGeo.u_city_name.getDisplayValue();
          }
          draftObj.locationsAdded.push(locationObj);
        }
      }

      //added
      var categories = gr_drafts.u_category_list.split(',');
      for (var j in categories) {
        var grCats = new GlideRecord('u_lov_professional_category');
        grCats.addActiveQuery();
        grCats.addQuery('sys_id', categories[j]);
        grCats.query();
        while (grCats.next()) {
          draftObj.categoryList.push(categories[j]);
          if (draftObj.categoryShow !== '') {
            draftObj.categoryShow += ', ';
          }
          draftObj.categoryShow += grCats.u_professional_category.getDisplayValue();
        }
      }

      data.requestMaster[requestId] = draftObj;
      projectObj.requests.push(requestId);
    }

    var gr_requests = new GlideRecord('sc_req_item');
    gr_requests.addActiveQuery();
    gr_requests.addEncodedQuery('u_next_project=' + projectId + '^ORu_current_project=' + projectId);
    gr_requests.addQuery('u_smart_main_process', 'scheduling');
    gr_requests.orderBy('sys_created_on');
    gr_requests.query();
    while (gr_requests.next()) {
      var requestId = gr_requests.getUniqueValue();
      var requestObj = {};
      if (!data.requestMaster.hasOwnProperty(requestId)) {

        requestObj = {
          sys_id: requestId,
          nextProjectId: gr_requests.getValue('u_next_project'),
          numResources: gr_requests.u_number_of_resources.getDisplayValue(),
          status: {
            label: gr_requests.u_status.getDisplayValue(),
            value: req_status[gr_requests.u_status.getDisplayValue()]
          },
          categoryList: [],
          categoryShow: '',
          role: gr_requests.u_role.getDisplayValue(),
          responsibilities: gr_requests.u_role_description.getDisplayValue() || '',
          st_date: gr_requests.u_start_date.getDisplayValue(),
          end_date: gr_requests.u_end_date.getDisplayValue(),
          new_end_date: gr_requests.u_new_end_date.getDisplayValue(),
          locationsAdded: [],
          hoursPerWeek: parseInt(gr_requests.u_hour_week.getDisplayValue()),
          mandatoryComps: {
            technology: [],
            business: [],
            languages: [],
            clientKnowledge: []
          },
          optionalComps: {
            technology: [],
            business: [],
            languages: [],
            clientKnowledge: []
          },
          proposedResources: [],
          allocatedResource: gr_requests.u_allocated_resource.getDisplayValue() || '',
          assignedTo: gr_requests.assigned_to.getDisplayValue(), //review
          requester: gr_requests.opened_by.getDisplayValue(),
          requestDate: gr_requests.opened_at.getDisplayValue(),
          requesterComments: gr_requests.u_requester_comments.getDisplayValue() || '',
          pcComments: gr_requests.u_scenario_comments.getDisplayValue() || '',
          approvalComments: gr_requests.u_approver_comments.getDisplayValue() || '',
          cancellationReason: gr_requests.u_cancellation_reason.getDisplayValue() || '',
          commOutComments: gr_requests.u_communication_out_comments.getDisplayValue() || '',
          dateCommOut: gr_requests.u_communication_out_date.getDisplayValue(),
          daysLeft: gr_requests.u_days_left_to_complete_task.getDisplayValue(),
          dependantRequestId: gr_requests.getValue('u_dependant_request') || undefined
        };

        if(gr_requests.u_resource_available.getDisplayValue() === 'false') {
          requestObj.hasDependantRequest = true;
        }

        if (gr_requests.getValue('parent')) {
          requestObj.parentRequestId = gr_requests.parent.toString();
          /*at this point, the parent request is already always on the
          requestMaster data structure due to the orderBy in the glide record*/
          if (data.requestMaster.hasOwnProperty(requestObj.parentRequestId)) {
            data.requestMaster[requestObj.parentRequestId].childRequestId = requestObj.sys_id;
          }
        }

        var mandComps = gr_requests.u_mandatory_competencies.split(',');
        for (var m in mandComps) {
          var grComps = new GlideRecord('u_competency');
          grComps.addActiveQuery();
          grComps.addQuery('sys_id', mandComps[m]);
          grComps.query();
          while (grComps.next()) {
            var compName = grComps.name.getDisplayValue();
            var compCat = data.competenciesObjects[compName].cat;
            if (compCat == 'Technology') {
              requestObj.mandatoryComps.technology.push(data.competenciesObjects[compName]);
            } else if (compCat == 'Business') {
              requestObj.mandatoryComps.business.push(data.competenciesObjects[compName]);
            } else if (compCat == 'Languages') {
              requestObj.mandatoryComps.languages.push(data.competenciesObjects[compName]);
            } else if (compCat == 'Client Knowledge') {
              requestObj.mandatoryComps.clientKnowledge.push(data.competenciesObjects[compName]);
            }
          }
        }

        var optComps = gr_requests.u_optional_competencies.split(',');
        for (var m in optComps) {
          var grComps = new GlideRecord('u_competency');
          grComps.addActiveQuery();
          grComps.addQuery('sys_id', optComps[m]);
          grComps.query();
          while (grComps.next()) {
            var compName = grComps.name.getDisplayValue();
            var compCat = data.competenciesObjects[compName].cat;
            if (compCat == 'Technology') {
              requestObj.optionalComps.technology.push(data.competenciesObjects[compName]);
            } else if (compCat == 'Business') {
              requestObj.optionalComps.business.push(data.competenciesObjects[compName]);
            } else if (compCat == 'Languages') {
              requestObj.optionalComps.languages.push(data.competenciesObjects[compName]);
            } else if (compCat == 'Client Knowledge') {
              requestObj.optionalComps.clientKnowledge.push(data.competenciesObjects[compName]);
            }
          }
        }

        var propResources = gr_requests.u_proposed_resource.split(',');
        for (var r in propResources) {
          if (propResources[r] === '') {
            continue;
          }
          var gr_user = new GlideRecord('sys_user');
          var res;
          if (gr_user.get(propResources[r])) {
            res = {
              mandComps: [],
              optComps: [],
              availability: [],
              name: gr_user.name.toString(),
              cat: gr_user.u_ref_professional_category.getDisplayValue(),
              sys_id: propResources[r]
            };
          }
          for (var m in mandComps) {
            var comp = {};
            var grUserComps = new GlideRecord('u_user_competencies');
            grUserComps.addQuery('u_collaborator', propResources[r]);
            grUserComps.addQuery('u_competency', mandComps[m]);
            grUserComps.query();
            if (grUserComps.next()) {
              comp.name = grUserComps.u_competency.getDisplayValue();
              comp.prof = grUserComps.u_proficiency_level.getDisplayValue();
              res.mandComps.push(comp);
            } else {
              var grUserComps = new GlideRecord('u_user_competencies');
              grUserComps.addQuery('u_competency', mandComps[m]);
              grUserComps.query();
              if (grUserComps.next()) {
                comp.name = grUserComps.u_competency.getDisplayValue();
                comp.prof = 'N/A';
                res.mandComps.push(comp);
              }
            }
          }
          for (var o in optComps) {
            var comp = {};
            var grUserComps = new GlideRecord('u_user_competencies');
            grUserComps.addQuery('u_collaborator', propResources[r]);
            grUserComps.addQuery('u_competency', optComps[o]);
            grUserComps.query();
            if (grUserComps.next()) {
              comp.name = grUserComps.u_competency.getDisplayValue();
              comp.prof = grUserComps.u_proficiency_level.getDisplayValue();
              res.optComps.push(comp);
            } else {
              var grUserComps = new GlideRecord('u_user_competencies');
              grUserComps.addQuery('u_competency', optComps[o]);
              grUserComps.query();
              if (grUserComps.next()) {
                comp.name = grUserComps.u_competency.getDisplayValue();
                comp.prof = 'N/A';
                res.optComps.push(comp);
              }
            }
          }
          var gr_scheduling = new GlideAggregate('u_scheduling_details');
          gr_scheduling.addQuery('u_resource', propResources[r]);
          gr_scheduling.addQuery('u_date_pa', '!=', '');
          gr_scheduling.addAggregate('SUM', 'u_hours_week');
          gr_scheduling.groupBy('u_resource');
          gr_scheduling.groupBy('u_date_pa');
          gr_scheduling.groupBy('u_week');
          gr_scheduling.orderBy('u_date');
          gr_scheduling.query();
          while (gr_scheduling.next()) {
            //var dateWeek = gr_scheduling.u_date.getDisplayValue().split('-');
            var week = {
              date: gr_scheduling.u_date_pa.getDisplayValue(),
              week: gr_scheduling.u_week.getDisplayValue(),
              hours: parseInt(gr_scheduling.getAggregate('SUM', 'u_hours_week'))
            };
            res.availability.push(week);
          }
          requestObj.proposedResources.push(res);
        }

        var engagement = gr_requests.u_engagement.getDisplayValue();
        var grEng = new GlideRecord('u_engagement');
        grEng.addQuery('u_engagement_short_name', engagement);
        grEng.query();
        while (grEng.next()) {
          requestObj.engID = grEng.getUniqueValue();
          requestObj.engagement = grEng.u_engagement_number.getDisplayValue() + ' - ' + engagement;
        }

        var requestGeo = gr_requests.u_geography.split(',');
        for (var i in requestGeo) {
          var grGeo = new GlideRecord('u_conf_countries_iso_3166');
          grGeo.addActiveQuery();
          grGeo.addQuery('sys_id', requestGeo[i]);
          grGeo.query();
          while (grGeo.next()) {
            var locationObj = {
              sys_id: requestGeo[i],
              location: grGeo.u_country_name.getDisplayValue()
            };
            if (grGeo.u_city_name.getDisplayValue()) {
              locationObj.location = locationObj.location + ' - ' + grGeo.u_city_name.getDisplayValue();
            }
            requestObj.locationsAdded.push(locationObj);
          }
        }

        var categories = gr_requests.u_category_list.split(',');
        for (var j in categories) {
          var grCats = new GlideRecord('u_lov_professional_category');
          grCats.addActiveQuery();
          grCats.addQuery('sys_id', categories[j]);
          grCats.query();
          while (grCats.next()) {
            requestObj.categoryList.push(categories[j]);
            if (requestObj.categoryShow !== '') {
              requestObj.categoryShow += ', ';
            }
            requestObj.categoryShow += grCats.u_professional_category.getDisplayValue();
          }
        }
        data.requestMaster[requestId] = requestObj;
      }
      projectObj.requests.push(requestId);
    }
    data.projDict[projectId] = projIndex;
    projIndex++;
    data.projects.push(projectObj);

  }
  var newProjID = decodeURIComponent(gs.action.getGlideURI());
  if (newProjID.indexOf("&modal=") !== -1) {
    data.modal = newProjID.split("&modal=")[1].split("&")[0];
  }

  console.log(data);
})();
