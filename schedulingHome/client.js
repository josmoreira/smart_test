function controller($scope, $filter, $http, $timeout, $mdDateLocale) {
    /* widget controller */
    var c = this;

    var collapsed = false;
    var currentCompetency = "";
    $scope.currentWrittenCompetency = "";

    $timeout(function() {

        if (!isMobile.apple.tablet) {
            $('body').tooltip({
                selector: '[data-toggle="tooltip"]',
                trigger: 'hover'
            });
        }
        $(".modal-body").scroll(function() {
            console.log("Scrolling");

            var opened = $("md-autocomplete")
                .filter((_, e) => !angular.element(e).controller('mdAutocomplete').hidden);
            if (opened.length >= 1) {
                $(document.activeElement).blur();
            }
        });
    }, 1000);

    $scope.viewPropResIndex = 0;

    $scope.prevRes = function() {
        $scope.viewPropResIndex--;
    };

    $scope.nextRes = function() {
        $scope.viewPropResIndex++;
    };

    $scope.locationResults = function() {
        return $scope.selectedRequest.project.geography
            .filter(loc => loc.location.toLowerCase().includes($scope.currentWrittenLocation.toLowerCase()));
    };

    $scope.competencyResults = function(currentWrittenComp) {
        $scope.currentWrittenCompetency = currentWrittenComp;
        return $scope.data.competenciesNames
            .filter(comp => comp.toLowerCase().includes(currentWrittenComp.toLowerCase()));
    };



    $scope.data.competenciesObjects = Object.keys($scope.data.competenciesObjects)
        .map(k => $scope.data.competenciesObjects[k]);
    //$scope.indexStat1 = 0;
    //$scope.selectedComp1 = $scope.data.dummyRequest.resources[0].comps[0]

    $scope.addedComps = [];

    $scope.competencyType = 'Mandatory';

    $scope.toggleCompetencyType = function() {
        if ($scope.competencyType == 'Mandatory') {
            $scope.competencyType = 'Optional';
        } else {
            $scope.competencyType = 'Mandatory';
        }
    };

    $scope.addCompetency = function() {
        if ($scope.competencyType == 'Mandatory') {
            $scope.addMandatory();
        } else { //Optional
            $scope.addOptional();
        }
    };

    $mdDateLocale.formatDate = function(date) {
        return moment(date).format("DD-MM-YYYY");
    };

    $mdDateLocale.parseDate = function(dateString) {
        var m = moment(dateString, 'DD-MM-YYYY', true);
        return m.isValid() ? m.toDate() : undefined;
    };

    $scope.mobileModalShowNext = true;
    $scope.mobileModalShowPrevious = false;

    $scope.currentPage = 0; // current paging page

    $scope.currentPageModal = 0;

    $scope.closeModal = function() {
        $scope.mobileModalShowNext = true;
        $scope.mobileModalShowPrevious = false;
        $scope.currentPageModal = 0;
        $($scope.modalToUse()).modal('hide');
    };

    $scope.restoreRequest = function(cloning) {

        //REVER quando se deve faze restore
        if ($scope.selectedRequest.request.status.value === 'draft') {
            $scope.selectedRequest.request = $scope.storedRequest;
        } else if (cloning) {
            $scope.selectedRequest.request = $scope.storedRequest;
            return;
        }
        $scope.closeModal();
    };

    $scope.performCancellation = function() {
        $scope.selectedRequest.request.cancellationReason = $scope.cancellationReason;
        var requestObj = {
            sys_id: $scope.selectedRequest.request.sys_id,
            status: "cancelled",
            cancellationReason: $scope.cancellationReason
        };

        $http({
            method: 'PUT',
            url: $scope.data.instanceURL + 'api/dnsa/update_scheduling_request/update',
            headers: {
                'Content-Type': 'application/json'
            },
            data: JSON.stringify(requestObj)
        }).success(function() {
            //paste here
        });

        //this code should be inside the success function
        var requestIndex = $scope.selectedRequest.index;
        var prevState = $scope.selectedRequest.request.status.value;

        if (prevState == 'draft') { //delete if it's a draft
            $scope.selectedRequest.project.requests =
                $scope.selectedRequest.project.requests
                .filter((r) => r.sys_id !== $scope.selectedRequest.request.sys_id);
        } else {
            $scope.selectedRequest.request.status = states.find((s) => s.value == 'cancelled');
            if ($scope.selectedRequest.request.hasOwnProperty('childRequestId')) {
                $scope.data.requestMaster[$scope.selectedRequest.request.childRequestId].status = states.find((s) => s.value == 'invisible');
            }
        }
    };

    $scope.cancelRequest = function() {
        $scope.closeModal();

        if (!$scope.dependsOnMe($scope.selectedRequest.request)) {
            return;
        }

        var prevState = $scope.selectedRequest.request.status.value;

        //in case it requires a reason for cancellation
        if (prevState == 'pmanager_approval') {
            $scope.cancellationReason = "";
            showCancelReasonModal();
            return;
        }
        $scope.performCancellation();
    };

    $scope.acceptProposal = function() {
        var projectIndex = $scope.selectedRequest.project_index;
        var requestIndex = $scope.selectedRequest.index;
        if ($scope.selectedRequest.request.hasDependantRequest) {
            $scope.selectedRequest.request.status = states.find((s) => s.value == 'communication_out');
        } else {
            $scope.selectedRequest.request.status = states.find((s) => s.value == 'closed');
        }
        $scope.selectedRequest.request.allocatedResource = $scope.selectedResource.name;

        if ($scope.selectedRequest.request.hasOwnProperty('childRequestId')) {
            $scope.data.requestMaster[$scope.selectedRequest.request.childRequestId].status = states.find((s) => s.value == 'closed');
        }

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd;
        }

        if (mm < 10) {
            mm = '0' + mm;
        }

        today = dd + '/' + mm + '/' + yyyy;

        if ($scope.selectedRequest.request.approvalComments !== '') {
            $scope.selectedRequest.request.approvalComments += ' | ';
        }

        $scope.selectedRequest.request.approvalComments += today + ' - ' + $scope.data.currentUsername + ': ' + $scope.selectedRequest.request.newApprovalComments;

        var requestObj = {
          'sys_id' : $scope.selectedRequest.request.sys_id,
          'status' : 'approved',
          'approvalComments': $scope.selectedRequest.request.approvalComments,
          'allocatedResource': $scope.selectedResource.sys_id
        };
        $http({
            method: 'PUT',
            url: $scope.data.instanceURL + 'api/dnsa/update_scheduling_request/update',
            headers: {
                'Content-Type': 'application/json'
            },

            data: JSON.stringify(requestObj)
        });

        $scope.closeModal();
    };

    $scope.rejectProposal = function() {
        var projectIndex = $scope.selectedRequest.project_index;
        var requestIndex = $scope.selectedRequest.index;
        $scope.selectedRequest.request.status = states.find((s) => s.value == 'pconnect_approval');

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd;
        }

        if (mm < 10) {
            mm = '0' + mm;
        }

        today = dd + '/' + mm + '/' + yyyy;

        if ($scope.selectedRequest.request.approvalComments !== '') {
            $scope.selectedRequest.request.approvalComments += ' | ';
        }

        $scope.selectedRequest.request.approvalComments += today + ' - ' + $scope.data.currentUsername + ': ' + $scope.selectedRequest.request.newApprovalComments;

        var requestObj = {
          'sys_id': $scope.selectedRequest.request.sys_id,
          'status': 'rejected',
          'approvalComments': $scope.selectedRequest.request.approvalComments
        };

        if ($scope.selectedRequest.request.hasOwnProperty('childRequestId')) {
            $scope.data.requestMaster[$scope.selectedRequest.request.childRequestId].status = states.find((s) => s.value == 'invisible');
        }

        $http({
            method: 'PUT',
            url: $scope.data.instanceURL + 'api/dnsa/update_scheduling_request/update',
            headers: {
                'Content-Type': 'application/json'
            },

            data: JSON.stringify(requestObj)
        });


        $scope.closeModal();
    };

    $scope.saveRequest = function() {
        var requestIndex = $scope.selectedRequest.index;
        $scope.selectedRequest.request.engID = $scope.selectedEngagement.sys_id;
        $scope.selectedRequest.request.engagement = $scope.selectedEngagement.engNumber;
        $scope.selectedRequest.request.nextProjectId = $scope.selectedRequest.project.sys_id;
        var prevState = $scope.selectedRequest.request.status.value;
        $scope.selectedRequest.request.daysLeft = '-';
        $scope.selectedRequest.request.status = states.find((s) => s.value == 'draft');

        var mandComps = getCompsIDs($scope.selectedRequest.request.mandatoryComps);
        var optComps = getCompsIDs($scope.selectedRequest.request.optionalComps);
        var locations = getLocationsIDs($scope.selectedRequest.request.locationsAdded);

        var obj = {
            numResources: $scope.selectedRequest.request.numResources,
            status: $scope.selectedRequest.request.status.value,
            sys_id: $scope.selectedRequest.request.sys_id || null,
            engID: $scope.selectedRequest.request.engID,
            category: $scope.selectedRequest.request.categoryList.toString(),
            role: $scope.selectedRequest.request.role,
            responsibilites: $scope.selectedRequest.request.responsibilities,
            mandatoryComps: mandComps,
            optionalComps: optComps,
            st_date: $scope.selectedRequest.request.st_date,
            end_date: $scope.selectedRequest.request.end_date,
            hoursPerWeek: $scope.selectedRequest.request.hoursPerWeek,
            project_id: $scope.selectedRequest.project.sys_id,
            requesterComments: $scope.selectedRequest.request.requesterComments,
            geography: locations
        };

        $http({
                method: 'PUT',
                url: $scope.data.instanceURL + 'api/dnsa/update_scheduling_request/update',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: JSON.stringify(obj)
            })
            .success(function(response, status, headers, config) {
                $scope.selectedRequest.request.sys_id = response.result;
            });
        console.log($scope.selectedRequest.request.sys_id);

        if (prevState == 'new') {
            $scope.selectedRequest.project.requests.push($scope.selectedRequest.request);
        }


        /*
        the data-dismiss doesnt work when some buttons get disabled/hidden
        due to the status of the request changing.
        so this line of code had to be added
        */
        $scope.closeModal();
        $scope.showProject($scope.selectedRequest.project);
    };


    $scope.submitRequest = function() {
        var requestIndex = $scope.selectedRequest.index;

        var prevState = $scope.selectedRequest.request.status.value;
        $scope.selectedRequest.request.status = states.find((s) => s.value == 'pconnect_approval');
        $scope.selectedRequest.request.engID = $scope.selectedEngagement.sys_id;
        $scope.selectedRequest.request.engagement = $scope.selectedEngagement.engNumber;
        $scope.selectedRequest.request.nextProjectId = $scope.selectedRequest.project.sys_id;
        $scope.selectedRequest.request.requestDate = new Date();
        $scope.selectedRequest.request.requester = $scope.data.currentFullName;

        var mandComps = getCompsIDs($scope.selectedRequest.request.mandatoryComps);
        var optComps = getCompsIDs($scope.selectedRequest.request.optionalComps);
        var locations = getLocationsIDs($scope.selectedRequest.request.locationsAdded);

        var requestObj = {
          "numResources": $scope.selectedRequest.request.numResources,
          "status": $scope.selectedRequest.request.status.value,
          "opened_by": $scope.data.currentUserID,
          "sys_id": $scope.selectedRequest.request.sys_id || null,
          "engID": $scope.selectedRequest.request.engID,
          "category": $scope.selectedRequest.request.categoryList.toString(),
          "role": $scope.selectedRequest.request.role,
          "responsibilites": $scope.selectedRequest.request.responsibilities,
          "mandatoryComps": mandComps,
          "optionalComps": optComps,
          "st_date": $scope.selectedRequest.request.st_date,
          "end_date": $scope.selectedRequest.request.end_date,
          "hoursPerWeek": $scope.selectedRequest.request.hoursPerWeek,
          "project_id": $scope.selectedRequest.project.sys_id,
          "requesterComments": $scope.selectedRequest.request.requesterComments,
          "geography": locations
        };

        $http({
            method: 'PUT',
            url: $scope.data.instanceURL + 'api/dnsa/update_scheduling_request/update',
            headers: {
                'Content-Type': 'application/json'
            },
            data: JSON.stringify(requestObj)
        });

        if (prevState == 'draft') { // remove the draft
            $scope.selectedRequest.project.requests.splice(requestIndex, 1);
        }

        var iterations = $scope.selectedRequest.request.numResources;
        $scope.selectedRequest.request.numResources = 1;
        for (var i = 0; i < iterations; i++) {
            var request = angular.copy($scope.selectedRequest.request);
            $scope.selectedRequest.project.requests.push(request);
        }

        $scope.closeModal();
        $scope.showProject($scope.selectedRequest.project);
    };


    $scope.submitAndNew = function() {
        $scope.submitRequestAgain();
        $scope.selectRequest($scope.selectedRequest.project, $scope.selectedRequest.project_index);
        $($scope.modalToUse()).animate({
            scrollTop: 0
        }, 'slow');
    };

    $scope.sendCommunication = function() {
        $scope.selectedRequest.request.status = states.find((s) => s.value == 'closed');

        var requestObj =
            {"status": $scope.selectedRequest.request.status.value,
            "sys_id": $scope.selectedRequest.request.sys_id
          };

        $http({
                method: 'PUT',
                url: $scope.data.instanceURL + 'api/dnsa/update_scheduling_request/update',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: JSON.stringify(requestObj)
            })
            .success(function(response, status, headers, config) {
                //do nothing
            });

        var dependantRequestId = $scope.selectedRequest.request.dependantRequestId;
        $scope.data.requestMaster[dependantRequestId].status = states.find((s) => s.value == 'closed_early_leave');
        $scope.data.requestMaster[dependantRequestId].new_end_date = $scope.selectedRequest.request.st_date;

        var requestObj =
            {
            "status": $scope.selectedRequest.request.dependantRequest.status.value, //change this to 'communicated' when the backend is ready
            "sys_id": $scope.selectedRequest.request.dependantRequest.sys_id,
            "dateCommOut": $scope.selectedRequest.request.dependantRequest.dateCommOut,
            "commOutComments": $scope.selectedRequest.request.dependantRequest.commOutComments,
            "new_end_date": $scope.selectedRequest.request.st_date
          };

        $http({
                method: 'PUT',
                url: $scope.data.instanceURL + 'api/dnsa/update_scheduling_request/update',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: requestObj
            })
            .success(function(response, status, headers, config) {
                //do nothing
            });

        $scope.closeModal();
    };

    var ops = {
        close: {
            title: "Close",
            classes: "closed",
            icons: "glyphicon glyphicon-remove",
            tooltip: "Close",
            func: $scope.restoreRequest
        },
        discard: {
            title: "Discard",
            classes: "discard",
            icons: "glyphicon glyphicon-trash",
            tooltip: "Discard",
            func: $scope.cancelRequest,
            showOnTable: true,
            hidden: (request) => (request.parentRequestId && request.status.value === 'pmanager_approval')
        },
        save: {
            title: "Save",
            classes: "save",
            icons: "glyphicon glyphicon-floppy-disk",
            tooltip: "Save",
            func: $scope.saveRequest
        },
        submit: {
            title: "Submit",
            classes: "submit",
            icons: "glyphicon glyphicon-save",
            tooltip: "Submit",
            func: $scope.submitRequest,
            disable: (request, project) => ["numResources", "role", "engID", "hoursPerWeek", "categoryList", "locationsAdded", "mandatoryComps", "st_date", "end_date"]
                .find((p) => request[p] === undefined || request[p] === null || request[p].length === 0 || !atLeastOneMandatoryComp(request))
        },
        submitComOut: {
            title: "Submit",
            classes: "submit",
            icons: "glyphicon glyphicon-save",
            tooltip: "Submit Communication Out",
            func: $scope.sendCommunication,
            disable: (request, project) => (request.dependantRequest !== undefined && request.dependantRequest.commOutComments.length === 0),
            hidden: (request, project) => !$scope.communicationIsActive(request, project)
        },
        //button removed, functionality to be implemented with a modal that asks if the user wants to submit another request
        /*submitAndNew: {
            title: "Submit & Create New",
            classes: "submitCreateNew",
            disable: (request) => ["numResources", "role", "engID", "hoursPerWeek", "categoryList", "locationsAdded", "mandatoryComps"]
                .find((p) => request[p] === undefined || request[p].length === 0),
            func: $scope.submitAndNew
        },*/
        accept: {
            title: "Accept",
            classes: "acceptApproval",
            icons: "glyphicon glyphicon-ok",
            tooltip: "Accept Proposal",
            func: $scope.acceptProposal,
            hidden: (request) => request.parentRequestId
        },
        reject: {
            title: "Reject",
            classes: "rejectApproval",
            icons: "glyphicon glyphicon-ban-circle",
            tooltip: "Reject Proposal",
            func: $scope.rejectProposal,
            disable: (request) => request.newApprovalComments === '',
            hidden: (request) => request.parentRequestId
        }
    };

    var states = [{
        value: "draft",
        label: "Draft",
        mandatory: true,
        actions: [ops.close, ops.discard, ops.save, ops.submit],
        modalBlocks: {
            professionalMatrix: (request, project) => ({
                show: true,
                collapsable: false
            }),
            resourceDetails: (request, project) => ({
                show: true,
                collapsable: false
            }),
            competencies: (request, project) => ({
                show: true,
                collapsable: false
            })
        }
    }, {
        value: "new",
        label: "Draft", // to the user this is a draft
        mandatory: true,
        actions: [ops.close, ops.save, ops.submit],
        modalBlocks: {
            professionalMatrix: (request, project) => ({
                show: true,
                collapsable: false
            }),
            resourceDetails: (request, project) => ({
                show: true,
                collapsable: false
            }),
            competencies: (request, project) => ({
                show: true,
                collapsable: false
            })
        }
    }, {
        value: "clone",
        label: "Draft", //to the user this is a draft
        mandatory: true,
        actions: [ops.close, ops.submit],
        modalBlocks: {
            professionalMatrix: (request, project) => ({
                show: true,
                collapsable: false
            }),
            resourceDetails: (request, project) => ({
                show: true,
                collapsable: false
            }),
            competencies: (request, project) => ({
                show: true,
                collapsable: false
            })
        }
    }, {
        value: "pconnect_approval",
        label: "PConnect Approval",
        actions: [ops.close],
        modalBlocks: {
            professionalMatrix: (request, project) => ({
                show: true,
                collapsable: false
            }),
            requestDetails: (request, project) => ({
                show: true,
                collapsable: false
            }),
            separateProposalComments: (request, project) => ({
                show: true,
                collapsable: false
            }),
            resourceDetails: (request, project) => ({
                show: true,
                collapsable: false
            }),
            competencies: (request, project) => ({
                show: true,
                collapsable: false
            })
        }
    }, {
        value: "closed",
        label: "Closed",
        actions: [ops.close], // extend e early leave
        modalBlocks: {
            allocatedResource: (request, project) => ({
                show: true,
                collapsable: false
            }),
            requestDetails: (request, project) => ({
                show: true,
                collapsable: false
            }),
            resourceDetails: (request, project) => ({
                show: true,
                collapsable: false
            }),
            competencies: (request, project) => ({
                show: true,
                collapsable: false
            })
        }
    }, {
        value: "closed_early_leave",
        label: "Closed (Early Leave)",
        actions: [ops.close], // extend e early leave
        modalBlocks: {
            allocatedResource: (request, project) => ({
                show: true,
                collapsable: false
            }),
            earlyLeave: (request, project) => ({
                show: true,
                collapsable: false
            }),
            requestDetails: (request, project) => ({
                show: true,
                collapsable: false
            }),
            resourceDetails: (request, project) => ({
                show: true,
                collapsable: false
            }),
            competencies: (request, project) => ({
                show: true,
                collapsable: false
            })
        }
    }, {
        value: "cancelled",
        label: "Cancelled",
        actions: [ops.close], //clone
        modalBlocks: {
            cancellationReason: (request, project) => ({
                show: true,
                collapsable: false
            }),
            requestDetails: (request, project) => ({
                show: true,
                collapsable: false
            }),
            resourceDetails: (request, project) => ({
                show: true,
                collapsable: true,
                startCollapsed: true
            }),
            competencies: (request, project) => ({
                show: true,
                collapsable: true,
                startCollapsed: true
            })
        }
    }, {
        value: "pmanager_approval",
        label: "Project Approval",
        actions: [ops.close, ops.discard, ops.reject, ops.accept],
        modalBlocks: {
            pconnectProposal: (request, project) => ({
                show: true,
                collapsable: false
            }),
            requestDetails: (request, project) => ({
                show: true,
                collapsable: false
            }),
            separateProposalComments: (request, project) => ({
                show: true,
                collapsable
            }),
            resourceDetails: (request, project) => ({
                show: true,
                collapsable: false
            }),
            competencies: (request, project) => ({
                show: true,
                collapsable: false
            })
        }
    }, {
        value: "communication_out",
        label: "Communication Out",
        actions: [ops.close, ops.submitComOut], // submit se for activo
        modalBlocks: {
            commOutDetails: (request, project) => ({
                show: $scope.communicationIsActive(request, project),
                collapsable: false
            }),
            requestDetails: (request, project) => ({
                show: true,
                collapsable: false
            }),
            allocatedResource: (request, project) => ({
                show: !$scope.communicationIsActive(request, project),
                collapsable: false
            }),
            resourceDetails: (request, project) => ({
                show: true,
                collapsable: false
            }),
            competencies: (request, project) => ({
                show: true,
                collapsable: false
            })
        }
    }, {
        value: "invisible",
        label: "Invisible",
        actions: [],
        modalBlocks: {}
    }];


    $scope.collapseSection = function(section){
      collapseManager.toggleCollapseState(section);
    }

    var collapseManager = {
        toggleCollapseState: (section) => {
            $("[collapse-target='" + section + "']").collapse("toggle");
            $("[collapse-button-hide='" + section + "']").toggle();
            $("[collapse-button-show='" + section + "']").toggle();
        },
        resetCollapseStates: () => {
            var blockObj = $scope.selectedRequest.request.status.modalBlocks;
            Object.keys(blockObj)
                .forEach(sec => {

                    var flags = blockObj[sec]($scope.selectedRequest.request, $scope.selectedRequest.project);
                    if (!flags.collapsable) {
                        $("[collapse-target='" + sec + "']").removeClass("collapse");
                    } else if (flags.startCollapsed) {
                        $("[collapse-target='" + sec + "']").addClass("collapse");
                        $("[collapse-target='" + sec + "']").collapse("hide");
                        $("[collapse-button-hide='" + sec + "']").hide();
                        $("[collapse-button-show='" + sec + "']").show();
                    } else {
                        $("[collapse-target='" + sec + "']").addClass("collapse in");
                        $("[collapse-target='" + sec + "']").collapse("show");
                        $("[collapse-button-hide='" + sec + "']").show();
                        $("[collapse-button-show='" + sec + "']").hide();
                    }

                });

        }
    };

    $scope.selectedRequestNeedsSections = function(sections) {

        if (!$scope.selectedRequest || !$scope.selectedRequest.request) return false;
        return sections
            .filter(prop =>
                $scope.selectedRequest.request.status.modalBlocks[prop] &&
                $scope.selectedRequest.request.status.modalBlocks[prop]($scope.selectedRequest.request, $scope.selectedRequest.project).show
            ).length > 0;
    };

    $scope.selectedRequestSectionIsCollapsable = function(section) {
        if (!$scope.selectedRequest || !$scope.selectedRequest.request) return false;

        return $scope.selectedRequest.request.status.modalBlocks[section] &&
            $scope.selectedRequest.request.status.modalBlocks[section]($scope.selectedRequest.request, $scope.selectedRequest.project).collapsable;
    };

    function showCancelReasonModal() {
        $("#cancelReasonModal").modal('show');
    }

    //replace states
    Object.keys($scope.data.requestMaster)
        .forEach((k) => {
            var obj = $scope.data.requestMaster[k];
            obj.status = states.find((s) => s.value == obj.status.value);
        });

    $scope.data.projects.forEach((proj) => {
        proj.requests = proj.requests.map((id) => $scope.data.requestMaster[id]);
        proj.requests
            .filter((r) => r.hasOwnProperty('dependantRequestId'))
            .forEach((req) => req.dependantRequest = $scope.data.requestMaster[req.dependantRequestId]);
    });

    //build the date objects from the strings
    for (var project in $scope.data.projects) {
        $scope.data.projects[project].startDate = moment($scope.data.projects[project].startDate).toDate();
        if ($scope.data.projects[project].endDate === '' || $scope.data.projects[project].endDate === undefined) {
          $scope.data.projects[project].endDate = moment($scope.data.projects[project].startDate).add(5, 'year').toDate();
        } else {
          $scope.data.projects[project].endDate = moment($scope.data.projects[project].endDate).toDate();
        }
        for (var request in $scope.data.projects[project].requests) {
            $scope.data.projects[project].requests[request].st_date = moment($scope.data.projects[project].requests[request].st_date).toDate();
            $scope.data.projects[project].requests[request].end_date = moment($scope.data.projects[project].requests[request].end_date).toDate();
            $scope.data.projects[project].requests[request].requestDate = moment($scope.data.projects[project].requests[request].requestDate).toDate();
            $scope.data.projects[project].requests[request].new_end_date = moment($scope.data.projects[project].requests[request].new_end_date).toDate();
            if ($scope.data.projects[project].requests[request].dateCommOut === '' || $scope.data.projects[project].requests[request].dateCommOut === undefined) {
                $scope.data.projects[project].requests[request].dateCommOut = new Date();
            } else {
                $scope.data.projects[project].requests[request].dateCommOut = moment($scope.data.projects[project].requests[request].dateCommOut).toDate();
            }
        }
    }

    $scope.changePageModal = function(next) {

        if (next) $scope.currentPageModal = $scope.currentPageModal + 1;
        else $scope.currentPageModal = $scope.currentPageModal - 1;

        $($scope.modalToUse() + " .carousel-inner").carousel($scope.currentPageModal);

        //whenever a new page is opened it should scroll to top
        $("#desktopProjModalMobile .modal-body").scrollTop(0);

        var nrItems = $("#desktopProjModalMobile .item").length;

        if ($scope.currentPageModal == nrItems - 1) {
            $scope.mobileModalShowNext = false;
        } else {
            $scope.mobileModalShowNext = true;
        }

        if ($scope.currentPageModal === 0) {
            $scope.mobileModalShowPrevious = false;
        } else $scope.mobileModalShowPrevious = true;
    };

    $scope.mobile = function() {
        var width = $(window).width();
        return width < 768;
    };

    $scope.projectTitleLimit = function() {
        if (!$scope.mobile()) return 50;
        return Math.floor($(window).width() / 14);
    };

    $scope.empCategoryLimit = function() {
        if ($scope.mobile()) return 20;
        return 100;
    };

    $scope.modalToUse = function() {
        return ($scope.mobile()) ? "#desktopProjModalMobile" : "#desktopProjModal";
    };

    $($scope.modalToUse() + " .carousel-inner").carousel({
        interval: false
    });

    // scroll back to first page when closing modal
    $($scope.modalToUse()).on("show.bs.modal", function() {
        $($scope.modalToUse() + " .carousel-inner").carousel(0);
        $scope.currentPageModal = 1; // set as the second for it will be decremented
        $scope.changePageModal(false); // decremented here
        setTimeout(function() {
            $($scope.modalToUse() + " .modal-body").scrollTop(0);
        }, 250);
    });

    //INJECTION
    $timeout(() => {
        var htmlToCompile = $("#modalBodyTemplate").html();
        $("#desktopProjModal .modal-body").prepend($compile(htmlToCompile)($scope));
        $("#desktopProjModalMobile .carousel-inner").append($compile(htmlToCompile)($scope));
        $scope.$apply();
    });

    $scope.currentWrittenLocation = "";

    $scope.canAddCurrentWrittenLocation = function() {
      if ($scope.selectedRequest === undefined) {
        return;
      }
        if ($scope.selectedRequest.project === undefined) {
            return false;
        }
        return (($scope.selectedRequest.project.geography
            .find(e => e.location == $scope.currentWrittenLocation)
        ) && ($scope.selectedRequest.request.locationsAdded === undefined ||
            !$scope.selectedRequest.request.locationsAdded
            .find(e => e.location == $scope.currentWrittenLocation)
        ));
    };

    $scope.addLocationToSelectedRequest = function() {
        var match = $scope.selectedRequest.project.geography
            .find(e => e.location == $scope.currentWrittenLocation);
        if (match === undefined) return;
        if (!$scope.selectedRequest.request.locationsAdded) {
            $scope.selectedRequest.request.locationsAdded = [];
        }
        $scope.selectedRequest.request.locationsAdded.push(match);
        $scope.currentWrittenLocation = "";
    };

    $scope.removeLocationFromSelectedRequest = function(toRemove) {
        if (!$scope.isEditable($scope.selectedRequest.request)) {
            return;
        }
        var ind = $scope.selectedRequest.request.locationsAdded.findIndex(e => e.sys_id == toRemove.sys_id);
        if (ind == -1) return;
        $scope.selectedRequest.request.locationsAdded.splice(ind, 1);
    };

    $scope.selectFilter = function(filter) {
        $scope.filteringActiveProjects = false;
        $scope.filterRequests = filter;
        $scope.currentPage = 0;
    };

    const resetCurrentPage = () => {
        $scope.currentPage = 0;
        $scope.showProject(); //unshow
    };

    $scope.$watch("filterRequests", resetCurrentPage);
    $scope.$watch("filterProjects", resetCurrentPage);

    $scope.projectFilter = function(proj) {
        //show all case
        if (proj.type !== "Project")
            return false;
        else
            return $scope.filterProjects.value === undefined ||
                $scope.filterProjects.value === proj.status.value;

    };

    $timeout(function() {
        if ($scope.data.modal !== undefined) {
            var projIndex = $scope.data.projDict[$scope.data.modal];
            $scope.selectRequest($scope.data.projects[projIndex], projIndex);
            $($scope.modalToUse()).modal('toggle');
        }
    }, 500);

    $scope.characterLimit = 35;

    //change states
    $scope.isActive = function(request) {
        if (!request.status.value.includes('closed') && request.status.value !== 'cancelled') {
            return true;
        } else {
            return false;
        }
    };

    $scope.isClosed = function(request) {
        return request.status.value.includes('closed');
    };

    $scope.communicationIsActive = function(request, project) {
        return request !== undefined && (request.status.value == 'communication_out' && request.nextProjectId != project.sys_id);
    };

    $scope.dependsOnMe = function(request) {
        return request !== undefined && (request.status.value === 'draft' || (request.status.value === 'pmanager_approval' && !request.parentRequestId));
    };

    $scope.showProject = function(project) {
        $scope.showingProject = project;
        $scope.currentPage = 0;
    };

    $scope.selectedRequest = {};

    var selectedEngagement = {};

    function addedComps(where2search) {
        var mandatory = Object.keys(where2search.mandatoryComps)
            .reduce((acc, k) => acc.concat(where2search.mandatoryComps[k]), []);
        var optional = Object.keys(where2search.optionalComps)
            .reduce((acc, k) => acc.concat(where2search.optionalComps[k]), []);
        return mandatory.concat(optional);
    }
    //set the selected project
    $scope.selectRequest = function(project, projIndex, request, reqIndex) {
        var requestCompetencies = [];
        if (request === undefined) {
            $scope.selectedEngagement = '';
            request = {
                status: states.find((s) => s.value == 'new'),
                st_date: new Date(),
                end_date: moment().add(1, 'month').toDate(),
                mandatoryComps: angular.copy(project.mandatoryComps),
                optionalComps: angular.copy(project.optionalComps),
                approvalComments: '',
                hoursPerWeek: 40,
                categoryShow: '',
                categoryList: []
            };
            requestCompetencies = addedComps(project);
        } else {
            $scope.storedRequest = angular.copy(request);
            $scope.selectedEngagement = {
                engNumber: request.engagement,
                sys_id: request.engID
            };
            if (request.status.value == 'pmanager_approval') {
                $scope.selectedResource = request.proposedResources[0];
            }
            requestCompetencies = addedComps(request);
        }
        $scope.selectedRequest = {
            index: reqIndex,
            project: project,
            project_index: projIndex,
            request: request,
            addedComps: requestCompetencies,
            processedComments: (request.approvalComments ? request.approvalComments : "")
                .split('|').map(c => c.trim()).join('\n')
        };
        $scope.selectedRequest.request.newApprovalComments = '';
    };

    $timeout(function() {

        $('#desktopProjModal').on('shown.bs.modal', function(e) {
            collapseManager.resetCollapseStates();
        });

    });

    //Function to select Engagement
    $scope.selectedEngagementChanged = function(index) {
        $scope.selectedEngagement = $scope.selectedRequest.project.engagements[index];
        $scope.selectedRequest.request.engID = $scope.selectedEngagement.sys_id;
    };

    $scope.selectedResourceChanged = function(index) {
        $scope.selectedResource = $scope.selectedRequest.request.proposedResources[index];
    };



    $scope.catIsSelected = function(cat_sys_id) {
        return ($scope.selectedRequest.request !== undefined && $scope.selectedRequest.request.categoryList !== '' &&
            $scope.selectedRequest.request.categoryList.indexOf(cat_sys_id) > -1);
    };

    //Function to update Profissional Category Field
    $scope.activeCategory = function($event, catLabel, catSysId) {
        if ($scope.selectedRequest.request.categoryList.indexOf(catSysId) > -1) {
            var Id_index = $scope.selectedRequest.request.categoryList.indexOf(catSysId);
            $scope.selectedRequest.request.categoryList.splice(Id_index, 1);
            var catLabelsArray = $scope.selectedRequest.request.categoryShow.split(', ');
            var label_index = catLabelsArray.indexOf(catLabel);
            catLabelsArray.splice(label_index, 1);
            $scope.selectedRequest.request.categoryShow = catLabelsArray.join(", ");
        } else {
            $scope.selectedRequest.request.categoryList.push(catSysId);
            if ($scope.selectedRequest.request.categoryShow !== '') {
                $scope.selectedRequest.request.categoryShow += ', ';
            }
            $scope.selectedRequest.request.categoryShow += catLabel;
        }
    };

    $scope.canAddComp = function() {
        return $scope.data.currentComp;
    };

    $scope.compSearchOptions = function() {
        return $scope.data.competenciesObjects
            .filter(c1 => !$scope.selectedRequest.addedComps.find(c2 => c1.sys_id === c2.sys_id))
            .filter(c => c.name.toLowerCase().includes($scope.data.compSearchTerm.toLowerCase()));
    };

    $scope.addMandatory = function() {
        var compObj = $scope.data.currentComp;
        $scope.selectedRequest.addedComps.push(compObj);
        if (compObj.cat === 'Technology' && $.inArray(compObj, $scope.selectedRequest.request.mandatoryComps.technology) < 0 &&
            $.inArray(compObj, $scope.selectedRequest.request.optionalComps.technology) < 0) {
            $scope.selectedRequest.request.mandatoryComps.technology.push(compObj);
        } else if (compObj.cat === 'Business' && $.inArray(compObj, $scope.selectedRequest.request.mandatoryComps.business) < 0 &&
            $.inArray(compObj, $scope.selectedRequest.request.optionalComps.business) < 0) {
            $scope.selectedRequest.request.mandatoryComps.business.push(compObj);
        } else if (compObj.cat === 'Languages' && $.inArray(compObj, $scope.selectedRequest.request.mandatoryComps.languages) < 0 &&
            $.inArray(compObj, $scope.selectedRequest.request.optionalComps.languages) < 0) {
            $scope.selectedRequest.request.mandatoryComps.languages.push(compObj);
        } else if (compObj.cat === 'Client Knowledge' && $.inArray(compObj, $scope.selectedRequest.request.mandatoryComps.clientKnowledge) < 0 &&
            $.inArray(compObj, $scope.selectedRequest.request.optionalComps.clientKnowledge) < 0) {
            $scope.selectedRequest.request.mandatoryComps.clientKnowledge.push(compObj);
        }
        $scope.data.compSearchTerm = "";
    };

    $scope.addOptional = function() {
        var compObj = $scope.data.currentComp;
        $scope.selectedRequest.addedComps.push(compObj);
        if (compObj.cat === 'Technology' && $.inArray(compObj, $scope.selectedRequest.request.mandatoryComps.technology) < 0 &&
            $.inArray(compObj, $scope.selectedRequest.request.optionalComps.technology) < 0) {
            $scope.selectedRequest.request.optionalComps.technology.push(compObj);
        } else if (compObj.cat === 'Business' && $.inArray(compObj, $scope.selectedRequest.request.mandatoryComps.business) < 0 &&
            $.inArray(compObj, $scope.selectedRequest.request.optionalComps.business) < 0) {
            $scope.selectedRequest.request.optionalComps.business.push(compObj);
        } else if (compObj.cat === 'Languages' && $.inArray(compObj, $scope.selectedRequest.request.mandatoryComps.languages) < 0 &&
            $.inArray(compObj, $scope.selectedRequest.request.optionalComps.languages) < 0) {
            $scope.selectedRequest.request.optionalComps.languages.push(compObj);
        } else if (compObj.cat === 'Client Knowledge' && $.inArray(compObj, $scope.selectedRequest.request.mandatoryComps.clientKnowledge) < 0 &&
            $.inArray(compObj, $scope.selectedRequest.request.optionalComps.clientKnowledge) < 0) {
            $scope.selectedRequest.request.optionalComps.clientKnowledge.push(compObj);
        }
        $scope.data.compSearchTerm = "";
    };

    $scope.removeMT = function(index) {
        if (!$scope.isEditable($scope.selectedRequest.request)) {
            return;
        }
        var sys_id = $scope.selectedRequest.request.mandatoryComps.technology[index].sys_id;
        $scope.selectedRequest.request.mandatoryComps.technology.splice(index, 1);
        $scope.selectedRequest.addedComps = $scope.selectedRequest.addedComps.filter(c => c.sys_id !== sys_id);
    };

    $scope.removeMB = function(index) {
        if (!$scope.isEditable($scope.selectedRequest.request)) {
            return;
        }
        var sys_id = $scope.selectedRequest.request.mandatoryComps.business[index].sys_id;
        $scope.selectedRequest.request.mandatoryComps.business.splice(index, 1);
        $scope.selectedRequest.addedComps = $scope.selectedRequest.addedComps.filter(c => c.sys_id !== sys_id);
    };

    $scope.removeML = function(index) {
        if (!$scope.isEditable($scope.selectedRequest.request)) {
            return;
        }
        var sys_id = $scope.selectedRequest.request.mandatoryComps.languages[index].sys_id;
        $scope.selectedRequest.request.mandatoryComps.languages.splice(index, 1);
        $scope.selectedRequest.addedComps = $scope.selectedRequest.addedComps.filter(c => c.sys_id !== sys_id);
    };

    $scope.removeMCK = function(index) {
        if (!$scope.isEditable($scope.selectedRequest.request)) {
            return;
        }
        var sys_id = $scope.selectedRequest.request.mandatoryComps.clientKnowledge[index].sys_id;
        $scope.selectedRequest.request.mandatoryComps.clientKnowledge.splice(index, 1);
        $scope.selectedRequest.addedComps = $scope.selectedRequest.addedComps.filter(c => c.sys_id !== sys_id);
    };

    $scope.removeOT = function(index) {
        if (!$scope.isEditable($scope.selectedRequest.request)) {
            return;
        }
        var sys_id = $scope.selectedRequest.request.optionalComps.technology[index].sys_id;
        $scope.selectedRequest.request.optionalComps.technology.splice(index, 1);
        $scope.selectedRequest.addedComps = $scope.selectedRequest.addedComps.filter(c => c.sys_id !== sys_id);

    };

    $scope.removeOB = function(index) {
        if (!$scope.isEditable($scope.selectedRequest.request)) {
            return;
        }
        var sys_id = $scope.selectedRequest.request.optionalComps.business[index].sys_id;
        $scope.selectedRequest.request.optionalComps.business.splice(index, 1);
        $scope.selectedRequest.addedComps = $scope.selectedRequest.addedComps.filter(c => c.sys_id !== sys_id);
    };

    $scope.removeOL = function(index) {
        if (!$scope.isEditable($scope.selectedRequest.request)) {
            return;
        }
        var sys_id = $scope.selectedRequest.request.optionalComps.languages[index].sys_id;
        $scope.selectedRequest.request.optionalComps.languages.splice(index, 1);
        $scope.selectedRequest.addedComps = $scope.selectedRequest.addedComps.filter(c => c.sys_id !== sys_id);
    };

    $scope.removeOCK = function(index) {
        if (!$scope.isEditable($scope.selectedRequest.request)) {
            return;
        }
        var sys_id = $scope.selectedRequest.request.optionalComps.clientKnowledge[index].sys_id;
        $scope.selectedRequest.request.optionalComps.clientKnowledge.splice(index, 1);
        $scope.selectedRequest.addedComps = $scope.selectedRequest.addedComps.filter(c => c.sys_id !== sys_id);
    };

    $scope.cloneRequest = function() {
        $scope.restoreRequest(true);
        $scope.selectedRequest.request.status = states.find((s) => s.value == 'clone');
    };

    $scope.submitRequestAgain = function() {
        $scope.submitRequest();
        $scope.closeModal();
    };

    function getCompsIDs(compsObj) {

        return Object.keys(compsObj)
            .reduce((acc, cat) => acc.concat(compsObj[cat]), [])
            .map(c => c.sys_id).join(",");

    }

    function getLocationsIDs(locationsObj) {
        if (locationsObj === undefined) {
            return '';
        }
        return locationsObj.map(e => e.sys_id).join(",");
    }

    $scope.requestsNDraftsForProject = function(project) {
        return project.requests
            .filter(request => {
                if (!$scope.filterRequests.value) return true;
                return $scope.filterRequests.value == request.status.value;
            })
            .filter(request => {
                return (request.nextProjectId === project.sys_id ||
                    (request.nextProjectId !== project.sys_id && request.status.value === 'communication_out'));
            })
            .filter(request => { //removes deleted records (invisible)
                return request.status.value !== 'invisible';
            })
    };

    $scope.requestPaginationFilter = function(obj, index) {
        if (!$scope.activateGridLayout) {
            return parseInt(index / 10) == $scope.currentPage;
        }
        return parseInt(index / ($scope.nrSquaresPerRow() * $scope.nrRowsToShow)) == $scope.currentPage;
    };

    $scope.initialAvailability = 0;
    $scope.availabilityPagesNr = 3;

    $scope.availabilityPaginationFilter = function(_, index) {
        return parseInt(index / 4) >= $scope.initialAvailability &&
            parseInt(index / 4) < $scope.initialAvailability + $scope.availabilityPagesNr;
    }

    $scope.nextAvailability = function() {
        $scope.initialAvailability++;
    }

    $scope.backAvailability = function() {
        $scope.initialAvailability--;
    }

    //LAYOUT TOGGLE
    $timeout(() => $scope.activateGridLayout = ($(window).width() <= 1199));

    $scope.toggleGridLayout = function() {
        $scope.currentPage = 0;
        $scope.activateGridLayout = !$scope.activateGridLayout;
    };

    //PAGINATION
    //Get number of squares that fit in each row
    $scope.nrSquaresPerRow = function() {
        var winWidth = $(window).width();
        if (winWidth < 768) {
            return 1;
        } else if (winWidth <= 1199) {
            return 2;
        } else {
            return 4;
        }
    };

    $scope.currentPage = 0;
    $scope.nrRowsToShow = 2;
    $scope.paginationButtonClicked = function(pageNr, project) {
        if (pageNr == "<<") {
            $scope.currentPage = Math.max($scope.currentPage - 1, 0);
        } else if (pageNr == ">>") {
            $scope.currentPage = Math.min($scope.currentPage + 1, $scope.nrPages($scope.requestsNDraftsForProject(project).length) - 1);
        } else {
            $scope.currentPage = pageNr - 1;
        }
    };

    $scope.nrPages = function(nrEntries) {
        if (!$scope.activateGridLayout) {
            return Math.ceil(nrEntries / 10);
        }
        return Math.ceil(nrEntries / ($scope.nrSquaresPerRow() * $scope.nrRowsToShow));
    };

    $scope.isEditable = function(request) {
        return request === undefined || ['draft', 'new', 'clone'].find(e => e == request.status.value);
    };

    var prevPagingContext = {};

    $scope.pagesArray = function(nrEntries) {
        var nrPages = $scope.nrPages(nrEntries);

        //if no changes return last ret val as not to trigger an update loop
        if (prevPagingContext.nrPages == nrPages && $scope.currentPage == prevPagingContext.currentPage) return prevPagingContext.pgs;

        var pgs = Array.from({
            length: nrPages
        }, (_, k) => k + 1);

        pgs = pgs
            .map((_, i) => ({
                pageNr: i + 1,
                current: i == $scope.currentPage,
                active: true
            }))
            .filter(p => nrPages <= 5 || Math.abs(p.pageNr - 1 - $scope.currentPage) < 2 || (Math.abs(p.pageNr - 1 - $scope.currentPage) < 3 && ($scope.currentPage === 0 || $scope.currentPage == nrPages - 1)));

        if (nrPages > 5) {
            var arrowBack = {
                pageNr: "<<",
                current: false,
                active: ($scope.currentPage !== 0)
            };
            var arrowForward = {
                pageNr: ">>",
                current: false,
                active: ($scope.currentPage <= nrPages - 1)
            };
            pgs = [arrowBack].concat(pgs, [arrowForward]);
        }

        prevPagingContext = {
            nrPages: nrPages,
            pgs: pgs,
            currentPage: $scope.currentPage
        };

        return pgs;
    };

    //convert to array
    $scope.data.filterOptionsProjs = Object.keys($scope.data.filterOptionsProjs)
        .map(k => $scope.data.filterOptionsProjs[k]);


    var showAll = {
        label: "Show All Requests"
    };

    //append a Show all option to all request filter
    $scope.data.filterOptionsProjs.forEach(f => f.requestFilters.unshift(showAll));



    var allReqsFilters = $scope.data.filterOptionsProjs.reduce(
        (all, cur) =>
        //get all non repeted
        all.concat(cur.requestFilters
            .filter(f => !all.find(f2 => f2.value === f.value))
        ), []);

    $scope.data.filterOptionsProjs.unshift({
        label: "Show All Projects",
        requestFilters: allReqsFilters
    });


    $scope.filterProjects = $scope.data.filterOptionsProjs[0];
    $scope.filterRequests = showAll;

    $scope.$watch('filterProjects', function() {
        $scope.filterRequests = showAll;
    });


    $scope.editProject = function(projectId) {
        var url = "/sp?id=_u__scheduling_new_project&project=" + projectId;
        window.location.replace(url);
    };

    function atLeastOneMandatoryComp(request) {
        for (var compType in request.mandatoryComps) {
            if (request.mandatoryComps[compType].length > 0) {
                return true;
            }
        }
        return false;
    }
}

//# sourceURL=schedulingHome.js
