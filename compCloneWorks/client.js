function controller($scope, $rootScope, $http, $timeout) {

    /*

    Pagination control vars

    */

    const range = (start, end) => Array.from({
        length: (end - start)
    }, (v, k) => k + start);

    $scope.nrMyCompPages = 0;
    $scope.curPage = 0;
    var entriesPerPage = 30;

    /* widget controller */
    var c = this;
    var noFilter = true;

    $scope.titleCharLimit = 38;
    $scope.subtitleCharLimit = 60;
    var props = ["cats", "subs", "comps"];

    $scope.changedSomething = function() {
        $rootScope.disableSubmit = false;
        /*
        		$timeout(function () {
        			$('.tooltip').remove();
        		}, 700);
        */
    };

    $scope.isMobile = function() {
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            return true;
        }
        return false;
    };

    function flatten(curName, obj) {

        var availableProps = props.filter(function(p) {
            return obj[p];
        });


        if (availableProps.length === 0) {
            obj.prettyName = curName.join(" > ");
            return [obj];
        }

        var prop = availableProps[0];
        var tmp = [];
        var ys = obj[prop];
        for (var i = 0; i < obj[prop].length; i++) {
            var y = obj[prop][i];
            var newCurName = curName.concat([obj.name]);
            tmp = tmp.concat(flatten(newCurName, y));
        }
        return tmp;

    }

    $rootScope.$broadcast("submitPage");

    $scope.myCompsToShowForState = function(state, doPaging) {

        if (!$scope.data.flattenCompetencies) return [];


        var unpagedComps = $scope.data.flattenCompetencies
            .filter(c => c.approvalState === state) // only approved ones
            .filter(c => $scope.noFilter || c.macroName === $scope.macroCategory); // filter by macro category

        if (!doPaging) return unpagedComps;

        $scope.nrMyCompPages = Math.ceil(unpagedComps.length / parseFloat(entriesPerPage));

        return unpagedComps
            .filter((c, i) => !$scope.isMobile() || (i >= $scope.curPage * entriesPerPage && i < ($scope.curPage + 1) * entriesPerPage))
    }

    var myCompsPagesArray = []

    $scope.myCompsPagesArray = function() {

        var startIndex = Math.max($scope.curPage - 2, 0);
        var endIndex = Math.min($scope.curPage + 3, $scope.nrMyCompPages);

        var newPagesArray = range(startIndex, endIndex).map(i => ({
            "index": i,
            "active": () => i == $scope.curPage
        }));

        var equalPages = myCompsPagesArray.filter(p => newPagesArray.find(p2 => p2.index === p.index || p2.active() === p.active()));

        if (equalPages.length !== newPagesArray.length || myCompsPagesArray.length !== equalPages.length) myCompsPagesArray = newPagesArray;
        return myCompsPagesArray;

    }

    $scope.increasePage = function() {
        $scope.curPage = Math.min($scope.curPage + 1, $scope.nrMyCompPages - 1);
    }
    $scope.decreasePage = function() {
        $scope.curPage = Math.max($scope.curPage - 1, 0);
    }
    $scope.paginationButtonClicked = function(page) {
        $scope.curPage = page.index;
    }

    //event listeners
    $rootScope.$on('sendDataStructure', function(event, data) {
        $scope.data = data;
        $rootScope.$broadcast("competencyScale", data.competencyScale);
        var tmp = [];
        for (var i = 0; i < $scope.data.competencies.length; i++) {
            var macro = data.competencies[i];
            var toAdd = flatten([], macro);
            toAdd.forEach(function(e) {
                e.macroName = macro.name;
            });
            tmp = tmp.concat(toAdd);
        }
        $scope.data.flattenCompetencies = tmp;
        $scope.employeeType = data.userEmployeeType;
        $('body').tooltip({
            selector: '[data-toggle="tooltip"]',
            trigger: 'hover'
        });
    });

    $rootScope.$on('showCategories', function(event, data) {
        $scope.noFilter = false;
        $scope.macroIndex = data;
        $scope.openMacroName = $scope.data.competencies[$scope.macroIndex].name;
        $scope.macroCategory = $scope.data.competencies[$scope.macroIndex].name;
    });

    $rootScope.$on('searchEnabler', function(event, data) {
        showSearchBarMobile();
    });

    $rootScope.$on('searchDisabler', function(event, data) {
        hideSearchBarMobile();
    });

    $rootScope.$on('noFilter', function(event, data) {
        $scope.noFilter = true;
        $scope.macroIndex = '';
        $scope.openMacroName = undefined;
        $scope.macroCategory = '';
    });

    $scope.sendCompetencyMobile = function(selection) {
        if (selection[0] !== 'na') {
            console.log("You already have that competency.");
            return;
        }
        var compListIndex = selection[4];
        var comp = selection[5];
        var sub = selection[6];
        var cat = selection[7];
        var macro = selection[8];
        if (macro === undefined) {
            macro = $scope.macroCategory;
        }

        $rootScope.$broadcast('sendCompetency', selection);

        //level 1 hierarchy competency
        if ((cat === undefined && sub === undefined) || (macro === sub)) {
            var macroIndex = $scope.data.indexes[macro].index;
            var compIndex = $scope.data.indexes[macro][comp].index;
            $scope.data.competencies[macroIndex].comps[compIndex].approvalState = 'added';
        }
        //level 2 hierarchy competency
        else if ((cat === undefined) || (macro === cat)) {
            var macroIndex = $scope.data.indexes[macro].index;
            var subIndex = $scope.data.indexes[macro][sub].index;
            var compIndex = $scope.data.indexes[macro][sub][comp].index;
            $scope.data.competencies[macroIndex].subs[subIndex].comps[compIndex].approvalState = 'added';
        }
        //level 3 hierarchy competency
        else {
            var macroIndex = $scope.data.indexes[macro].index;
            var catIndex = $scope.data.indexes[macro][cat].index;
            var subIndex = $scope.data.indexes[macro][cat][sub].index;
            var compIndex = $scope.data.indexes[macro][cat][sub][comp].index;
            $scope.data.competencies[macroIndex].cats[catIndex].subs[subIndex].comps[compIndex].approvalState = 'added';
        }
        $scope.data.competenciesList[compListIndex].approvalState = 'added';
    };

    $scope.applyCssToTitles = function() {
        /*for (var idx = 0; 0 <document.getElementsByClassName('competencyblock').length; idx++){
        	if (document.getElementsByClassName('competencyblock')[idx] == undefined){
        		break;
        	}
        	var divHeight = document.getElementsByClassName('competencyblock')[idx].clientHeight;
        	var midHeight = Math.round(((100 - divHeight)/2)-7);
        	var midHeight = midHeight.toString() + 'px';*/
        //document.getElementsByClassName('competencyblock')[idx].getElementsByClassName('competency-title')[0].style.setProperty('padding-top',midHeight,'important');
        /*}*/
    };


    if (!$rootScope.worksRegistered) {
        //Add new competencies to a data structure and to the workspace
        $rootScope.$on('sendCompetency', function(event, data) {
            console.log('on');
            var value = data[1];
            var sys_id = data[2];
            var updated = data[3];
            var comp = data[5];
            var sub = data[6];
            var cat = data[7];
            var macro = data[8];
            if (macro === undefined) {
                macro = $scope.macroCategory;
            }

            var newComp = {
                name: comp,
                macro: macro,
                cat: cat,
                sub: sub,
                value: value,
                approvalState: 'added',
                sys_id: sys_id,
                updated: updated
            };

            var oldComps = $scope.data.newCompetencies;
            $scope.data.newCompetencies = [];
            $scope.data.newCompetencies.push(newComp);
            for (var i in oldComps) {
                $scope.data.newCompetencies.push(oldComps[i]);
            }
        });

        $rootScope.worksRegistered = true;
    }


    $scope.calcNewCompetencies = function() {
        var result = false;
        var newCompsIsNull = typeof $scope.data.newCompetencies === 'undefined';
        if (!(newCompsIsNull)) {
            if ($scope.data.newCompetencies.length > 0) {
                result = true;
            }
        }
        return result;
    };

    $scope.formatDate = function(date) {
        var newDate = Date.parse(date);
        return newDate;
    };

    //collapse controllers
    $scope.openMacroName = undefined;
    $scope.openMacro = function(macroName) {
        $scope.curPage = 0;
        if ($scope.openMacroName == macroName) {
            $scope.openMacroName = undefined;
            $scope.macroCategory = '';
            $scope.noFilter = true;
            $scope.macroIndex = '';
            $rootScope.$broadcast('selectMacro', $scope.macroCategory);
        } else {
            $scope.openMacroName = macroName;
            $scope.macroCategory = macroName;
            $scope.macroIndex = $scope.data.indexes[macroName].index;
            $scope.noFilter = false;
            $rootScope.$broadcast('selectMacro', $scope.data.indexes[macroName].index);
        }
        $scope.openCatName = undefined;
        $scope.openSubCatName = undefined;
    };

    $scope.openCatName = undefined;
    $scope.openCat = function(catName) {
        if ($scope.openCatName == catName) {
            $scope.openCatName = undefined;
        } else {
            $scope.openCatName = catName;
        }
        $scope.openSubCatName = undefined;
    };

    $scope.openSubCatName = undefined;
    $scope.openSubCat = function(subName) {
        if ($scope.openSubCatName == subName) {
            $scope.openSubCatName = undefined;
        } else {
            $scope.openSubCatName = subName;
        }
    };





    var showSearchBar = false;

    $scope.toggleSearchBar = function() {
        console.log($(".navbar").css('height'));
        console.log($(".bottomBar").css('height'));

        if (!showSearchBar) {
            showSearchBarMobile();
        } else {
            hideSearchBarMobile();
        }
        showSearchBar = !showSearchBar;
    };

    function stylizeSideBar() {
        $(".sideButton").css({
            top: $("header").css('height'),
            left: $(window).width() - 44
        });

        $(".sideTablet").css({
            left: $(window).width() - 9,
            height: $(".page").height(),
            top: $("header").css('height')
        });

        var navbarHeight = 0;


        if ($("#bottomNavTablet").is(":visible")) {
            navbarHeight = $("#bottomNavTablet").height();
        } else if ($("#bottomNavMobile").is(":visible")) {
            navbarHeight = $("#bottomNavMobile").height();
        }

        $(".searchMobile").css({
            left: $(window).width(),
            top: $("header").css('height'),
            height: $(window).height() - navbarHeight - $("header").height() //111 = bottom height + header height
        });
    }


    $timeout(function() {
        stylizeSideBar();
    });
    $(window).on("orientationchange", function(event) {
        stylizeSideBar();
    });


    function showSearchBarMobile() {
        $(".searchMobile").animate({
            "left": $(window).width() - 300
        });
        $(".sideButton").animate({
            "left": $(window).width() - 335
        });
    }

    function hideSearchBarMobile() {
        $(".searchMobile").animate({
            "left": $(window).width()
        });
        $(".sideButton").animate({
            "left": $(window).width() - 44
        });
    }

    $rootScope.$on("scrollToMyComp", function(event,data) {
			angular.element(".page").animate({
					scrollTop: angular.element("#comp_" + data).offset().top - $(".page").height()/2
			}, 1000);

			$timeout(function() {
					$("#comp_" + data).addClass("glow");

					$timeout(function() {
							$("#comp_" + data).addClass("unglow");
							$timeout(function() {
									$("#comp_" + data).removeClass("glow");
									$("#comp_" + data).removeClass("unglow");
							}, 1000);
					}, 2000);
			}, 1000);

    });


}



//# sourceURL=comp_clone_ws.js
