function controller($rootScope, $scope, $timeout, $http, $uibModal, $filter, $mdDateLocale) {
    /* widget controller */
    var c = this;
    var selectedLocation = "";
    var searchName = "";
    //$scope.locationSearchText = " ";
    var engagementSearchName = "";
    $scope.isMandatoryCommentShown = false;

    if (!$scope.data.addedLocations) $scope.data.addedLocations = [];

    $timeout(function() {
        if (!isMobile.apple.tablet) {
            $('body').tooltip({
                selector: '[data-toggle="tooltip"]',
                trigger: 'hover'
            });
        }

        $("section").scroll(function() {
            console.log("Scrolling");

            var opened = $("md-autocomplete")
                .filter((_, e) => !angular.element(e).controller('mdAutocomplete').hidden);
            if (opened.length >= 1) {
                $(document.activeElement).blur();
            }
        });
    }, 500);


    $scope.data.projectTypes = Object.keys($scope.data.projectTypesDic)
        .map(k => $scope.data.projectTypesDic[k]);

    if ($scope.data.selectedType) {
        $scope.data.selectedType =
            $scope.data.projectTypes.find(t => t.name === $scope.data.selectedType);

        $scope.data.selectedStatus = $scope.data.selectedType.status
            .find(s => s.name === $scope.data.selectedStatus);
    }



    $scope.addedCompetencies = [];

    //$scope.data.competencyObjects
    //fill the array
    ["mandatory", "optional"].forEach(type => ["technology", "clientKnowledge", "languages", "business"].forEach(cat => {
        $scope.data[type][cat].forEach(cName =>
            $scope.addedCompetencies.push($scope.data.competencyObjects.find(c => c.name === cName))
        );
    }));

    //replace strings for date objs
    ["start_date", "end_date"]
    .forEach(p =>
        $scope.data[p] = ($scope.data[p] === "" || $scope.data[p] === undefined) ? undefined :
         moment($scope.data[p]).toDate()
    );

    $mdDateLocale.formatDate = function(date) {
        return moment(date).format("DD-MM-YYYY");

    };



    $mdDateLocale.parseDate = function(dateString) {
        var m = moment(dateString, 'DD-MM-YYYY', true);
        return m.isValid() ? m.toDate() : undefined;
    };

    $scope.mobile = $(window).width() <= 750;

    var locSearchCmp = (locObj) => locObj.name === $scope.locationSearchText;

    var engSearchCmp = (engObj) => ((engObj.description + ' - ' + engObj.number) === $scope.engSearchTerm);

    var locNotAlreadyAdded = (locObj) => !$scope.data.addedLocations.find(l => l == locObj);

    //set the bounds

    var dateWatcher = () => {

        if ($scope.end_date !== undefined && $scope.end_date !== null) {

            var priorToEndDate = moment($scope.end_date).toDate().setDate($scope.end_date.getDate() - 1);
            if ($scope.start_date > priorToEndDate) {
                $scope.end_date = moment(moment($scope.start_date).toDate().setDate($scope.start_date.getDate() + 1)).toDate();
            }

        } else {
          $timeout(function () {
            $("md-datepicker[ng-model='end_date'] input").val('');
          });
        }

        $scope.minMaxDate = moment(moment($scope.start_date).toDate().setDate($scope.start_date.getDate() + 1)).toDate();
    };

    $scope.$watch("start_date", dateWatcher);
    $scope.$watch("end_date", dateWatcher);


		$scope.canAddLocation = function() {

        return ($scope.data.locations
            .filter(locSearchCmp) // is a possible result
            .length > 0) ||
						($scope.locsToSearch().length === 1);
    };


    $scope.locsToSearch = function(locationSearchText) {
        return $scope.data.locations
            .filter(l => !$scope.data.addedLocations.find(n => n.name === l.name))
            .filter(l =>
                !$scope.locationSearchText || l.name.toLowerCase().includes($scope.locationSearchText.toLowerCase())
            );
    };

    if (!$scope.data.isOutsourcingAllowed) {
        document.getElementById("checkboxAll").checked = false;
        $scope.isMandatoryCommentShown = true;
    }

    $scope.changeOutsourcingValue = function() {
        $scope.data.isOutsourcingAllowed = !$scope.data.isOutsourcingAllowed;
        if (!$scope.data.isOutsourcingAllowed) {
            $('#desktopCommentModal').modal({
                backdrop: 'static',
                keyboard: false
            });
            document.getElementById("new-project-form").style.pointerEvents = "none";
        } else if ($scope.data.isOutsourcingAllowed) {
            if ($scope.isMandatoryCommentShown) {
                $scope.changeMandatoryJustificationShowningStatus();
            }
            $("#checkboxAll").prop("checked", true);
            $scope.data.currentMessage = "";
            document.getElementById("new-project-form").style.pointerEvents = "auto";
        }
    };

    $scope.changeMandatoryJustificationShowningStatus = function() {
        $scope.isMandatoryCommentShown = !$scope.isMandatoryCommentShown;
        document.getElementById("new-project-form").style.pointerEvents = "auto";
    };

    //Dates

    $scope.start_date = moment($scope.data.start_date).toDate();

    $scope.end_date =
        ($scope.data.end_date === "") ? undefined : $scope.data.end_date;

    $scope.clearEngagements = function() {
        $scope.data.engagementsadded = [];
    };

    $scope.endDateBiggerthanStartDate = function() {
        if ($scope.start_date > $scope.end_date && $scope.end_date !== undefined) {
            $scope.end_date = $scope.start_date;
        }
    };

    //Close Buttons

    $scope.removeLocation = function(locName) {

        var index = $scope.data.addedLocations
            .findIndex((e) => e.name === locName);
        if (index === -1) return;
        $scope.data.addedLocations.splice(index, 1);
    };


    $("#commonSkillBoxList").on("click", "button", function(e) {
        e.preventDefault();
        var temp = $(this).parent().text();
        var tempSubString = temp.substring(0, temp.length - 1);
        $scope.data.competenciesList.push(tempSubString);
        delete $scope.data.competenciesadded[tempSubString];
        $scope.$apply();
    });

    $("#searchDataClient").on("input", function() {
        //$scope.addClient = function (){
        var content = $('#searchDataClient').val();
        var parts = content.split(" - ");
        if (parts.length === 0) return;
        var name = parts[1];
        var code = parts[0];
        var clientFound = $scope.data.clientsList.find(c => c.code == code);
        if (content !== "" && clientFound) {
            $scope.data.clientsadded = clientFound;
            $scope.data.engagementsadded = [];
            $scope.invalidClient = false;


        } else if (!clientFound && content !== '') {
            $scope.data.clientsadded = undefined;
            $scope.invalidClient = true;
        }
        $scope.$apply();

    });

    $scope.removeTheLineEngagementTable = function(eng) {
        var index = $scope.data.engagementsadded
            .findIndex(e => e.number == eng.number);
        if (index === -1) return;
        $scope.data.engagementsadded.splice(index, 1);
    };

    $scope.removeTheLineManagerTable = function(user) {
        var index = $scope.data.managersadded.indexOf(user);
        if (index >= 0) {
            $scope.data.managersadded.splice(index, 1);
        }
    };

    $scope.removeComp = function(comp) {
        var index = $scope.addedCompetencies.findIndex(c => c.name == comp);

        if (index === -1) return;
        $scope.addedCompetencies.splice(index, 1);

        ["mandatory", "optional"].forEach(type => ["technology", "clientKnowledge", "languages", "business"].forEach(cat => {
            var index = $scope.data[type][cat].findIndex(c => c === comp);
            if (index === -1) return;
            $scope.data[type][cat].splice(index, 1);
        }));


    };
    ///Add butttons

    $scope.addLocation = function() {

        if ($scope.data.addedLocations === undefined) {
            $scope.data.addedLocations = [];
        }

        var matches = $scope.locsToSearch($scope.locationSearchText);
        if (matches.length === 0) return;

        //already there
        if ($scope.data.addedLocations.find(l => l === matches[0])) {
            return;
        }

        $scope.data.addedLocations.push(matches[0]);

        $scope.locationSearchText = "";
    };

    $scope.canAddProjectManager = function() {
        return (!$scope.data.managersadded.includes($scope.selectedEngOwner) &&
                $scope.selectedEngOwner) ||
            (
                $scope.engOwnersSearchOptions().length === 1
            );
        //return $scope.data.managersObjects[$('#searchInputProjectManager').val()];
    };

    $scope.addProjectManager = function(user) {
        var matches = $scope.engOwnersSearchOptions();
        if (matches.length === 0) return;
        $scope.data.managersadded.push(matches[0]);
        $scope.engOwnerSearchTerm = "";
    };

    $scope.engOwnersSearchOptions = function() {

        return $scope.data.managersObjects
            .filter(o =>
                o.name.toLowerCase().includes($scope.engOwnerSearchTerm.toLowerCase()) ||
                o.username.toLowerCase().includes($scope.engOwnerSearchTerm.toLowerCase()))
            .filter(o => !$scope.data.managersadded.find(n => n.name === o.name));
    };



    $scope.canAddEngagement = function() {
        return ($scope.data.engagementsadded.indexOf($scope.selectedEng) < 0 && $scope.data.clientsadded &&
            $scope.data.engagementList
            .filter(engSearchCmp) // is a possible result
            .length > 0) ||
						($scope.engSearchOptions().length === 1);
    };
    $scope.engagementToAddRepeatedFilter = function(eng) {
        return $scope.data.engagementsadded
            .find(e => e.number === eng.number) === undefined;
    };
    $scope.addEngagement = function() {
        var matches = $scope.engSearchOptions();
        if (matches.length === 0) return;
        $scope.data.engagementsadded.push(matches[0]);
        $scope.engSearchTerm = "";
    };

    $scope.canAddCompetency = function() {
        return (!$scope.addedCompetencies.includes($scope.currentlySelectedCompetency) &&
            $scope.currentlySelectedCompetency) ||
						($scope.competenciesForSearch().length === 1);

    };

    $scope.competenciesForSearch = function() {

        return $scope.data.competencyObjects
            .filter(c => !$scope.addedCompetencies.includes(c))
            .filter(c => c.name.toLowerCase().includes($scope.compSearchTerm.toLowerCase()));
    };

    $scope.addMandatory = function() {
        var compName = $scope.currentlySelectedCompetency.name;
        $scope.addedCompetencies.push($scope.currentlySelectedCompetency);
        if ($scope.currentlySelectedCompetency.cat === 'Technology' && $.inArray(compName, $scope.data.mandatory.technology) < 0 && $.inArray(compName, $scope.data.optional.technology) < 0) {
            $scope.data.mandatory.technology.push(compName);
        } else if ($scope.currentlySelectedCompetency.cat === 'Business' && $.inArray(compName, $scope.data.mandatory.business) < 0 && $.inArray(compName, $scope.data.optional.business) < 0) {
            $scope.data.mandatory.business.push(compName);
        } else if ($scope.currentlySelectedCompetency.cat === 'Languages' && $.inArray(compName, $scope.data.mandatory.languages) < 0 && $.inArray(compName, $scope.data.optional.languages) < 0) {
            $scope.data.mandatory.languages.push(compName);
        } else if ($scope.currentlySelectedCompetency.cat === 'Client Knowledge' && $.inArray(compName, $scope.data.mandatory.clientKnowledge) < 0 && $.inArray(compName, $scope.data.optional.clientKnowledge) < 0) {
            $scope.data.mandatory.clientKnowledge.push(compName);
        }
        $scope.compSearchTerm = "";
    };

    $scope.addOptional = function() {
        var compName = $scope.currentlySelectedCompetency.name;
        $scope.addedCompetencies.push($scope.currentlySelectedCompetency);
        if ($scope.currentlySelectedCompetency.cat === 'Technology' && $.inArray(compName, $scope.data.mandatory.technology) < 0 && $.inArray(compName, $scope.data.optional.technology) < 0) {
            $scope.data.optional.technology.push(compName);
        } else if ($scope.currentlySelectedCompetency.cat === 'Business' && $.inArray(compName, $scope.data.mandatory.business) < 0 && $.inArray(compName, $scope.data.optional.business) < 0) {
            $scope.data.optional.business.push(compName);
        } else if ($scope.currentlySelectedCompetency.cat === 'Languages' && $.inArray(compName, $scope.data.mandatory.languages) < 0 && $.inArray(compName, $scope.data.optional.languages) < 0) {
            $scope.data.optional.languages.push(compName);
        } else if ($scope.currentlySelectedCompetency.cat === 'Client Knowledge' && $.inArray(compName, $scope.data.mandatory.clientKnowledge) < 0 && $.inArray(compName, $scope.data.optional.clientKnowledge) < 0) {
            $scope.data.optional.clientKnowledge.push(compName);
        }
        $scope.compSearchTerm = "";
    };

    const compTypes = [{
            name: "Mandatory",
            func: $scope.addMandatory
        },
        {
            name: "Optional",
            func: $scope.addOptional
        }
    ];

    $scope.compTypeToAdd = compTypes[0];

    $scope.toggleCompTypeToAdd = function() {
        $scope.compTypeToAdd = compTypes
            .find(c => c.name !== $scope.compTypeToAdd.name);
    };

    //Change selected values

    $scope.selectedLocationChanged = function(index) {
        $scope.selectedLocation = $scope.data.locationObjects[index];
    };

    $scope.selectedTypeChanged = function(type) {
        $scope.data.selectedType = type;
        $scope.data.selectedStatus = undefined;
    };

    $scope.selectedStatusChanged = function(status) {
        $scope.data.selectedStatus = status;
    };

    /*$scope.selectedClusterChanged = function(key) {
      $scope.data.selectedCluster = $scope.data.clusterList[key];
      $scope.data.clientsadded = undefined;
      $scope.data.engagementsadded = [];
      $scope.engSearchTerm = "";
      //document.getElementById('searchDataClient').value = "";
    };*/

    $scope.clientOptions = function() {


        return $scope.data.clientsList
            //filter by cluster (removed)
            //.filter(c => !$scope.data.selectedCluster || c.cluster_id == $scope.data.selectedCluster.sys_id)
            //filter by searchTerm
            .filter(c =>
                (c.code + ' - ' + c.name).toLowerCase().includes($scope.searchClientText.toLowerCase())
            );
    };

    $scope.selectedUserChanged = function(index) {
        $scope.selectedUser = $scope.data.managersObjects[index];
    };

    //Filters

    $scope.delToFilterProjectManagers = function(entry) {
        var isSearchContentContainedInName = true;
        if ($scope.searchName) {
            isSearchContentContainedInName = entry.name.toLowerCase().indexOf($scope.searchName.toLowerCase()) !== -1;
        }

        return entry.selected && isSearchContentContainedInName;
    };



    $scope.engSearchOptions = function() {


        return $scope.data.engagementList
            .filter(obj => !$scope.data.clientsadded || obj.client_name == $scope.data.clientsadded.name || obj.general == "true")
            .filter($scope.engagementToAddRepeatedFilter)
            .filter(eng =>
                eng.number.toLowerCase().includes($scope.engSearchTerm.toLowerCase()) ||
                eng.description.toLowerCase().includes($scope.engSearchTerm.toLowerCase()) ||
                (eng.description.toLowerCase() + " - " + eng.number.toLowerCase()).includes($scope.engSearchTerm.toLowerCase())
            );
        /*
                var result = {};

                angular.forEach(engagementList, function(value, key) {
                    if (value.client_name == $scope.data.clientsadded.name) {
                        result[key] = value;
                    }
                });
                return result;
                */
    };

    /*$scope.filterClientIfClusterSelected = function(entry) {
      if (jQuery.isEmptyObject($scope.data.selectedCluster)) {
        return true;
      }
      return entry.cluster_id == $scope.data.selectedCluster.sys_id;
    };*/

    //New Project


    $scope.createNewProject = function(isResourceRequested) {
        console.log('New Project');
        var projectObj = {
            projectID: $scope.data.projectId,
            status: $scope.data.selectedStatus.sys_id,
            type: $scope.data.selectedType.value,
            //cluster: $scope.data.selectedCluster.sys_id,
            short_description: $scope.data.newprojects.description,
            name: $scope.data.newprojects.projectName,
            notAllowingOutsourcingReason: $scope.data.currentMessage,
            client: $scope.data.clientsadded.sys_id,
            engagements: $scope.data.engagementsadded
                .map(e => e.sys_id).toString(),
            mandatoryCompetencies: getCompsIDs($scope.data.mandatory),
            optionalCompetencies: getCompsIDs($scope.data.optional),
            managers: $scope.data.managersadded
                .map(m => m.userid),
            outsourcing: $scope.data.isOutsourcingAllowed,
            startDate: $scope.start_date,
            endDate: $scope.end_date,
        };
        /*
        projectObj.projectID = $scope.data.projectId;
        projectObj.status = $scope.data.selectedStatus;
        projectObj.type = $scope.data.selectedType;
        projectObj.cluster = $scope.data.selectedCluster.sys_id;
        projectObj.short_description = $scope.data.newprojects.description;
        projectObj.name = $scope.data.newprojects.projectName;
        projectObj.notAllowingOutsourcingReason = $scope.data.currentMessage;


        projectObj.client = $scope.data.clientsadded.sys_id;


        projectObj.engagements = $scope.data.engagementsadded.
        map(e => e.sys_id).toString();


        var mandatoryComps = getCompsIDs($scope.data.mandatory);
        projectObj.mandatoryCompetencies = mandatoryComps;

        var optionalComps = getCompsIDs($scope.data.optional);
        projectObj.optionalCompetencies = optionalComps;

        var managersadded = Object.keys($scope.data.managersadded)
            .map(k => $scope.data.managersadded[k].userid);

        projectObj.managers = managersadded.toString();
        */

        if ($scope.data.addedLocations) {
            projectObj.locations = $scope.data.addedLocations.map((e) => e.sys_id).toString();
        } else {
            projectObj.locations = "";
        }
        //projectObj.outsourcing = $scope.data.isOutsourcingAllowed;
        projectObj.sys_id = '';

        var endDate = angular.element(document.getElementsByClassName('end_date'));
        var modelforEndDate = endDate.controller('ngModel');
        var isPristine = modelforEndDate.$pristine;

        if (isPristine) {
            //projectObj.endDate = '';
        } else if ($scope.end_date < $scope.start_date) {
            //projectObj.endDate = '';
        } else {
            //projectObj.endDate = $scope.end_date;
        }

        //projectObj.startDate = $scope.start_date;

        $http({
                method: 'PUT',
                url: $scope.data.instanceURL + 'api/dnsa/update_scheduling_request/update_project',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: projectObj
            })
            .success(function(response, status, headers, config) {
                var NewProjectSysId = "";
                if (isResourceRequested) {
                    NewProjectSysId = response.result.projectId;
                }
                $scope.changeToSchedulingHome(NewProjectSysId);
            });

    };

    $scope.createNewProjectAndResource = function() {
        $scope.createNewProject(true);
    };


    function getCompsIDs(array) {

        return ["technology", "clientKnowledge", "languages", "business"]
            .reduce((acc, curr) => {
                return acc.concat(array[curr]);
            }, [])
            .map(name => $scope.addedCompetencies.find(c => c.name === name).sys_id);

    }

    //Other
    $scope.isTypeSelected = function() {

        var isTypeSelected = $scope.data.selectedType !== undefined;
        if (isTypeSelected) {
            return false;
        } else {
            return true;
        }
    };

    $scope.areMandatoryFieldsFilled = function() {

        var isNameFilled = $scope.data.newprojects.projectName !== "";
        var isStatusFilled = $scope.data.selectedStatus !== undefined;
        var isTypeFilled = $scope.data.selectedType !== undefined;
        var isClientFilled = !jQuery.isEmptyObject($scope.data.clientsadded);
        //var isClusterFilled = $scope.data.selectedCluster !== undefined;
        var isManagerFilled = $scope.data.managersadded.length > 0;
        var isDescriptionFilled = $scope.data.newprojects.description !== "";
        var isLocationsFilled = $scope.data.addedLocations.length > 0;
        var isEngagementFilled = $scope.data.engagementsadded.length > 0;
				var isStartDateFilled = $scope.start_date !== null && $scope.start_date !== undefined;


        return isNameFilled && isStatusFilled && isTypeFilled && isClientFilled &&
            isManagerFilled && isDescriptionFilled && isLocationsFilled &&
            isEngagementFilled && isStartDateFilled;

    };

    $scope.changeToSchedulingHome = function(projectId) {
        var url = "/sp?id=%5Bu%5Dschedulinghome";

        if (projectId) {
            url = url + "&modal=" + projectId;
        }

        window.location.replace(url);
    };



    //$scope.locationSearchText = "x";
}


//# sourceURL=schedulingNewProject.js
