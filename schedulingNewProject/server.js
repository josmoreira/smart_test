(function() {
  /* populate the 'data' object */
  /* e.g., data.table = $sp.getValue('table'); */
  data.instanceURL = gs.getProperty('glide.servlet.uri');

  data.newprojects = {};
  data.newprojects.engagements = [];
  data.newprojects.projectStartDate = '';
  data.newprojects.projectEndDate = '';
  data.newprojects.short_description = '';
  data.newprojects.commonSkills = '';
  data.newprojects.client = '';
  data.newprojects.location = '';
  data.newprojects.projectName = '';
  data.newprojects.description = '';

  data.engagementsadded = [];
  data.defaultDate = '';
  data.selectedStatus = "";
  data.isOutsourcingAllowed = true;
  data.currentMessage = "";
  data.start_date = new Date();
  data.addedLocations = [];

  data.managersadded = [];
  data.competenciesadded = {};

  data.mandatory = {};
  data.mandatory.technology = [];
  data.mandatory.business = [];
  data.mandatory.languages = [];
  data.mandatory.clientKnowledge = [];

  data.optional = {};
  data.optional.technology = [];
  data.optional.business = [];
  data.optional.languages = [];
  data.optional.clientKnowledge = [];

  //Constructors

  ClusterConsructor = function(name, code, sys_id) {
    var clusterObj = {
      name: name,
      code: code,
      sys_id: sys_id
    };
    return clusterObj;
  };

  ClientConstructor = function(name, sys_id, cluster_id, code) {
    var clientObj = {
      name: name,
      sys_id: sys_id,
      cluster_id: cluster_id,
      code: code
    };
    return clientObj;
  };

  locationConstructor = function(name, sys_id) {
    var LocationObj = {
      name: name,
      sys_id: sys_id
    };
    return LocationObj;
  };

  engagementConstructor = function(clientName, number, sys_id, startDate, endDate, description, general) {
    var start_date = new GlideDateTime(startDate);
    var end_date = new GlideDateTime(endDate);
    var engagementObj = {
      client_name: clientName,
      number: number,
      sys_id: sys_id,
      general: general,
      start_date: '' + start_date.getDate(),
      end_date: '' + end_date.getDate(),
      description: description
    };
    return engagementObj;
  };

  managerConstructor = function(username, name, userID, number) {
    if (number === '') {
      number = '-';
    }
    var managerObj = {
      username: username,
      name: name,
      userid: userID,
      number: number
    };
    return managerObj;
  };

  conpetencyConstructor = function(name, sys_id, cat) {
    var competencyObj = {
      name: name,
      sys_id: sys_id,
      cat: cat
    };
    return competencyObj;
  };
  //Auxiliary functions

  getLocationDisplayName = function(country, city) {
    var locationDisplay = country;
    var cityName = city;
    if (cityName) {
      locationDisplay = locationDisplay + " - " + cityName;
    }
    return locationDisplay;
  };

  insertCompetencyInOptional = function(compObj) {
    if (compObj.cat === 'Technology') {
      data.optional.technology.push(compObj.name);
    } else if (compObj.cat === 'Business') {
      data.optional.business.push(compObj.name);
    } else if (compObj.cat === 'Languages') {
      data.optional.languages.push(compObj.name);
    } else if (compObj.cat === 'Client Knowledge') {
      data.optional.clientKnowledge.push(compObj.name);
    }
  };

  insertCompetencyInMandatory = function(compObj) {
    if (compObj.cat === 'Technology') {
      data.mandatory.technology.push(compObj.name);
    } else if (compObj.cat === 'Business') {
      data.mandatory.business.push(compObj.name);
    } else if (compObj.cat === 'Languages') {
      data.mandatory.languages.push(compObj.name);
    } else if (compObj.cat === 'Client Knowledge') {
      data.mandatory.clientKnowledge.push(compObj.name);
    }
  };

  /*isCompetencyInProject = function(competencyName) {
    var isMandatoryTechnology = (data.mandatory.technology.indexOf(competencyName) != -1);
    var isMandatoryBusiness = (data.mandatory.business.indexOf(competencyName) != -1);
    var isMandatoryLanguages = (data.mandatory.languages.indexOf(competencyName) != -1);
    var isMandatoryClientKnowledge = (data.mandatory.clientKnowledge.indexOf(competencyName) != -1);

    var isOptionalTechnology = (data.optional.technology.indexOf(competencyName) != -1);
    var isOptionalBusiness = (data.optional.business.indexOf(competencyName) != -1);
    var isOptionalLanguages = (data.optional.languages.indexOf(competencyName) != -1);
    var isOptionalClientKnowledg = (data.optional.clientKnowledge.indexOf(competencyName) != -1);

    return isMandatoryTechnology || isMandatoryBusiness || isMandatoryLanguages || isMandatoryClientKnowledge ||
      isOptionalTechnology || isOptionalBusiness || isOptionalLanguages || isOptionalClientKnowledg;
  };*/

  //URL Parameter

  data.projectId;

  var URLParameter = decodeURIComponent(gs.action.getGlideURI());
  if (URLParameter.indexOf("&project=") !== -1) {
    data.projectId = URLParameter.split("&project=")[1].split("&")[0];
  }

  //Load existing project information

  if (data.projectId) {
    var gr_project = new GlideRecord('pm_project');
    gr_project.addQuery('sys_id', data.projectId);
    gr_project.query();

    if (gr_project.next()) {
      data.newprojects.projectName = gr_project.short_description.getDisplayValue();
      data.selectedStatus = gr_project.u_project_status.getDisplayValue();
      data.selectedType = gr_project.u_type.getDisplayValue();
      var parsed_allow_outsourcing = (gr_project.u_allow_outsourcing.getDisplayValue() === "true");
      data.isOutsourcingAllowed = parsed_allow_outsourcing;
      data.currentMessage = gr_project.u_not_allowing_outsourcing_reason.getDisplayValue();
      data.selectedCluster = ClusterConsructor(gr_project.u_cluster.u_cluster_name.toString(), gr_project.u_cluster.u_cluster_code.toString(), gr_project.u_cluster.sys_id.toString());
      data.clientsadded = ClientConstructor(gr_project.u_client.u_client_short_name.getDisplayValue(), gr_project.u_client.sys_id.toString(), gr_project.u_client.u_cluster.toString(), gr_project.u_client.u_client_code.getDisplayValue());
      data.start_date = gr_project.u_start_date.getDisplayValue();
      data.end_date = gr_project.u_end_date.getDisplayValue();

      var locationsFromProject = gr_project.u_geography.split(',');
      for (var i in locationsFromProject) {
        var gr_geographies = new GlideRecord('u_conf_countries_iso_3166');
        gr_geographies.addQuery('sys_id', locationsFromProject[i]);
        gr_geographies.query();
        if (gr_geographies.next()) {
          var locationDisplay = getLocationDisplayName(gr_geographies.u_country_name.getDisplayValue(), gr_geographies.u_city_name.getDisplayValue());
          var LocationObj = locationConstructor(locationDisplay, gr_geographies.sys_id.getDisplayValue());
          data.addedLocations.push(LocationObj);
        }
      }

      data.newprojects.description = gr_project.description.getDisplayValue();

      var engagementsFromProject = gr_project.u_engagements.split(',');
      for (var i in engagementsFromProject) {
        var gr_engagements = new GlideRecord('u_engagement');
        gr_engagements.addQuery('sys_id', engagementsFromProject[i]);
        gr_engagements.query();
        if (gr_engagements.next()) {
          var engagementObj = engagementConstructor(gr_engagements.u_client.u_client_short_name.getDisplayValue(), gr_engagements.u_engagement_number.toString(), gr_engagements.sys_id.toString(), gr_engagements.u_start_date.getDisplayValue(), gr_engagements.u_end_date.getDisplayValue(), gr_engagements.u_engagement_short_name.getDisplayValue(), gr_engagements.u_general_engagement.getDisplayValue());
          data.engagementsadded.push(engagementObj);
        }
      }

      var mandatoryCompsFromProject = gr_project.u_mandatory_competencies.split(',');
      for (var i in mandatoryCompsFromProject) {
        var gr_competencies = new GlideRecord('u_competency');
        gr_competencies.addQuery('sys_id', mandatoryCompsFromProject[i]);
        gr_competencies.query();
        if (gr_competencies.next()) {
          var competencyObj = conpetencyConstructor(gr_competencies.name.getDisplayValue(), gr_competencies.sys_id.toString(), gr_competencies.u_competency_macro_type.getDisplayValue());
          insertCompetencyInMandatory(competencyObj);
        }
      }

      var optionalCompsFromProject = gr_project.u_optional_competencies.split(',');
      for (var i in optionalCompsFromProject) {
        var gr_competencies = new GlideRecord('u_competency');
        gr_competencies.addQuery('sys_id', optionalCompsFromProject[i])
        gr_competencies.query();
        if (gr_competencies.next()) {
          var competencyObj = conpetencyConstructor(gr_competencies.name.getDisplayValue(), gr_competencies.sys_id.toString(), gr_competencies.u_competency_macro_type.getDisplayValue());
          insertCompetencyInOptional(competencyObj);
        }
      }

      var managersFromProject = gr_project.u_project_managers.split(',');
      for (var i in managersFromProject) {
        var gr_groupMember = new GlideRecord('sys_user_grmember');
        gr_groupMember.addQuery('user.sys_id', managersFromProject[i])
        gr_groupMember.query();
        if (gr_groupMember.next()) {
          var managerObj = managerConstructor(gr_groupMember.user.user_name.toString(), gr_groupMember.user.getDisplayValue(), gr_groupMember.user.toString(), gr_groupMember.user.u_professional_id.toString());
          data.managersadded.push(managerObj);
        }
      }
    }
  }

  //Project Managers

  data.managersObjects = [];

  var gr_groupMember = new GlideRecord('sys_user_grmember');
  gr_groupMember.addQuery('group.name', "Management");
  gr_groupMember.orderBy('user');
  gr_groupMember.query();

  while (gr_groupMember.next()) {
    var managerObj = managerConstructor(gr_groupMember.user.user_name.toString(), gr_groupMember.user.getDisplayValue(), gr_groupMember.user.toString(), gr_groupMember.user.u_professional_id.toString());
    data.managersObjects.push(managerObj);
  }

  //Locations

  data.locations = [];
  data.locationList = [];

  var gr_locations = new GlideRecord('u_conf_countries_iso_3166');
  gr_locations.orderBy('u_country_name');
  gr_locations.query();

  while (gr_locations.next()) {
    var locationDisplay = getLocationDisplayName(gr_locations.u_country_name.getDisplayValue(), gr_locations.u_city_name.getDisplayValue());
    var LocationObj = locationConstructor(locationDisplay, gr_locations.sys_id.getDisplayValue());
    data.locations.push(LocationObj);
  }

  //Competencies

  data.competencyObjects = [];
  var gr_competencies = new GlideRecord('u_competency');
  gr_competencies.addQuery('u_competency_macro_type', '!=', "Country preferences");
  gr_competencies.orderBy('name');
  gr_competencies.query();

  while (gr_competencies.next()) {
    var competencyObj = conpetencyConstructor(gr_competencies.name.getDisplayValue(), gr_competencies.sys_id.toString(), gr_competencies.u_competency_macro_type.getDisplayValue());
    data.competencyObjects.push(competencyObj);

  }

  // Project Type &Status

  data.newprojects.projectType = '';
  //data.projectTypeList=[];
  data.projectTypesDic = {};

  var gr_projectType = new GlideRecord("u_conf_project_type");
  gr_projectType.addEncodedQuery('u_active!=0');
  gr_projectType.orderBy('u_order');
  gr_projectType.query();

  while (gr_projectType.next()) {
    var type = gr_projectType.u_project_type.toString();
    var typeValue = gr_projectType.u_project_type_value.toString();
    var status_name = gr_projectType.u_project_status.toString();
    var status_id = gr_projectType.sys_id.toString();

    if (!data.projectTypesDic[type]) {
      data.projectTypesDic[type] = {
        name: type,
        value: typeValue,
        status: []
      };
    }
    data.projectTypesDic[type].status.push({
      name: status_name,
      sys_id: status_id
    });
  }

  // Cluster

  data.newprojects.cluster = '';
  data.clusterList = {};

  var gr_clusters = new GlideRecord("u_cluster");
  gr_clusters.query();

  while (gr_clusters.next()) {
    var clusterObj = ClusterConsructor(gr_clusters.u_cluster_name.toString(), gr_clusters.u_cluster_code.toString(), gr_clusters.sys_id.toString());
    data.clusterList[gr_clusters.sys_id.toString()] = clusterObj;
  }

  //Clients

  data.clientsList = [];

  var gr_clients = new GlideRecord('u_client');
  gr_clients.orderBy('u_client_short_name');
  gr_clients.query();

  while (gr_clients.next()) {
    var clientObj = ClientConstructor(gr_clients.u_client_short_name.getDisplayValue(), gr_clients.sys_id.toString(), gr_clients.u_cluster.toString(), gr_clients.u_client_code.getDisplayValue());
    data.clientsList.push(clientObj);
  }

  //Engagements

  data.engagementList = [];

  var gr_engagements = new GlideRecord('u_engagement');
  gr_engagements.query();

  while (gr_engagements.next()) {
    /*
    if(!data.engagementsadded.find(e=>e.number === gr_engagements.u_engagement_number.getDisplayValue())){
    	var engagementObj = engagementConstructor(gr_engagements.u_client.u_client_short_name.getDisplayValue(), gr_engagements.u_engagement_number.toString(), gr_engagements.sys_id.toString(), gr_engagements.u_start_date.getDisplayValue(), gr_engagements.u_end_date.getDisplayValue(), gr_engagements.u_engagement_short_name.getDisplayValue());
    	data.engagementList.push(engagementObj);
    }
    */

    var alreadyAdded = false;
    for (var e in data.engagementsadded) {
      if (e.number === gr_engagements.u_engagement_number.getDisplayValue()) {
        alreadyAdded = true;
        break;
      }
    }
    if (!alreadyAdded) {
      var engagementObj = engagementConstructor(gr_engagements.u_client.u_client_short_name.getDisplayValue(), gr_engagements.u_engagement_number.toString(), gr_engagements.sys_id.toString(), gr_engagements.u_start_date.getDisplayValue(), gr_engagements.u_end_date.getDisplayValue(), gr_engagements.u_engagement_short_name.getDisplayValue(), gr_engagements.u_general_engagement.getDisplayValue());
      data.engagementList.push(engagementObj);
    }

  }

  console.log(data);
})();
