//PIECHART 
function() {
	/* widget controller */
	var c = this;
	c.heigth = (document.height !== undefined) ? document.height : document.body.offsetHeight;
	//c.width = (document.getElementById('blankDiv').width !== undefined) ? document.getElementById('blankDiv').width : document.getElementById('blankDiv').offsetWidth;
	//c.width = c.width-100;
	c.heigth2 = c.heigth + 20;
	/* Let's apply some CSS to the dashboard but only after it is fully loaded in the page */

	var process4 = function(){
		var iframe = document.getElementById('report_section3');
		var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
		if (iframe == undefined){
			setTimeout(process4, 100);
		}else{
			var iframe2 = innerDoc.getElementById("tabIframe");
			if (iframe2 == undefined){
				setTimeout(process4, 100);
			}else{
				var innerDoc2 = iframe2.contentDocument || iframe2.contentWindow.document;
				var score = innerDoc2.getElementsByClassName('highcharts-series');

				if(score[0] == undefined){
					setTimeout(process4, 100);
				}else{
					applyCSS4();
					return;
				}
			}
		}
	}
	process4();

		var applyCSS4 = function(){

		/* First we need to "dive" into the iframe which contains all the graphs */
		var iframe = document.getElementById('report_section3');

		var innerDoc = iframe.contentDocument || iframe.contentWindow.document;

		var iframe2 = innerDoc.getElementById("tabIframe");
		var innerDoc2 = iframe2.contentDocument || iframe2.contentWindow.document;
		var table = innerDoc2.getElementsByClassName("list2_body")[0];
		var containerHeigth = innerDoc2.getElementsByClassName('sn-canvas-left')[0];
		var gridContainer = innerDoc2.getElementsByClassName('grid-stack-container')[0];

		var link = document.createElement('link');
    link.type = 'text/css';
    link.rel = 'stylesheet';
    link.href = 'https://fonts.googleapis.com/css?family=Open+Sans&amp;v=13c6d05edbb83200fb8879600f9619f5';
    innerDoc2.head.appendChild(link);

		/* After that, lets apply some CSS to remove or adapt what we want*/

		containerHeigth.style.setProperty('height','810px');
		containerHeigth.style.setProperty('overflow','hidden');
		gridContainer.style.setProperty('border-width','inherit');
		table.style.setProperty('pointer-events','none','important');
		innerDoc2.getElementsByClassName("grid-stack")[0].style.setProperty('font-family','Open Sans');
		innerDoc2.getElementsByClassName("grid-stack")[0].style.setProperty('height','1px','important');
		innerDoc2.getElementsByClassName("grid-stack")[0].children[0].style.setProperty('width','102%','important');
		innerDoc2.getElementsByClassName("grid-stack")[0].children[0].style.setProperty('height','67px','important');
		innerDoc2.getElementsByClassName("grid-stack")[0].children[0].style.setProperty('margin-left','-5px');
		//innerDoc2.getElementsByClassName("grid-stack")[0].children[0].children[0].style.setProperty('background-color','black');
		innerDoc2.getElementsByClassName("grid-stack")[0].children[0].children[0].style.setProperty('height','65px','important');
		innerDoc2.getElementsByClassName("grid-stack")[0].children[0].style.setProperty('color','white');
		//innerDoc2.getElementsByClassName("select2-container")[0].children[0].style.setProperty('background-color','#232526');
		//innerDoc2.getElementsByClassName("select2-container")[0].style.setProperty('background-color','black');
		//innerDoc2.getElementsByClassName("select2-container")[0].children[0].children[0].style.setProperty('background-color','#232526');
		innerDoc2.getElementsByClassName("select2-container")[0].children[0].children[0].style.setProperty('color','black');
		innerDoc2.getElementsByClassName("grid-stack")[0].children[1].style.setProperty('width','50%','important');
		innerDoc2.getElementsByClassName("grid-stack")[0].children[1].style.setProperty('left','50%','important');
		innerDoc2.getElementsByClassName("grid-widget-content")[1].style.setProperty('margin-top','-17px','important');
		innerDoc2.getElementsByClassName("grid-widget-content")[0].style.setProperty('margin-top','-6px','important');
		innerDoc2.getElementsByTagName("p")[0].children[0].style.setProperty('font-family','Open Sans');
		innerDoc2.getElementsByTagName("p")[0].children[0].style.setProperty('font-size','smaller');
		innerDoc2.getElementsByTagName("p")[0].children[0].style.setProperty('font-weight','bold');
		innerDoc2.getElementsByTagName("p")[0].children[0].style.setProperty('color','black');
		innerDoc2.getElementsByTagName("p")[0].style.setProperty('margin-top','-17px');
		innerDoc2.getElementsByTagName("p")[0].style.setProperty('position','fixed');
		innerDoc2.getElementsByTagName("p")[0].style.setProperty('margin-left','18px');
		innerDoc2.getElementsByClassName("select2-container")[0].children[0].style.setProperty('border-color','#53565a');
		innerDoc2.getElementsByClassName("select2-container")[0].children[0].style.setProperty('border-radius','4px');
		innerDoc2.getElementsByTagName("p")[0].children[0].style.setProperty('letter-spacing','0.2px');

		innerDoc2.getElementsByClassName("grid-stack")[0].children[0].appendChild(innerDoc2.getElementsByClassName("grid-stack")[0].children[1]);


		var headerPerGraph = innerDoc2.getElementsByClassName('grid-widget-header');
		var whiteSpaceHeader = innerDoc2.getElementsByName('search');
		var infoBody = innerDoc2.getElementsByClassName('list_decoration_cell col-small col-center ');
		var infoIcon = innerDoc2.getElementsByClassName('btn btn-icon table-btn-lg icon-info list_popup');
		var headerWhite = innerDoc2.getElementsByClassName(' col-control');

		while(headerWhite[0]) {
				headerWhite[0].parentNode.removeChild(headerWhite[0]);
		}

		while(headerPerGraph[0]) {
				headerPerGraph[0].parentNode.removeChild(headerPerGraph[0]);
		}

		while(whiteSpaceHeader[0]) {
				whiteSpaceHeader[0].parentNode.removeChild(whiteSpaceHeader[0]);
		}

		while(infoBody[0]){
			infoBody[0].parentNode.removeChild(infoBody[0]);
		}
		while(infoIcon[0]){
			infoIcon[0].parentNode.removeChild(infoIcon[0]);
		}

		for (var idk = 0; idk < innerDoc2.getElementsByClassName("vt").length; idk++){
			innerDoc2.getElementsByClassName("vt")[idk].style.setProperty('color','#97999b');
			innerDoc2.getElementsByClassName("vt")[idk].style.setProperty('letter-spacing','0.2px');
			innerDoc2.getElementsByClassName("vt")[idk].style.setProperty('font-size','12px');
			innerDoc2.getElementsByClassName("vt")[idk].style.setProperty('padding-left','0px');
			innerDoc2.getElementsByClassName("vt")[idk].style.setProperty('font-weight','bolder');
		}

		var tab4 = innerDoc2.querySelectorAll("a[href^='u_lov_prof']");
		var tab3 = innerDoc2.querySelectorAll("a[href^='u_lov_comp']");
		//var tab1 = innerDoc2.querySelectorAll("a[href^='u_user_comp']");
		var tab2 = innerDoc2.querySelectorAll("a[href^='sys_user']");
		var categoryLabel = innerDoc2.querySelectorAll("th[glide_label^='Professional Category']");

			for (var ido = 0; ido < categoryLabel.length; ido++){
							categoryLabel[ido].style.setProperty('text-align','center');
			}

						for (var idj = 0; idj < tab4.length; idj++){
							tab4[idj].style.setProperty('color','#97999b');
							tab4[idj].style.setProperty('letter-spacing','0.2px');
							tab4[idj].style.setProperty('font-size','12px');
							tab4[idj].style.setProperty('font-weight','100');
							tab4[idj].parentElement.style.setProperty('text-align','center');


						}
/*
						for (var idm = 0; idm < tab1.length; idm++){
							tab1[idm].style.setProperty('color','#53565a');
							tab1[idm].style.setProperty('letter-spacing','0.2px');
							tab1[idm].style.setProperty('font-size','12px');
							tab1[idm].style.setProperty('font-weight','100');
						}*/

						for (var idf = 0; idf < tab3.length; idf++){
							tab3[idf].style.setProperty('color','#53565a');
							tab3[idf].style.setProperty('letter-spacing','0.2px');
							tab3[idf].style.setProperty('font-size','12px');
							tab3[idf].style.setProperty('font-weight','100');
						}


		for (var idi = 0; idi < tab2.length; idi++){
			tab2[idi].style.setProperty('color','#000000');
			tab2[idi].style.setProperty('letter-spacing','0.2px');
			tab2[idi].style.setProperty('font-size','12px');
		}

		innerDoc2.getElementsByClassName('text-align-left')[3].style.setProperty('width','100px');
		innerDoc2.getElementsByClassName('text-align-left')[3].children[0].children[0].innerHTML = 'Level';
		//innerDoc2.getElementsByClassName('text-align-left')[0].children[0].children[0].innerHTML = 'ID';
		innerDoc2.getElementsByClassName('text-align-left')[1].children[0].children[0].innerHTML = 'Group';
			innerDoc2.getElementsByClassName('text-align-left')[1].style.setProperty('width','10px');
		innerDoc2.getElementsByClassName('text-align-left')[2].children[0].children[0].innerHTML = 'Category';
			innerDoc2.getElementsByClassName('text-align-left')[2].style.setProperty('width','175px');
			innerDoc2.getElementsByClassName('text-align-left')[0].style.setProperty('width','200px');

		//document.getElementById('blankDiv').style.setProperty('height','0');
		//document.getElementById('loading').remove();

		/* This part of the script is used to detect any change in the dashboard*/
		var lastBodyHTML = "";
		var changes = 0;

		function responsiveCSS() {
				if (lastBodyHTML == "") {
						lastBodyHTML = innerDoc2.body.innerHTML;
				}

				if (lastBodyHTML != innerDoc2.body.innerHTML) {
						changes++;

						var iframe_temp1 = document.getElementById('report_section3');
						var innerDoc_temp1 = iframe_temp1.contentDocument || iframe_temp1.contentWindow.document;
						var iframe_temp2 = innerDoc_temp1.getElementById("tabIframe");
						var innerDoc_temp2 = iframe_temp2.contentDocument || iframe_temp2.contentWindow.document;

						var table = innerDoc_temp2.getElementsByClassName("list2_body")[0];
						table.style.setProperty('pointer-events','none','important');

						var whiteSpaceHeader = innerDoc_temp2.getElementsByName('search');
						var infoBody = innerDoc_temp2.getElementsByClassName('list_decoration_cell col-small col-center ');
						var infoIcon = innerDoc_temp2.getElementsByClassName('btn btn-icon table-btn-lg icon-info list_popup');
						var headerWhite = innerDoc_temp2.getElementsByClassName(' col-control');

						while(headerWhite[0]) {
								headerWhite[0].parentNode.removeChild(headerWhite[0]);
						}

						while(whiteSpaceHeader[0]) {
								whiteSpaceHeader[0].parentNode.removeChild(whiteSpaceHeader[0]);
						}

						while(infoBody[0]){
							infoBody[0].parentNode.removeChild(infoBody[0]);
						}

						while(infoIcon[0]){
							infoIcon[0].parentNode.removeChild(infoIcon[0]);
						}
						lastBodyHTML = innerDoc2.body.innerHTML;

						for (var idk = 0; idk < innerDoc2.getElementsByClassName("vt").length; idk++){
							innerDoc2.getElementsByClassName("vt")[idk].style.setProperty('color','black');
							innerDoc2.getElementsByClassName("vt")[idk].style.setProperty('letter-spacing','0.2px');
							innerDoc2.getElementsByClassName("vt")[idk].style.setProperty('font-size','12px');
							innerDoc2.getElementsByClassName("vt")[idk].style.setProperty('padding-left','0px');
							innerDoc2.getElementsByClassName("vt")[idk].style.setProperty('font-weight','bolder');
						}

						var categoryLabel = innerDoc2.querySelectorAll("th[glide_label^='Professional Category']");

						for (var ido = 0; ido < categoryLabel.length; ido++){
										categoryLabel[ido].style.setProperty('text-align','center');
						}

						var tab4 = innerDoc2.querySelectorAll("a[href^='u_lov_prof']");
						var tab3 = innerDoc2.querySelectorAll("a[href^='u_lov_comp']");
						//var tab1 = innerDoc2.querySelectorAll("a[href^='u_user_comp']");
						var tab2 = innerDoc2.querySelectorAll("a[href^='sys_user']");

						for (var idj = 0; idj < tab4.length; idj++){
							tab4[idj].style.setProperty('color','#97999b');
							tab4[idj].style.setProperty('letter-spacing','0.2px');
							tab4[idj].style.setProperty('font-size','12px');
							tab4[idj].style.setProperty('font-weight','100');
							tab4[idj].parentElement.style.setProperty('text-align','center');
						}
/*
						for (var idm = 0; idm < tab1.length; idm++){
							tab1[idm].style.setProperty('color','#53565a');
							tab1[idm].style.setProperty('letter-spacing','0.2px');
							tab1[idm].style.setProperty('font-size','12px');
							tab1[idm].style.setProperty('font-weight','100');
						}*/

						for (var idf = 0; idf < tab3.length; idf++){
							tab3[idf].style.setProperty('color','#53565a');
							tab3[idf].style.setProperty('letter-spacing','0.2px');
							tab3[idf].style.setProperty('font-size','12px');
							tab3[idf].style.setProperty('font-weight','100');
						}

						for (var idi = 0; idi < tab2.length; idi++){
							tab2[idi].style.setProperty('color','#000000');
							tab2[idi].style.setProperty('letter-spacing','0.2px');
							tab2[idi].style.setProperty('font-size','12px');
						}

					if (innerDoc_temp2.getElementsByClassName('text-align-left')[5] != undefined){
						if (innerDoc_temp2.getElementsByClassName('text-align-left')[5].children[0].children[0].className.includes('list_hdrcell')){
							innerDoc_temp2.getElementsByClassName('text-align-left')[7].style.setProperty('width','100px');
							innerDoc_temp2.getElementsByClassName('text-align-left')[7].children[0].children[0].innerHTML = 'Level';
							//innerDoc_temp2.getElementsByClassName('text-align-left')[5].children[0].children[0].innerHTML = 'ID';
							innerDoc_temp2.getElementsByClassName('text-align-left')[5].children[0].children[0].innerHTML = 'Group';
							innerDoc_temp2.getElementsByClassName('text-align-left')[6].children[0].children[0].innerHTML = 'Category';
							innerDoc_temp2.getElementsByClassName('text-align-left')[6].style.setProperty('width','175px');
			innerDoc_temp2.getElementsByClassName('text-align-left')[4].style.setProperty('width','200px');
							innerDoc_temp2.getElementsByClassName('text-align-left')[5].style.setProperty('width','10px');

						}else{
							var options = innerDoc_temp2.getElementsByClassName('icon-menu');
							while(options[0]) {
								options[0].parentNode.removeChild(options[0]);
							}
						}
					}
				}

			window.setTimeout(responsiveCSS, 0);
		}

		responsiveCSS();
	}
}
