(function() {
    /* populate the 'data' object */
    /* e.g., data.table = $sp.getValue('table'); */
    data.myTasks = {};
    data.notifications = [];
    data.toDos = [];
    data.alerts = [];
    data.myUserSysID = gs.getUserID();
    data.myUserName = gs.getUser().getFullName();
    data.dataToShow = false;
    data.alertsToShow = false;
    data.urlCompetencies = '';

    String.prototype.capitalizeFirstLetter = function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
    }

    var userID = gs.getUserID();
    var user = gs.getUser();
    console.log('UserID : ' + userID);
    var gr_user = new GlideRecord('sys_user');
    gr_user.addQuery('sys_id', userID);
    gr_user.query();
    if (gr_user.next()) {
        data.userInfo = {
            isStaff: user.isMemberOf("Staff"),
            competenciesState: gr_user.source.getDisplayValue(),
            isValidator: user.isMemberOf("Validators"),
            isManagement: user.isMemberOf("Management"),
            isPeopleConnect: user.isMemberOf("People Connect"),
            isHeadOfPeopleConnect: user.isMemberOf("Head of People Connect")
        };
    }



    console.log(data.userInfo);

    /*
    //notifications
    var gr_message = new GlideRecord('live_message');
    gr_message.addQuery('state', 'published');
    gr_message.query();
    while (gr_message.next()) {
    	var gr_group = new GlideRecord('live_group_member');
    	gr_group.addQuery('group', gr_message.group);
    	gr_group.addQuery('member.document', gs.getUserID());
    	gr_group.query();
    	while(gr_group.next()) {
    		// TODO Excluir admin e peopleconnectbot
    		var user = gr_group.member.document;
    		var message = gr_message.message.getDisplayValue();
    		var title = gr_message.context.getDisplayValue;
    		var notification = {title: title,
    												 description: message
    												}
    		data.notifications.push(notification)
    	}
    }*/

    //TODOs
    var gr_request = new GlideAggregate('sc_request');
    gr_request.addAggregate('COUNT', 'u_deadline');
    gr_request.addEncodedQuery('u_smart_main_process=competency^stage=fulfillment^ORu_state=pending');
    gr_request.addEncodedQuery('assigned_to !=""');
    gr_request.groupBy('assigned_to');
    gr_request.query();
    while (gr_request.next()) {
        var user = gr_request.assigned_to;
        var number_of_total_actions = gr_request.getAggregate('COUNT', 'u_deadline');

        var gr_request_filtered = new GlideAggregate('sc_request');
        gr_request_filtered.addEncodedQuery('u_smart_main_process=competency^stage=fulfillment^ORu_state=pending');
				gr_request_filtered.addQuery('short_description', '!=', '');
				gr_request_filtered.addQuery('u_urgency', '!=', '');
				gr_request_filtered.addQuery('u_deadline', '!=', '');
        gr_request_filtered.addAggregate('COUNT', 'u_deadline');
        gr_request_filtered.addQuery('assigned_to', user);
        gr_request_filtered.groupBy('u_smart_main_process');
        gr_request_filtered.groupBy('u_deadline');
        gr_request_filtered.groupBy('short_description');
        gr_request_filtered.groupBy('assigned_to');
        gr_request_filtered.groupBy('u_urgency');
        gr_request_filtered.groupBy('u_smart_url');
        gr_request_filtered.query();
        while (gr_request_filtered.next()) {
            var num_actions = gr_request_filtered.getAggregate('COUNT', 'u_deadline');
            var titleTodo = gr_request_filtered.short_description.getDisplayValue();
            //var description = gr_request_filtered.u_description.getDisplayValue();
            var timeLeft = gr_request_filtered.u_deadline.getDisplayValue();
            //var state = gr_request_filtered.u_status.getDisplayValue();
            //var startDate = gr_request_filtered.u_start_date.getDisplayValue();
            //var dueDate = gr_request_filtered.u_priority_status.getDisplayValue();
            var area = gr_request_filtered.u_smart_main_process.getDisplayValue();
            //var daysLeft = gr_request_filtered.u_deadline.getDisplayValue();
            var priority = gr_request_filtered.getValue('u_urgency');
            var userID = gr_request_filtered.getValue('assigned_to');
            var url = gr_request_filtered.u_smart_url.getDisplayValue();
            var actions = num_actions;

            if (data.myUserSysID === userID && !data.dataToShow) {
                data.dataToShow = true;
            }

            var todo = {
                title: titleTodo,
                //description: description,
                priority: priority,
                timeLeft: timeLeft,
                //state: state,
                //startDate: startDate,
                //dueDate: dueDate,
                area: area,
                user: userID,
                url: url,
                //daysLeft: daysLeft
                actions: actions
            }
            data.toDos.push(todo);
        }
    }

	console.log(data.toDos);
    data.toDos.sort(function(a, b) {
			if(a.priority == 'worrisome') a.priority = "req_" + a.priority;
			if(b.priority == 'worrisome') b.priority = "req_" + b.priority;
        if (a.priority < b.priority) return 1;
        if (a.priority > b.priority) return -1;
        return 0;
    })

    /* TODO: Add alerts to the page based on a future alert table


    //Alerts
    var gr_alerts = new GlideRecordSecure('u_homepage_todos');
    gr_alerts.addActiveQuery();
    gr_alerts.addEncodedQuery('u_status!=complete');
    gr_alerts.orderBy('u_priority_status');
    gr_alerts.orderBy('u_due_date');
    gr_alerts.query();
    while (gr_alerts.next()) {
    	var titleA = gr_alerts.u_title.getDisplayValue();
    	var descriptionA = gr_alerts.u_description.getDisplayValue();
    	var priorityA = gr_alerts.u_priority_status.getDisplayValue();
    	var stateA = gr_alerts.u_state.getDisplayValue();
    	var startDateA = gr_alerts.u_start_date.getDisplayValue();
    	var dueDateA = gr_alerts.u_due_date.getDisplayValue();
    	var areaA = gr_alerts.u_area.getDisplayValue();
    	var daysLeft = gr_alerts.u_days_left_to_complete_task.getDisplayValue();
    	var userIDA = gr_alerts.u_task_owner;

    	var alert = {title: titleA,
    							description: descriptionA,
    							priority: priorityA,
    							state: stateA,
    							startDate: startDateA,
    							dueDate: dueDateA,
    							daysLeft: daysLeft,
    							area: areaA,
    							user: userIDA
    						 }
    	data.alerts.push(alert);
    }
    */


    console.log(data.toDos);

})();
