function ($rootScope, $scope, snRecordWatcher, spUtil, $location, $uibModal, cabrillo, $timeout) {

	$rootScope.$broadcast("homePage");

	$timeout(function() {
	    $('body').tooltip({
	      selector: '[data-toggle="tooltip"]',
				trigger : 'hover'
	    });
	  }, 500);

	$scope.myTodosForThisWeek = function() {
		return $scope.data.toDos
		.filter(toDo => toDo.user === $scope.data.myUserSysID && ((toDo.priority == 'urgent') || (toDo.priority == 'req_worrisome')));
	}

	$scope.myTodosForLater = function() {
		return $scope.data.toDos
		.filter(toDo => toDo.user === $scope.data.myUserSysID && ((toDo.priority != 'urgent') && (toDo.priority != 'req_worrisome')));
	}

	$scope.myAlerts = function() {
		return $scope.data.alerts
		.filter(alert => alert.user === $scope.data.myUserSysID);
	}

	 $scope.showModal = function(idDiv) {
		$('#'+idDiv).modal('show');
	}

	$("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
  });

	$scope.pauseVideo = function(vid){
		$('#'+vid).attr('src', $('#'+vid).attr('src'));
	}

	$scope.stopVideo = function (vide){
		$('#'+vid).remove();
	}

	$(document).keyup(function(e) {
     if (e.keyCode == 27) {
        $('#vidHome').attr('src', $('#vidHome').attr('src'));
				$('#vidDash').attr('src', $('#vidDash').attr('src'));
    }
	});

	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
				$('#vidHome').remove();
				$('#vidDash').remove();
   }

}
//# sourceURL=home.js
