function controller($scope, $rootScope, $http, $timeout) {
  /* widget controller */

  //############ LIMITS ###########

  $scope.titleCharLimit = 45;
  $scope.subtitleCharLimit = 60;

  //############ FLATTENING STRUCTURE ##############
  var props = ["cats", "subs", "comps"];

  function flatten(curName, path, obj) {

    var availableProps = props.filter(function(p) {
      return obj[p];
    });


    if (availableProps.length === 0) {
      obj.prettyName = curName.join(" > ");
      obj.path = path;
      return [obj];
    }

    var prop = availableProps[0];
    var tmp = [];
    var ys = obj[prop];
    for (var i = 0; i < obj[prop].length; i++) {
      var y = obj[prop][i];
      var newCurName = curName.concat([obj.name]);

      tmp = tmp.concat(flatten(newCurName, path.concat([obj]), y));
    }
    return tmp;

  }

  //############ FILTER ###########

  $scope.competenciesFilter = function(comp) {
    if (comp.macroName != $scope.macro.name) return false;
    if ($scope.macro.numOfHierarchies === 3) {
      return comp.path[1].name === $scope.macro.cats[$scope.currentCatIndex].name && comp.path[2].name === comp.path[1].subs[$scope.currentSubIndex].name;
    }
    if ($scope.macro.numOfHierarchies === 2) {
      return comp.path[1].name === $scope.macro.subs[$scope.currentSubIndex].name;
    }
    return true;
  };

	//######### PAGE NOTIF ###########

	$rootScope.$broadcast("onbordingPage");


  var c = this;
  var answered;
  var percentageAnswered;
  $scope.lastSet = false;
  $scope.checkEveryRadio = false;
  $scope.showBackButton = false;
  $scope.currentPage = 1;
  $scope.maxPage = 1;
  $scope.fullFilled = false;

  //$rootScope.$broadcast('hideBottomBar');

  //event listeners
  $rootScope.$on('sendDataStructure', function(event, data) {
    $scope.data = data[0];
		$rootScope.$broadcast("competencyScale",$scope.data.competencyScale);
    $scope.employeeType = $scope.data.userEmployeeType;
    $scope.currentMacroIndex = data[1];
    $scope.currentCatIndex = data[2];
    $scope.currentSubIndex = data[3];
    $scope.initMacro = $scope.data.startPage.macro;
    $scope.initCat = $scope.data.startPage.cat;
    $scope.initSub = $scope.data.startPage.sub;
    if ($scope.initMacro !== 0 || $scope.initCat !== 0 || $scope.initSub !== 0) {
      $scope.showBackButton = true;
    }
    $scope.macro = $scope.data.competencies[$scope.currentMacroIndex];
    $scope.finalMacro = $scope.data.competencies.length - 1;
    $scope.finalHierarchy = $scope.data.competencies[$scope.finalMacro].numOfHierarchies;
    if ($scope.finalHierarchy === 3) {
      $scope.finalCat = $scope.data.competencies[$scope.finalMacro].cats.length - 1;
      $scope.finalSub = $scope.data.competencies[$scope.finalMacro].cats[$scope.finalCat].subs.length - 1;
    } else if ($scope.finalHierarchy === 2) {
      $scope.finalCat = 0;
      $scope.finalSub = $scope.data.competencies[$scope.finalMacro].subs.length - 1;
    } else { //finalHierarchy === 1
      $scope.finalCat = 0;
      $scope.finalSub = 0;
    }
    $scope.totalCompetencies = $scope.data.numOfCompetencies;
    $scope.answeredCompetencies = $scope.data.answersCounter;
    //reloading in the last page
    if ($scope.finalMacro === $scope.initMacro && $scope.finalCat === $scope.initCat && $scope.finalSub === $scope.initSub) {
      $scope.lastSet = true;
      $rootScope.$broadcast('activateSubmit');
      setProgress(100);
      $scope.fullFilled = true;
    } else {
      //need to remove the number of answers in the current page
      $scope.removeCurrentPageAnswers();
      percentageAnswered = ($scope.answeredCompetencies / $scope.totalCompetencies) * 100;
      setProgress(Math.floor(percentageAnswered));
    }

    var tmp = [];
    for (var i = 0; i < $scope.data.competencies.length; i++) {
      var macro = $scope.data.competencies[i];
      var toAdd = flatten([], [], macro);
      toAdd.forEach(function(e) {
        e.macroName = macro.name;
      });
      tmp = tmp.concat(toAdd);
    }
    $scope.data.flattenCompetencies = tmp;

  });

  $scope.applyCssToTitles = function() {
    for (var idx = 0; 0 < document.getElementsByClassName('competencyblock').length; idx++) {
      if (document.getElementsByClassName('competencyblock')[idx] == undefined) {
        break;
      }
      var divHeight = document.getElementsByClassName('competencyblock')[idx].clientHeight;
      var midHeight = Math.round(((100 - divHeight) / 2) - 7);
      midHeight = midHeight.toString() + 'px';
    }
  }

  $scope.removeCurrentPageAnswers = function() {
    answered = 0;
    var macro = $scope.data.competencies[$scope.currentMacroIndex];
    if (macro.numOfHierarchies === 1) {
      for (var i in macro.comps) {
        if (macro.comps[i].value !== '') {
          answered++;
        }
      }
    } else if (macro.numOfHierarchies === 2) {
      for (var i in macro.subs[$scope.currentSubIndex].comps) {
        if (macro.subs[$scope.currentSubIndex].comps[i].value !== '') {
          answered++;
        }
      }
    } else { //numOfHierarchies === 3
      for (var i in macro.cats[$scope.currentCatIndex].subs[$scope.currentSubIndex].comps) {
        if (macro.cats[$scope.currentCatIndex].subs[$scope.currentSubIndex].comps[i].value !== '') {
          answered++;
        }
      }
    }
    $scope.answeredCompetencies = $scope.answeredCompetencies - answered;
  }

  //set the 'value' property to the radio button values in the last page
  $scope.setValue = function() {
    answered = 0;
    var macro = $scope.data.competencies[$scope.currentMacroIndex];
    if (macro.numOfHierarchies === 1) {
      answered = macro.comps.length;
      for (var i in macro.comps) {
        macro.comps[i].value = macro.comps[i].approvedValue;
      }
    } else if (macro.numOfHierarchies === 2) {
      answered = macro.subs[$scope.currentSubIndex].comps.length;
      for (var i in macro.subs[$scope.currentSubIndex].comps) {
        macro.subs[$scope.currentSubIndex].comps[i].value = macro.subs[$scope.currentSubIndex].comps[i].approvedValue;
      }
    } else { //numOfHierarchies === 3
      answered = macro.cats[$scope.currentCatIndex].subs[$scope.currentSubIndex].comps.length;
      for (var i in macro.cats[$scope.currentCatIndex].subs[$scope.currentSubIndex].comps) {
        macro.cats[$scope.currentCatIndex].subs[$scope.currentSubIndex].comps[i].value = macro.cats[$scope.currentCatIndex].subs[$scope.currentSubIndex].comps[i].approvedValue;
      }
    }
  }

  $scope.next = function() {
    $('html, section').animate({
      scrollTop: 0
    }, 'slow');
    $scope.setValue();
    $scope.currentPage++;
    $scope.showBackButton = true;
    if ($scope.currentPage > $scope.maxPage) {
      $scope.answeredCompetencies += answered;
      percentageAnswered = ($scope.answeredCompetencies / $scope.totalCompetencies) * 100;
      setProgress(Math.floor(percentageAnswered));
      $scope.maxPage = $scope.currentPage;
    }
    $scope.macro = $scope.data.competencies[$scope.currentMacroIndex];
    if ($scope.macro.numOfHierarchies === 1) {
      $scope.currentMacroIndex++;
      $scope.currentCatIndex = 0;
      $scope.currentSubIndex = 0;
      $rootScope.$broadcast('incrementMacro');
      $rootScope.$broadcast('resetCat');
      $rootScope.$broadcast('resetSub');
    } else if ($scope.macro.numOfHierarchies === 2) {
      $scope.currentSubIndex++;
      if ($scope.currentSubIndex > $scope.macro.subs.length - 1) {
        $scope.currentMacroIndex++;
        $scope.currentCatIndex = 0;
        $scope.currentSubIndex = 0;
        $rootScope.$broadcast('incrementMacro');
        $rootScope.$broadcast('resetCat');
        $rootScope.$broadcast('resetSub');
      } else {
        $rootScope.$broadcast('incrementSub');
      }
    } else { //numOfHierarchies === 3
      $scope.currentSubIndex++;
      if ($scope.currentSubIndex > $scope.macro.cats[$scope.currentCatIndex].subs.length - 1) {
        $scope.currentCatIndex++;
        $scope.currentSubIndex = 0;
        if ($scope.currentCatIndex > $scope.macro.cats.length - 1) {
          $scope.currentMacroIndex++;
          $scope.currentCatIndex = 0;
          $scope.currentSubIndex = 0;
          $rootScope.$broadcast('incrementMacro');
          $rootScope.$broadcast('resetCat');
          $rootScope.$broadcast('resetSub');
        } else {
          $rootScope.$broadcast('incrementCat');
          $rootScope.$broadcast('resetSub');
        }
      } else {
        $rootScope.$broadcast('incrementSub');
      }
    }
    $scope.macro = $scope.data.competencies[$scope.currentMacroIndex];
    if ($scope.currentMacroIndex === $scope.finalMacro) {
      if ($scope.finalHierarchy === 3) {
        if ($scope.currentCatIndex === $scope.finalCat) {
          if ($scope.currentSubIndex === $scope.finalSub) {
            $scope.lastSet = true;
          }
        }
      } else if ($scope.finalHierarchy === 2) {
        if ($scope.currentSubIndex === $scope.finalSub) {
          $scope.lastSet = true;
        }
      } else {
        $scope.lastSet = true;
      }
    }
    if ($scope.lastSet) {
      $rootScope.$broadcast('activateSubmit');
      setProgress(100);
      $scope.fullFilled = true;
    }
    console.log('' + $scope.answeredCompetencies + ' / ' + $scope.totalCompetencies)
  }

  $scope.back = function() {
    $('html, section').animate({
      scrollTop: 0
    }, 'slow');
    $scope.currentPage--;
    $scope.macro = $scope.data.competencies[$scope.currentMacroIndex];
    if ($scope.macro.numOfHierarchies === 1) {
      $scope.currentMacroIndex--;
      $scope.macro = $scope.data.competencies[$scope.currentMacroIndex];
      if ($scope.macro.numOfHierarchies === 3) {
        $scope.currentCatIndex = $scope.macro.cats.length - 1;
        $scope.currentSubIndex = $scope.macro.cats[$scope.currentCatIndex].subs.length - 1;
        $rootScope.$broadcast('decrementMacro');
        $rootScope.$broadcast('getLastCat');
        $rootScope.$broadcast('getLastSub');
      } else if ($scope.macro.numOfHierarchies === 2) {
        $scope.currentSubIndex = $scope.macro.subs.length - 1;
        $rootScope.$broadcast('decrementMacro');
        $rootScope.$broadcast('getLastSub');
      } else {
        $rootScope.$broadcast('decrementMacro');
      }
    } else if ($scope.macro.numOfHierarchies === 2) {
      $scope.currentSubIndex--;
      if ($scope.currentSubIndex < 0) {
        $scope.currentMacroIndex--;
        $scope.macro = $scope.data.competencies[$scope.currentMacroIndex];
        if ($scope.macro.numOfHierarchies === 3) {
          $scope.currentCatIndex = $scope.macro.cats.length - 1;
          $scope.currentSubIndex = $scope.macro.cats[$scope.currentCatIndex].subs.length - 1;
          $rootScope.$broadcast('decrementMacro');
          $rootScope.$broadcast('getLastCat');
          $rootScope.$broadcast('getLastSub');
        } else if ($scope.macro.numOfHierarchies === 2) {
          $scope.currentSubIndex = $scope.macro.subs.length - 1;
          $rootScope.$broadcast('decrementMacro');
          $rootScope.$broadcast('getLastSub');
        } else {
          $rootScope.$broadcast('decrementMacro');
        }
      } else {
        $rootScope.$broadcast('decrementSub');
      }
    } else {
      $scope.currentSubIndex--;
      if ($scope.currentSubIndex < 0) {
        $scope.currentCatIndex--;
        if ($scope.currentCatIndex < 0) {
          $scope.currentMacroIndex--;
          $scope.macro = $scope.data.competencies[$scope.currentMacroIndex];
          if ($scope.macro.numOfHierarchies === 3) {
            $scope.currentCatIndex = $scope.macro.cats.length - 1;
            $scope.currentSubIndex = $scope.macro.cats[$scope.currentCatIndex].subs.length - 1;
            $rootScope.$broadcast('decrementMacro');
            $rootScope.$broadcast('getLastCat');
            $rootScope.$broadcast('getLastSub');
          } else if ($scope.macro.numOfHierarchies === 2) {
            $scope.currentSubIndex = $scope.macro.subs.length - 1;
            $rootScope.$broadcast('decrementMacro');
            $rootScope.$broadcast('getLastSub');
          } else {
            $rootScope.$broadcast('decrementMacro');
          }
        } else {
          $rootScope.$broadcast('decrementCat');
          $scope.currentSubIndex = $scope.macro.cats[$scope.currentCatIndex].subs.length - 1;
          $rootScope.$broadcast('getLastSub');
        }
      } else {
        $rootScope.$broadcast('decrementSub');
      }
    }
    if ($scope.currentMacroIndex <= 0 && $scope.currentCatIndex <= 0 && $scope.currentSubIndex <= 0) {
      $scope.showBackButton = false;
    }
    $scope.macro = $scope.data.competencies[$scope.currentMacroIndex];
    if ($scope.lastSet) {
      $scope.lastSet = false;
    }
  }

	$scope.submit = function () {
		$rootScope.$broadcast('submitOnboarding');
	}

	$scope.save = function () {
		$rootScope.$broadcast('saveOnboarding');
	}
}

function setProgress(value) {
  var elem = document.getElementById("myBar");
  var elemMobile = document.getElementById("myBarMobile");
  elem.style.width = value + '%';
  elemMobile.style.width = value + '%';
  document.getElementById("progressLabel").innerHTML = value * 1 + '%';
}


//# sourceURL=onboardingWorkspace.js
