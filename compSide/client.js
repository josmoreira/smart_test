function($scope,$rootScope,$http) {
  /* widget controller */
  var c = this;

	//event listeners
	$rootScope.$on('sendDataStructure', function(event,data) {
		$scope.data = data;
  });

	$rootScope.$on('noFilter', function(event,data) {
		$scope.macroIndex = '';
  });

	$rootScope.$on('showCategories', function(event,data) {
		$scope.macroIndex = data;
		$scope.macroCategory = $scope.data.competencies[$scope.macroIndex].name;
  });

	//collapse controllers
	$scope.openCatName = undefined;
	$scope.openCat = function(catName){
		if ($scope.openCatName == catName){
			$scope.openCatName = undefined;
		}
		else {
			$scope.openCatName = catName;
		}
		$scope.openSubCatName = undefined;
	}

	$scope.openSubCatName = undefined;
	$scope.openSubCat = function(subName){
		if ($scope.openSubCatName == subName){
			$scope.openSubCatName = undefined;
		}
		else{
			$scope.openSubCatName = subName;
		}
	}

	//event broadcasters
	$scope.sendCompetency = function(selection){
		if (selection[0] !== 'na') {
			console.log("You already have that competency.");
			$rootScope.$broadcast('scrollToMyComp', selection[2]); //selection[2] is the sys_id
			return;
		}
		var compListIndex = selection[4];
		var comp = selection[5];
		var sub = selection[6];
		var cat = selection[7];
		var macro = selection [8]
		if (macro == undefined) {
			macro = $scope.macroCategory;
		}

		console.log('broadcast');
		selection.debug = "ola";
		$rootScope.$broadcast('sendCompetency', selection);

		//level 1 hierarchy competency
		if((cat == undefined && sub == undefined) || (macro === sub)) {
			var macroIndex = $scope.data.indexes[macro].index;
			var compIndex = $scope.data.indexes[macro][comp].index;
			$scope.data.competencies[macroIndex].comps[compIndex].approvalState = 'added';
		}
		//level 2 hierarchy competency
		else if ((cat == undefined) || (macro === cat)) {
			var macroIndex = $scope.data.indexes[macro].index;
			var subIndex = $scope.data.indexes[macro][sub].index;
			var compIndex = $scope.data.indexes[macro][sub][comp].index;
			$scope.data.competencies[macroIndex].subs[subIndex].comps[compIndex].approvalState = 'added';
		}
		//level 3 hierarchy competency
		else {
			var macroIndex = $scope.data.indexes[macro].index;
			var catIndex = $scope.data.indexes[macro][cat].index;
			var subIndex = $scope.data.indexes[macro][cat][sub].index;
			var compIndex = $scope.data.indexes[macro][cat][sub][comp].index;
			$scope.data.competencies[macroIndex].cats[catIndex].subs[subIndex].comps[compIndex].approvalState = 'added';
		}
		$scope.data.competenciesList[compListIndex].approvalState = 'added';
  };

	var go = document.getElementById("go");
	searchBar = document.getElementById("searchBar");
	outige = document.getElementById("outige");

	if (searchBar)    {
		searchBar.addEventListener("keypress", function (e) {
			if (event.keyCode === 13)   {
				go.click();
				e.preventDefault();
			}
		});
		go.addEventListener("click", function () {
			//document.getElementById("myInput").value = $('#searchBar').val();
			searchEnabler($('#searchBar').val());
		});
	}

	function searchEnabler(filter) {
		if (filter.length === 0) {
			return;
		}
		var input, ul, li, a, i, uls, parsedName, ulMenu, compsUl;
		filter = filter.toUpperCase();
		//hides the current side menu categories and subcategories
		ulMenu = document.getElementsByClassName("sideUl");
		for (i = 0; i < ulMenu.length; i++) {
			ulMenu[i].style.display = "none";
		}
		//hides the current side menu competencies
		compsUl = document.getElementsByClassName("compsUl");
		for (i = 0; i < compsUl.length; i++) {
			compsUl[i].style.display = "none";
		}
		ul = document.getElementById("searchResults");
		ul.style.display = "";
		li = ul.getElementsByTagName("li");
		uls = ul.getElementsByTagName("ul");
		if(filter.length != 0){
			for (i = 0; i < li.length; i++) {
				a = li[i].getElementsByClassName("searchTerm")[0];
				if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
					li[i].style.display = "";
				} else {
					li[i].style.display = "none";
				}
			}
		}
		else{
			for (i = 0; i < li.length; i++) {
				li[i].style.display = "none";
			}
		}
	}
}

//# sourceURL=compSide.js
