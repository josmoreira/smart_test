function controller($timeout, $scope, $rootScope, $http) {
	/* widget controller */
	var c = this;

  $rootScope.disableSubmit = true;

	$scope.allowSubmit = function () {
		$rootScope.disableSubmit = false;
	};

	$scope.showingDelegated = false;

	$scope.allSelected = false;//all entries are selected

	$scope.characterLimit = isMobile() ? 32 : 45;

	$rootScope.$broadcast("approvalsPage");

	if ($scope.data.homepage_navigation){
		$('#myModal').modal('show');
		var articleID = $scope.data.user_navigation_details.articleID;
		var article_description = JSON.stringify($scope.data.user_navigation_details.article);
		var updateArticle = {};
		updateArticle.sys_id = articleID;
		updateArticle.description = article_description;

	/*$http({
					method:'PUT',
					url: 'api/now/table/kb_knowledge/' + articleID,
					headers: {'Content-Type' : 'application/json'},
					data : updateArticle
		});*/

		$http({
			method: 'PUT',
			url: $scope.data.instanceURL + 'api/dnsa/update_user_navigation/update',
			headers: { 'Content-Type': 'application/json' },
			data: updateArticle
		});
	}else{
		$('#vidNav').remove();
	}

	$scope.stopVideo = function (vide){
		$('#'+vide).remove();
	};

	$(document).keyup(function(e) {
     if (e.keyCode == 27) {
        $('#vidNav').attr('src', $('#vidNav').attr('src'));
    }
	});

	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      	$('#vidNav').remove();
  }


	function isMobile() {
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            return true;
        }
        return false;
    }


	if(isMobile()){
		$(".col-lg-4").hide();//hideDashboard
	}

	$timeout(function () {
		$rootScope.$broadcast('sendDataStructure', $scope.data);
		$('body').tooltip({
			selector: '[data-toggle="tooltip"]',
			trigger : 'hover'
    });
		if ($scope.data.myApprovals.length > 0) {
			$scope.selectedComp = $scope.data.viewAllInfo;
		} else {
			return;
		}
	}, 500);

	$scope.toggleDelegated = function () {
		$rootScope.$broadcast('toggleDelegated');
		$scope.showingDelegated = !$scope.showingDelegated;
	};

	$scope.viewAll = function () {
		$rootScope.$broadcast('viewAll');
		$scope.selectedComp = $scope.data.viewAllInfo;
	};

	$scope.selectedCompetencyChanged = function (index, label) {
		$scope.selectedComp = $scope.data.competenciesList[index];
		$rootScope.$broadcast('sendCompetencyLabel', label);
	};

	//http request to save the state of all approvals
	$scope.save = function () {
		$rootScope.$broadcast("showNotification", {message: "Competencies Saved", image:"glyphicon-ok"});
		var saveJson = '';
		for (var approval in $scope.data.myApprovals) {
			var approvalObj = $scope.data.myApprovals[approval];
			if (approvalObj.u_final_proficiency !== '') {
				if (saveJson !== '') {
					saveJson += ';';
				}
				var editedCompetencyJson = '{"approvalID": "' + approvalObj.sys_id + '", "u_final_proficiency": "' + approvalObj.u_final_proficiency + '", "u_state": "pending"}';
				saveJson += editedCompetencyJson;
			}
		}
		$scope.httpPut(saveJson);
	};

	$scope.submit = function () {

		$rootScope.$broadcast("showNotification", {message: "Approvals Submitted", image:"glyphicon-ok"});
		var submitJson = '';
		var proficiency;
		for (var approval in $scope.data.myApprovals) {
			var approvalObj = $scope.data.myApprovals[approval];
			if (approvalObj.checked) {
				if (approvalObj.u_final_proficiency === '') {
					proficiency = approvalObj.u_proposed_proficiency;
				} else {
					proficiency = approvalObj.u_final_proficiency;
				}
				if (submitJson !== '') {
					submitJson += ';';
				}
				approvalObj.u_state = 'approved';
				var editedCompetencyJson = '{"approvalID": "' + approvalObj.sys_id + '", "u_final_proficiency": "' + proficiency + '", "u_state": "approved"}';
				submitJson += editedCompetencyJson;

				$scope.data.viewAllInfo.totalApprovals--;
				$scope.data.competenciesList[approvalObj.listIndex].totalApprovals--;

				if (approvalObj.u_due_date <= 1) {
					$scope.data.viewAllInfo.urgentsCounter--;
					$scope.data.competenciesList[approvalObj.listIndex].urgentsCounter--;
				}
				approvalObj.checked = false;
			}
		}
		$scope.httpPut(submitJson);
    $rootScope.disableSubmit = true;
		$rootScope.$broadcast("submitAll");
	};

	$scope.httpPut = function (competenciesJSON) {
		$http({
			method: 'PUT',
			url: $scope.data.instanceURL + 'api/dnsa/update_competencies/update_approvals', 
			headers: { 'Content-Type': 'application/json' },
			data: competenciesJSON
		})
			.success(function (response, status, headers, config) {
				console.log(response);
			});
	};

	$scope.showPieTablet = function () {
		$rootScope.$broadcast("showPieTablet");
	};

	//Checkbox stufff


	$scope.$watch("allSelected",function(){
		$rootScope.$broadcast("setChecks",$scope.allSelected);
	});

	$scope.$on("allowSubmit", function() {
		$scope.allowSubmit();
	});

	$scope.$on("disableChecks",function(){
		$("#checkboxAll").prop("checked",false);
	});

	$scope.$on("enableChecks",function(){
		$("#checkboxAll").prop("checked",true);
	});

	$scope.$on('saveButtonClick',function(){
		$scope.save();
	});

	$scope.$on('submitAllButtonClick',function(){
		$scope.submit();
	});

}

$(document).on("click", ".toggleDelegate", function () {
	jQuery(this).toggleClass('active');
});


//# sourceURL=approvalsTopBar.js
