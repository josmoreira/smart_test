(function() {

	data.instanceURL = gs.getProperty('glide.servlet.uri');
	data.myApprovals = [];
	data.competenciesList = [];
	data.viewAllInfo = {urgentsCounter: 0,
										  totalApprovals: 0,
										  label: 'View All'
										 };
	data.myUserSysID = gs.getUserID();
	data.myUserName = gs.getUser().getFullName();
	data.name = '';

	var competency;
	var macro;
	var employeeType;
	var requesterName;
	var previousGrade;
	var proposedGrade;
	var finalGrade;
	var comments;
	var primaryApproval;
	var delegator;
	var delegatorName;
	var requesterCompetencyGroup;
	var requesterProfessionalCategory;
	var approvalID;
	var markAsRead;
	var lastUpdate;
	var dueDate;
	var checked = false;
	var approvalIndex = 0;

	data.homepage_navigation = false;
	data.user_navigation_details = {};
	// Video First Time log in
	var gr_video = new GlideRecord('kb_knowledge');
	gr_video.addActiveQuery();
	gr_video.addQuery('short_description', 'Smart User Navigation Tracking ');
	gr_video.query();
	if (gr_video.next()) {
		data.user_navigation_details.article = JSON.parse(gr_video.description);
		var page = 'competency_validation';
		if (!gs.nil(data.user_navigation_details.article[gs.getUser().getName()])){
				if (!gs.nil(data.user_navigation_details.article[gs.getUser().getName()][page])) {
					if (!data.user_navigation_details.article[gs.getUser().getName()][page]) {
						data.homepage_navigation = true;
					}
				}
				data.user_navigation_details.article[gs.getUser().getName()][page] = true;
				data.user_navigation_details.articleID = gr_video.getUniqueValue();
		}
	}

	//creates an instance of the table asmt_assessment_instance
	var gr_change_request = new GlideRecordSecure('sc_request');

	//TODO preencher lista de delegators com os users de gestão

	//queries the table
	gr_change_request.addActiveQuery();
	gr_change_request.addEncodedQuery('u_state=pending');
	gr_change_request.orderBy('u_competency');
	gr_change_request.query();

	//cycles through the table records
	while (gr_change_request.next()) {
		competency = gr_change_request.u_competency.getDisplayValue();
		macro = gr_change_request.u_competency.u_competency_macro_type.getDisplayValue();
		requesterName = gr_change_request.u_user.getDisplayValue();
		previousGrade = gr_change_request.u_current_proficiency.getDisplayValue();
		proposedGrade = gr_change_request.u_proposed_proficiency.getDisplayValue();
		finalGrade = gr_change_request.u_final_proficiency.getDisplayValue();
		comments = gr_change_request.u_comments.getDisplayValue();
		primaryApproval = gr_change_request.getValue('u_primary_approval');
		delegator = gr_change_request.getValue('u_delegate_to');
		delegatorName = gr_change_request.u_delegate_to.getDisplayValue();
		approvalID = gr_change_request.getUniqueValue();
		lastUpdate = gr_change_request.u_last_approval_date.getDisplayValue();
		dueDate = gr_change_request.u_days_left_to_complete_task.getDisplayValue();
		requesterCompetencyGroup = gr_change_request.u_user.u_ref_competency_group.getDisplayValue();
		requesterProfessionalCategory = gr_change_request.u_user.u_ref_professional_category.getDisplayValue()

		if (gs.getUser().getUserByID(gr_change_request.u_user.user_name).isMemberOf("Management")) {
			employeeType = "management";
		} else if (gs.getUser().getUserByID(gr_change_request.u_user.user_name).isMemberOf("Staff")) {
			employeeType = "staff";
		}

		//this makes sure that only your approvals are built into the data structure
		if (primaryApproval === data.myUserSysID || delegator === data.myUserSysID) {

			var approvalObj = {u_requester_name: requesterName,
												 label: competency,
												 macro: macro,
												 employeeType: employeeType,
											   u_requester_competency_group: requesterCompetencyGroup,
											   u_requester_professional_category: requesterProfessionalCategory,
											   u_current_proficiency: previousGrade,
											   u_proposed_proficiency: proposedGrade,
											   u_final_proficiency: finalGrade,
											   u_comments: comments,
											   u_newComments: '',
											   u_primary_approval: primaryApproval,
											   u_delegate_to: delegator || '',
												 u_delegator_name: delegatorName,
											   u_current_delegate_to: delegator || '',
											   sys_id: approvalID,
												 u_last_update: lastUpdate,
												 u_due_date: dueDate,
												 checked: checked,
												 index: approvalIndex
												}

			var lastCompIndex = data.competenciesList.length-1;
			if (lastCompIndex === -1 || data.competenciesList[lastCompIndex].label !== competency) {
				var compObj = {label: competency,
											 urgentsCounter: 0,
											 totalApprovals: 0
											}
				lastCompIndex++;
				data.competenciesList.push(compObj)
			}
			data.competenciesList[lastCompIndex].totalApprovals++;

			approvalObj.listIndex = lastCompIndex;

			data.myApprovals.push(approvalObj)
			data.viewAllInfo.totalApprovals++;
			approvalIndex++;


			if (dueDate <= 1) {
				data.competenciesList[lastCompIndex].urgentsCounter++;
				data.viewAllInfo.urgentsCounter++;
			}
		}
	}

	data.delegateTo = [];
	var gr_groupMember = new GlideRecord('sys_user_grmember');
	gr_groupMember.addQuery('group.name', "Management");
	gr_groupMember.orderBy('user')
	//gr_groupMember.addEncodedQuery('group.name=Validators^ORgroup.name=Management');

	gr_groupMember.query();
	while(gr_groupMember.next()){
		data.delegateTo.push({
			"name": gr_groupMember.user.getDisplayValue(),
			"sys_id": gr_groupMember.user.toString()
		})
	}

	var parser = new JSONParser();
	var gr_scales = new GlideRecord('kb_knowledge');
	gr_scales.addQuery('category.name', 'Competencies Scales');
	gr_scales.addQuery('short_description', 'Competencies Scales Description');
	gr_scales.query();
	if (gr_scales.next()) {
		data.competencyScale = JSON.parse(gr_scales.description)
	}
	console.log(data);

})();
