function controller($rootScope, $scope, $timeout, $http, $uibModal) {
	/* widget controller */
	$scope.currentMacroIndex = '';

	var c = this;
	if ($scope.data.homepage_navigation){
		$('#myModal').modal('show');
		var articleID = $scope.data.user_navigation_details.articleID;
		var article_description = JSON.stringify($scope.data.user_navigation_details.article);
		var updateArticle = {};
		updateArticle.sys_id = articleID;
		updateArticle.description = article_description;

		/*$http({
					method:'PUT',
					url: 'api/now/table/kb_knowledge/' + articleID,
					headers: {'Content-Type' : 'application/json'},
					data : updateArticle
		});*/

		$http({
			method: 'PUT',
			url: $scope.data.instanceURL + 'api/dnsa/update_user_navigation/update',
			headers: { 'Content-Type': 'application/json' },
			data: updateArticle
		});
	}else{
		$('#vidNav').remove();
	}

	$scope.stopVideo = function (vide){
		$('#'+vide).remove();
	};

	$(document).keyup(function(e) {
     if (e.keyCode == 27) {
        $('#vidHome').attr('src', $('#vidHome').attr('src'));
    }
	});

	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      	$('#vidNav').remove();
  }

	//event broadcasters when the page is loaded, after waiting 500ms
	$timeout(function () {
		$rootScope.$broadcast('noFilter');
		$rootScope.$broadcast('sendDataStructure', $scope.data);
	}, 500);

	//function that removes the macro filter
	$scope.noFilter = function () {
		var ul = document.getElementById("searchResults");
		ul.style.display = "none";
		$scope.currentMacroIndex = '';
		$rootScope.$broadcast('noFilter');
	};

	$scope.loadingFilter = false;
	//function that sets the selectedMacro when a tab is clicked and broadcasts it to the rootscope
	$scope.selectedMacro = function (macroName) {
		var ul = document.getElementById("searchResults");
		ul.style.display = "none";

		if($scope.currentMacroIndex === $scope.data.indexes[macroName].index){
			return;
		}
		$scope.currentMacroIndex = $scope.data.indexes[macroName].index;
		$scope.loadingFilter = true;
		$timeout(function () {
			$scope.loadingFilter = false;
		}, 1500);

		$rootScope.$broadcast('showCategories', $scope.currentMacroIndex);
	};

	$rootScope.$on('saveButtonClick', function (event, data) {
		$('#waitingModal').modal("show");
		$scope.save();
	});

	$rootScope.$on('submitAllButtonClick', function (event, data) {
		$('#waitingModal').modal("show");
		$scope.submit();
	});

	$rootScope.$on('selectMacro', function (event, data) {
		$scope.currentMacroIndex = data;
	});

	//Update questions on DB
	$scope.save = function () {
		var saveJson = '';

		for (var i in $scope.data.competenciesList) {
			var compObj = $scope.data.competenciesList[i];
			var accessedCompetency = {};


			if (compObj.macro !== compObj.cat) { //hierarchy 3
				var macroIndex = $scope.data.indexes[compObj.macro].index;
				var catIndex = $scope.data.indexes[compObj.macro][compObj.cat].index;
				var subIndex = $scope.data.indexes[compObj.macro][compObj.cat][compObj.sub].index;
				var compIndex = $scope.data.indexes[compObj.macro][compObj.cat][compObj.sub][compObj.name].index;
				accessedCompetency = $scope.data.competencies[macroIndex].cats[catIndex].subs[subIndex].comps[compIndex];
			} else if (compObj.macro === compObj.cat && compObj.cat !== compObj.sub) { //hierarchy 2
				var macroIndex = $scope.data.indexes[compObj.macro].index;
				var subIndex = $scope.data.indexes[compObj.macro][compObj.sub].index;
				var compIndex = $scope.data.indexes[compObj.macro][compObj.sub][compObj.name].index;
				accessedCompetency = $scope.data.competencies[macroIndex].subs[subIndex].comps[compIndex];
			} else { //hierarchy 1
				var macroIndex = $scope.data.indexes[compObj.macro].index;
				var compIndex = $scope.data.indexes[compObj.macro][compObj.name].index;
				accessedCompetency = $scope.data.competencies[macroIndex].comps[compIndex];
			}

			if ((accessedCompetency.approvedValue !== accessedCompetency.value) &&
				(accessedCompetency.approvalState !== 'added') && (accessedCompetency.approvalState !== 'new')) {
				if (saveJson !== '') {
					saveJson += ';';
				}
				var editedCompetencyJson = '{"competencyID": "' + accessedCompetency.sys_id + '", "value": "' + accessedCompetency.value + '", "u_state": "wip"}';
				saveJson += editedCompetencyJson;
			}
		}

		for (var j in $scope.data.newCompetencies) {
			var newCompetency = $scope.data.newCompetencies[j];
			var newCompetencyJson = ';{"competencyID": "' + newCompetency.sys_id + '", "value": "' + newCompetency.value + '", "u_state": "wip"}';
			saveJson += newCompetencyJson;
		}

		var instanceState = ';{"instanceID": "' + $scope.data.instanceInfo.sys_id + '", "state": "wip", "percent_answered": "50"}';
		saveJson += instanceState;

		$scope.httpPut(saveJson, 0);

		$timeout(function(){
			$('#waitingModal').modal('hide');
		},2000);
	};

	$scope.submit = function () {
			var saveJson = '';

		//console.log("teste: " + $scope.data.competenciesList);
		//console.log("teste: ");
		//console.log($scope.data.competencies);

			for (var i in $scope.data.competenciesList) {
				var compObj = $scope.data.competenciesList[i];
				var accessedCompetency = {};

				if (compObj.macro !== compObj.cat) { //hierarchy 3
					var macroIndex = $scope.data.indexes[compObj.macro].index;
					var catIndex = $scope.data.indexes[compObj.macro][compObj.cat].index;
					var subIndex = $scope.data.indexes[compObj.macro][compObj.cat][compObj.sub].index;
					var compIndex = $scope.data.indexes[compObj.macro][compObj.cat][compObj.sub][compObj.name].index;
					accessedCompetency = $scope.data.competencies[macroIndex].cats[catIndex].subs[subIndex].comps[compIndex];
				} else if (compObj.macro === compObj.cat && compObj.cat !== compObj.sub) { //hierarchy 2
					var macroIndex = $scope.data.indexes[compObj.macro].index;
					var subIndex = $scope.data.indexes[compObj.macro][compObj.sub].index;
					var compIndex = $scope.data.indexes[compObj.macro][compObj.sub][compObj.name].index;
					accessedCompetency = $scope.data.competencies[macroIndex].subs[subIndex].comps[compIndex];
				} else { //hierarchy 1
					var macroIndex = $scope.data.indexes[compObj.macro].index;
					var compIndex = $scope.data.indexes[compObj.macro][compObj.name].index;
					accessedCompetency = $scope.data.competencies[macroIndex].comps[compIndex];
				}

				if ((accessedCompetency.approvedValue !== accessedCompetency.value) &&
					(accessedCompetency.approvalState !== 'added') && (accessedCompetency.approvalState !== 'new')) {
					if (saveJson !== '') {
						saveJson += ';';
					}
					var editedCompetencyJson = '{"competencyID": "' + accessedCompetency.sys_id + '", "value": "' + accessedCompetency.value + '", "u_state": "complete"}';
					saveJson += editedCompetencyJson;
				}
			}

			for (var j in $scope.data.newCompetencies) {
				var newCompetency = $scope.data.newCompetencies[j];
				var newCompetencyJson = ';{"competencyID": "' + newCompetency.sys_id + '", "value": "' + newCompetency.value + '", "u_state": "complete"}';
				saveJson += newCompetencyJson;
			}

			var instanceState = ';{"instanceID": "' + $scope.data.instanceInfo.sys_id + '", "state": "wip", "percent_answered": "100"}';
			saveJson += instanceState;
			$scope.httpPut(saveJson, 2000);
		}

	$scope.httpPut = function (competenciesJSON, waitingTime) {
		$http({
			method: 'PUT',
			url: $scope.data.instanceURL + 'api/dnsa/update_competencies/update',
			headers: { 'Content-Type': 'application/json' },
			data: competenciesJSON
		})
			.success(function (response, status, headers, config) {
				console.log(response);
				if (waitingTime !== 0) {
					setTimeout(function () {
						window.location.reload()
					}, waitingTime)
				}
			})
		.finally(function() {
	//		$('#waitingModal').modal('hide');
		});
	}
}

//# sourceURL=compTop.js
