(function() {
	var macro;
	var cat;
	var sub;
	var comp;
	var value;
	var approvedValue;
	var approvalState;
	var compID;
	var updated;
	var compIndex;
	var numOfHierarchies;
	var lastMacroIndex;
	var lastCatIndex;
	var lastSubIndex;
	var lastCompIndex;

	var userT = gs.getUser();
	data.userInfo = {isStaff : userT.isMemberOf("Staff"),
						 competenciesState : userT.source,
						 isValidator : userT.isMemberOf("Validators"),
						 isManagement: userT.isMemberOf("Management"),
						 isPeopleConnect: userT.isMemberOf("People Connect"),
						 isHeadOfPeopleConnect: userT.isMemberOf("Head of People Connect")
						 };

	data.homepage_navigation = false;
	data.user_navigation_details = {};
	// Video First Time log in
	var gr_video = new GlideRecord('kb_knowledge');
	gr_video.addActiveQuery();
	gr_video.addQuery('short_description', 'Smart User Navigation Tracking ');
	gr_video.query();
	if (gr_video.next()) {
		data.user_navigation_details.article = JSON.parse(gr_video.description);
		var page = 'competency_onboarding';
		if (!gs.nil(data.user_navigation_details.article[gs.getUser().getName()])){
				if (!gs.nil(data.user_navigation_details.article[gs.getUser().getName()][page])) {
					if (!data.user_navigation_details.article[gs.getUser().getName()][page]) {
						data.homepage_navigation = true;
					}
				}
				data.user_navigation_details.article[gs.getUser().getName()][page] = true;
				data.user_navigation_details.articleID = gr_video.getUniqueValue();
		}
	}

	//creates an instance of the table asmt_assessment_instance
	var gr_instance = new GlideRecordSecure('asmt_assessment_instance');

	//current user info
	var user = gs.getUser();
	if (user.isMemberOf("Management")) {
		data.userEmployeeType = 'management';
	} else {
		data.userEmployeeType = 'staff';
	}

	//queries the table
	gr_instance.addActiveQuery();
	gr_instance.addQuery('user', gs.getUserID())
	gr_instance.addEncodedQuery('state=ready^ORstate=wip');
	gr_instance.addEncodedQuery('percent_answered!=100');
	gr_instance.orderByDesc('updated');
	gr_instance.query();

	data.instanceURL = gs.getProperty('glide.servlet.uri');
	console.log(data.instanceURL);
	data.competenciesList = []; //used for the search bar
	data.competencies = []; //array with every competency
	data.numOfCompetencies = 0;
	data.answersCounter = 0;
	data.startPage = {macro: 0,
										cat: 0,
										sub: 0
									 }

	// ludbarros init
	data.instanceInfo = {};
	// ludbarros end

	//cycles through the table records
	if (gr_instance.next()) {
		// ludbarros init
		data.instanceInfo.sys_id = gr_instance.getUniqueValue();
		data.instanceInfo.state = gr_instance.getValue('state');
		// lubarros end
		//creates an instance of the table asmt_assessment_instance_question
		var gr_question = GlideRecordSecure('asmt_assessment_instance_question');
		gr_question.addActiveQuery();
		gr_question.addQuery('instance', gr_instance.getUniqueValue());
		gr_question.orderBy('u_macro_category');
		gr_question.orderBy('u_survey_category');
		gr_question.orderBy('u_survey_sub_category');
		gr_question.orderBy('metric.name');
		gr_question.query();

		//cycles through the table records
		while (gr_question.next()) {
			// macro category order
			var macro_order = gr_question.u_macro_category.getDisplayValue();

			//builds the competencies tree data structure
			macro = macro_order.split(':#:')[1];
			cat = gr_question.u_survey_category.getDisplayValue();
			sub = gr_question.u_survey_sub_category.getDisplayValue();
			comp = gr_question.metric.getDisplayValue();
			value = gr_question.value.getDisplayValue();
			approvedValue = gr_question.string_value.getDisplayValue();
			approvalState = gr_question.reference_id.getDisplayValue();
			compID = gr_question.getUniqueValue();
			updated = gr_question.sys_updated_on.getDisplayValue();

			if (!data.hasPending && approvalState === 'requested') {
				data.hasPending = true;
			} else if (!data.hasApproved && approvalState === 'approved') {
				data.hasApproved = true;
			}

			//builds the array of competencies
			var competency = {}
			competency.name = comp;
			competency.macro = macro;
			competency.cat = cat;
			competency.sub = sub;
			competency.value = value;
			competency.approvedValue = approvedValue;
			competency.approvalState = approvalState;
			competency.sys_id = compID;
			competency.updated = updated;
			competency.edited = false;

			if (macro !== cat) {
				numOfHierarchies = 3
				competency.searchTerm = ''+comp+'|'+macro+'|'+cat+'|'+sub;
				competency.searchTree = ''+macro+' > '+cat+' > '+sub;
			} else if (macro === cat && cat !== sub) {
				numOfHierarchies = 2
				competency.searchTerm = ''+comp+'|'+macro+'|'+sub;
				competency.searchTree = ''+macro+' > '+sub;
			} else {
				numOfHierarchies = 1
				competency.searchTerm = ''+comp+'|'+macro;
				competency.searchTree = ''+macro;
			}

			data.competenciesList.push(competency);

			//builds the hierarchical structure of competencies
			compIndex = data.competenciesList.length - 1;

			var compObj = {name: comp,
										 value: value,
										 approvedValue: approvedValue,
										 approvalState: approvalState,
										 sys_id: compID,
										 updated: updated,
										 compIndex: compIndex
										}

			if (numOfHierarchies === 3) {
				lastMacroIndex = data.competencies.length-1;
				if (lastMacroIndex === -1 || data.competencies[lastMacroIndex].name !== macro) {
					var macroObj = {name: macro,
													cats: [],
													newCounter: 0,
													numOfHierarchies: 3
												 }
					lastMacroIndex++;
					data.competencies.push(macroObj);
				}

				lastCatIndex = data.competencies[lastMacroIndex].cats.length-1;
				if (lastCatIndex === -1 || data.competencies[lastMacroIndex].cats[lastCatIndex].name !== cat) {
					var catObj = {name: cat,
												subs: []
											 }
					lastCatIndex++;
					data.competencies[lastMacroIndex].cats.push(catObj);
				}

				lastSubIndex = data.competencies[lastMacroIndex].cats[lastCatIndex].subs.length-1;
				if (lastSubIndex === -1 || data.competencies[lastMacroIndex].cats[lastCatIndex].subs[lastSubIndex].name !== sub) {
					var subObj = {name: sub,
												comps: []
											 }
					lastSubIndex++;
					data.competencies[lastMacroIndex].cats[lastCatIndex].subs.push(subObj);
				}

				data.competencies[lastMacroIndex].cats[lastCatIndex].subs[lastSubIndex].comps.push(compObj);
				lastCompIndex = data.competencies[lastMacroIndex].cats[lastCatIndex].subs[lastSubIndex].comps.length-1;

			} else if (numOfHierarchies === 2) {
				lastMacroIndex = data.competencies.length-1;
				if (lastMacroIndex === -1 || data.competencies[lastMacroIndex].name !== macro) {
					var macroObj = {name: macro,
													subs: [],
													newCounter: 0,
													numOfHierarchies: 2
												 }
					lastMacroIndex++;
					data.competencies.push(macroObj);
				}

				lastSubIndex = data.competencies[lastMacroIndex].subs.length-1;
				if (lastSubIndex === -1 || data.competencies[lastMacroIndex].subs[lastSubIndex].name !== sub) {
					var subObj = {name: sub,
												comps: []
											 }
					lastSubIndex++;
					data.competencies[lastMacroIndex].subs.push(subObj);
				}

				lastCatIndex = 0;
				data.competencies[lastMacroIndex].subs[lastSubIndex].comps.push(compObj)
				lastCompIndex = data.competencies[lastMacroIndex].subs[lastSubIndex].comps.length-1;

			} else { //numOfHierarchies === 1
				lastMacroIndex = data.competencies.length-1;
				if (lastMacroIndex === -1 || data.competencies[lastMacroIndex].name !== macro) {
					var macroObj = {name: macro,
													comps: [],
													newCounter: 0,
													numOfHierarchies: 1
												 }
					lastMacroIndex++;
					data.competencies.push(macroObj)
				}

				lastCatIndex = 0;
				lastSubIndex = 0;
				data.competencies[lastMacroIndex].comps.push(compObj);
				lastCompIndex = data.competencies[lastMacroIndex].comps.length-1;

			}
			if (value !== '') {
				data.answersCounter++;
				data.startPage.macro = lastMacroIndex;
				data.startPage.cat = lastCatIndex;
				data.startPage.sub = lastSubIndex;
			}
			data.numOfCompetencies++;
		}
	}
	var parser = new JSONParser();
	var gr_scales = new GlideRecord('kb_knowledge');
	gr_scales.addQuery('category.name', 'Competencies Scales');
	gr_scales.addQuery('short_description', 'Competencies Scales Description');
	gr_scales.query();
	if (gr_scales.next()) {
		data.competencyScale = JSON.parse(gr_scales.description)
	}
	console.log(data);
})();
