function($rootScope,$scope,$timeout,$http,$uibModal) {
  /* widget controller */
  var c = this;
	var compValue;
	var Json;

	if ($scope.data.homepage_navigation){
		$('#myModal').modal('show');
		var articleID = $scope.data.user_navigation_details.articleID;
		var article_description = JSON.stringify($scope.data.user_navigation_details.article);
		var updateArticle = {};
		updateArticle.sys_id = articleID;
		updateArticle.description = article_description;

		/*$http({
					method:'PUT',
					url: 'api/now/table/kb_knowledge/' + articleID,
					headers: {'Content-Type' : 'application/json'},
					data : updateArticle
		});*/

		$http({
			method: 'PUT',
			url: $scope.data.instanceURL + 'api/dnsa/update_user_navigation/update',
			headers: { 'Content-Type': 'application/json' },
			data: updateArticle
		});
	}else{
		$('#vidNav').remove();
	}

	$scope.stopVideo = function (vide){
		$('#'+vide).remove();
	}

	$(document).keyup(function(e) {
     if (e.keyCode == 27) {
        $('#vidHome').attr('src', $('#vidHome').attr('src'));
    }
	});

	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
  	$('#vidNav').remove();
  }

	$scope.currentMacroIndex = $scope.data.startPage.macro;
	$scope.currentCatIndex = $scope.data.startPage.cat;
	$scope.currentSubIndex = $scope.data.startPage.sub;
	$scope.submitEnabled = false;

	//event broadcasters when the page is loaded, after waiting 500ms
	$timeout(function(){
		$rootScope.$broadcast('sendDataStructure', [$scope.data, $scope.currentMacroIndex, $scope.currentCatIndex, $scope.currentSubIndex])
		$('body').tooltip({
        selector: '[data-toggle="tooltip"]',
				trigger : 'hover'
    });
	},500);

	$rootScope.$on('incrementSub', function(event, data) {
		$scope.currentSubIndex++;
	});

	$rootScope.$on('decrementSub', function(event, data) {
		$scope.currentSubIndex--;
	});

	$rootScope.$on('incrementMacro', function(event, data) {
		$scope.currentMacroIndex++;
	});

	$rootScope.$on('decrementMacro', function(event, data) {
		$scope.currentMacroIndex--;
	});

	$rootScope.$on('activateSubmit', function(event, data) {
		$scope.submitEnabled = true;
	});

	$rootScope.$on('saveOnboarding', function(event, data) {
		$('#waitingModal').modal("show");
		$scope.save();
	});

	$rootScope.$on('submitOnboarding', function(event, data) {
		$('#waitingModal').modal("show");
		$scope.submit();
	});


	/** Save answers on DB (without submitting them)
	* The 'approvedValue' property is the String Value field on the database.
	* In the onboarding process, it's used as the ng-model in the workspace,
	* therefore it's changes have to be persisted in the DB.
	* When saving, the 'value' property is only used used to know where we left off,
	* in case we save, exit the page and come back later. It starts as an empty string and
	* everytime the Next button is clicked, that page's competencies 'value' property are updated
	* to the selected value on the radio button (0 as default)
	*/
	$scope.save = function() {
		//c.openModal();
		//$("body").css("pointer-events", "none");
		Json = '';

		for (var i in $scope.data.competencies) {
			var macro = $scope.data.competencies[i];
			if (macro.numOfHierarchies === 3) {
				for (var j in macro.cats) {
					var cat = macro.cats[j];
					for (var k in cat.subs) {
						var sub = cat.subs[k];
						for (var l in sub.comps) {
							var comp = sub.comps[l]
							if ((comp.value !== '' && comp.value != comp.approvedValue) || (comp.value === '' && comp.approvedValue != 0)) {
								compValue = comp.approvedValue;
								$scope.buildJson(comp.sys_id, compValue, comp.approvedValue);
							} else if (comp.value !== '') {
								compValue = comp.value;
								$scope.buildJson(comp.sys_id, compValue, comp.approvedValue);
							}
						}
					}
				}
			} else if (macro.numOfHierarchies === 2) {
				for (var j in macro.subs) {
					var sub = macro.subs[j];
					for (var k in sub.comps) {
						var comp = sub.comps[k];
						if ((comp.value !== '' && comp.value != comp.approvedValue) || (comp.value === '' && comp.approvedValue != 0)) {
							compValue = comp.approvedValue;
							$scope.buildJson(comp.sys_id, compValue, comp.approvedValue);
						} else if (comp.value !== '') {
							compValue = comp.value;
							$scope.buildJson(comp.sys_id, compValue, comp.approvedValue);
						}
					}
				}
			} else { //macro.numOfHierarchies === 1
				for (var j in macro.comps) {
					var comp = macro.comps[j];
					if ((comp.value !== '' && comp.value != comp.approvedValue) || (comp.value === '' && comp.approvedValue != 0)) {
						compValue = comp.approvedValue;
						$scope.buildJson(comp.sys_id, compValue, comp.approvedValue);
					} else if (comp.value !== '') {
						compValue = comp.value;
						$scope.buildJson(comp.sys_id, compValue, comp.approvedValue);
					}
				}
			}
		}
		var instanceState = ';{"instanceID": "' + $scope.data.instanceInfo.sys_id + '", "state": "' + 'wip' + '", "percent_answered": "' + '50' + '"}';
		Json += instanceState;
		console.log(Json);
		$scope.httpPutSave(Json, 0);

		$timeout(function(){
			$('#waitingModal').modal('hide');
		},2000);
	}

	/** Submit answers (finish the onboarding process)
	 * The user shall never come to this page again after submitting. Therefore, there's no need
	 * to know where he left off.
	 * The 'approvedValue' property is set to 0 to every competency since
	 * it's used in the competencies logic to know if a competency is approved.
	 * Before that, all the selected values in the radio buttons (ng-model: approvedValue)
	 * are copied into the 'value' property as the submitted values.
	 */
	$scope.submit = function() {
		//c.openModal();
		//$("body").css("pointer-events", "none");
		Json = '';

		for (var i in $scope.data.competencies) {
			var macro = $scope.data.competencies[i];
			if (macro.numOfHierarchies === 3) {
				for (var j in macro.cats) {
					var cat = macro.cats[j];
					for (var k in cat.subs) {
						var sub = cat.subs[k];
						for (var l in sub.comps) {
							var comp = sub.comps[l]
							if (comp.value != comp.approvedValue) {
								compValue = comp.approvedValue;
								$scope.buildJson(comp.sys_id, compValue, 0);
							} else {
								compValue = comp.value;
								$scope.buildJson(comp.sys_id, compValue, 0);
							}
						}
					}
				}
			} else if (macro.numOfHierarchies === 2) {
				for (var j in macro.subs) {
					var sub = macro.subs[j];
					for (var k in sub.comps) {
						var comp = sub.comps[k];
						if (comp.value != comp.approvedValue) {
							compValue = comp.approvedValue;
							$scope.buildJson(comp.sys_id, compValue, 0);
						} else {
							compValue = comp.value;
							$scope.buildJson(comp.sys_id, compValue, 0);
						}
					}
				}
			} else { //macro.numOfHierarchies === 1
				for (var j in macro.comps) {
					var comp = macro.comps[j];
					if (comp.value != comp.approvedValue) {
						compValue = comp.approvedValue;
						$scope.buildJson(comp.sys_id, compValue, 0);
					} else {
						compValue = comp.value;
						$scope.buildJson(comp.sys_id, compValue, 0);
					}
				}
			}
		}
		var instanceState = ';{"instanceID": "' + $scope.data.instanceInfo.sys_id + '", "state": "' + 'wip' + '", "percent_answered": "' + '100' + '"}';
		Json += instanceState;
		console.log(Json);
		$scope.httpPutSubmit(Json, 35000);
	}

	$scope.buildJson = function (compSys_id, compValue, compApprovedValue) {
		if (Json !== '') {
			Json += ';';
		}
		var editedCompetencyJson = '{"competencyID": "' + compSys_id + '", "value": "' + compValue + '", "string_value": "' + compApprovedValue + '", "u_state": "' + 'wip' + '"}';
		Json += editedCompetencyJson;
	}

	$scope.httpPutSave = function(competenciesJSON, waitingTime) {
		$http({
				method:'PUT',
				url: $scope.data.instanceURL + 'api/dnsa/update_competencies/update',
				headers: {'Content-Type' : 'application/json'},
				data : competenciesJSON
			})
			.success(function(response, status, headers, config){
			console.log(response);
			})
		.finally(function() {
			c.modalInstance.close();
			$("body").css("pointer-events", "auto");
		});
	}

	$scope.httpPutSubmit = function(competenciesJSON, waitingTime) {
		$http({
				method:'PUT',
				url: $scope.data.instanceURL + 'api/dnsa/update_competencies/update',
				headers: {'Content-Type' : 'application/json'},
				data : competenciesJSON
			})
			.success(function(response, status, headers, config){
			console.log(response);
			setTimeout(function() {
						window.location.replace("/sp?id=%5Bu%5Dclone_comp")
					}, waitingTime)
			})
		.finally(function() {
			c.modalInstance.close();
			$("body").css("pointer-events", "auto");
		});
	}

	c.openModal = function() {
		c.modalInstance = $uibModal.open({
			templateUrl: 'modalTemplate',
			scope: $scope,
			appendTo: $("body")
		});

		c.modalInstance.rendered.then(function(){
			console.log("Opened");
			$("body > div.modal").css("z-index",9999);
		});
	}
}

//# sourceURL=onboardingTop.js
