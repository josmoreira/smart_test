function controller($scope, $rootScope, $http, $timeout, $uibModal) {

		$timeout(function() {
	    $('body').tooltip({
	      selector: '[data-toggle="tooltip"]',
				trigger : 'hover'
	    });
	  }, 500);

    function isMobile() {
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            return true;
        }
        return false;
    }

    function stringWithContent(str) {
        return str !== undefined && str.length > 0;
    }

	  if (isMobile())
			$(".pieTablet").append($("#report_section3").clone());
    $("body").append("<div class='testclass'>Test</div>");
    $(".testclass").css({
        position: "absolute",
        top: 0,
        left: 0,
        width: "100%",
        height: "100%",
        "background-color": "black",
        display: "none"
    });



    //fixing bootstrap imposed stuff

    $("sp-page-row > div.row > div.col-md-8").css("padding-right", "2px");

    //Hovering


    $scope.toggleHover = function(app) {
        app.hovering = !app.hovering;
    };

    //LIMITS

    $scope.mobileApprovalLabelLimit = 20;
    $scope.competencyLabelLimit = 40;

    $scope.mobileApprovalLabelLimitLong = 50;

    //DETAILS STATE

    //currentEmployee = employee;
    //delegateToFilterString = "";
    //currentCompetencyIndex = competency.compIndex;


    $scope.approvalForDetails = undefined;
    var approvalForDetailsCopy;


    //############### DELEGATE MODAL #########################

    $scope.delegateToFilterString = "";
    //$scope.approvalForDetailsChanged(emp, comp);
    $scope.placeholderForDelegatePopover = function() {
        return ($scope.approvalForDetails && $scope.approvalForDetails.u_delegator_name.length > 0) ? $scope.approvalForDetails.u_delegator_name : "Search Here";
    };
    $scope.canSubmitDelegate = function() {
        if (!$scope.data.delegateTo) return false;
        var hasMatch = $scope.data.delegateTo.filter($scope.delToFilter).length == 1;
        var isSame = $scope.delegateToCurrent !== undefined && $scope.delegateToCurrent.sys_id === $scope.approvalForDetails.u_delegate_to;
        return hasMatch && !isSame;
    };
    $scope.popoverSubmitDelegate = function() {

        if (!$scope.canSubmitDelegate()) return;
        if (!$scope.delegateToCurrent) { // if we donw have one, we'll get the first match, it's safe to assume we have at least a match because canSubmitDelegate() asserts that
            $scope.delegateToCurrent = $scope.data.delegateTo.filter($scope.delToFilter)[0];
        }
        $scope.delForCurrentDetailedApprovalChanged($scope.delegateToCurrent);

        $('#desktopDelModal').modal('hide');
        //$('.popover').popover('destroy');

    };
    $scope.delToFilter = function(entry) {

        return $scope.delegateToFilterString === undefined || $scope.delegateToFilterString.length === 0 || entry.name.toLowerCase().indexOf($scope.delegateToFilterString.toLowerCase()) !== -1;
    };

    $scope.delegateToFilterSelected = function(newVal) {
        $scope.delegateToFilterString = newVal.name;
        $scope.delegateToCurrent = newVal;
    };

    //##################### END DELEGATE MODAL ####################


    function checkOrientation() {
        var width = $(window).width();
        var height = $(window).height();

        var tabletPortrait = width < height && isMobile() && width > 414;
        var phoneLandscape = width > height && isMobile() && width <= 736;
        if (tabletPortrait || phoneLandscape) { // tablet portrait
            $(".testclass").css({
                display: "block"
            });
        } else { // landscape
            $(".testclass").css({
                display: "none"
            });
        }
    }

    checkOrientation();
    $(window).on("orientationchange", function(event) {
        console.log("Changed");
        checkOrientation();
    });

    var graphDiv = $(".sp-row-content:last").children().last();
    var rowsDiv = $(".sp-row-content:last").children().first();

    /* widget controller */
    var c = this;

    $scope.showingGraphMobile = false;

    if (isMobile()) { // hide graph
        graphDiv.hide();
    }

    $rootScope.$on("graphToggle", function() {
        $scope.showingGraphMobile = !$scope.showingGraphMobile;
        if ($scope.showingGraphMobile) {
            graphDiv.show();
            rowsDiv.hide();
        } else {
            graphDiv.hide();
            rowsDiv.show();
        }
    });

    $scope.iOS = false;
    $scope.canSubmitComment = false;

    $scope.$watch("currentMessage", function() {
        $scope.canSubmitComment = stringWithContent($scope.currentMessage);
    });

    $scope.approvalForDetailsChanged = function(approval) {


        $scope.approvalForDetails = approval;
        $scope.delegateToFilterString = "";
        $scope.currentMessage = "";

        approvalForDetailsCopy = $.extend(true, {}, approval);
        $scope.commentsForDetailedUser = approval.u_comments.split("||");
        $scope.commentsForDetailedUser = $scope.commentsForDetailedUser.filter(stringWithContent);

        $scope.iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;

    };

    $scope.delForCurrentDetailedApprovalChanged = function(newDelegate) {
        $scope.approvalForDetails.u_final_proficiency = ''; //reset the radio button
        $scope.approvalForDetails.u_delegate_to = newDelegate.sys_id;
        $scope.approvalForDetails.u_delegator_name = newDelegate.name;
        $scope.submitDelegation($scope.approvalForDetails);

        //Need to update deep copy
        approvalForDetailsCopy = $scope.data.myApprovals[$scope.approvalForDetails.index];

    };

    $scope.restoreDelegateToLastSubmitedState = function() {
        //replace by the deep copy
        $scope.data.myApprovals[$scope.approvalForDetails.index] = approvalForDetailsCopy;
        $scope.approvalForDetails = undefined;
        $scope.currentMessage = undefined;
    };


    $scope.viewAll = true;
    $scope.selectedCompetency = '';
    $scope.showDelegated = false;

    //for pagination
    $scope.currentApprovals = [];
		$scope.allApprovals = [];
    $scope.currentPage = 0;
    $scope.pageSize = 9;


    $scope.isDelegating = function(employee) {
        if (!employee.isDelegating) {
            employee.isDelegating = true;
        } else {
            employee.isDelegating = false;
        }
    };

    $scope.allowSubmit = function () {
      $rootScope.$broadcast("allowSubmit");
    };

    $rootScope.$on('viewAll', function(event, data) {
        $scope.selectedCompetency = '';
        $scope.viewAll = true;
        $scope.filterApprovals();
    });

    $rootScope.$on('sendCompetencyLabel', function(event, data) {
        $scope.selectedCompetency = data;
        $scope.viewAll = false;
        $scope.filterApprovals();
    });

    $rootScope.$on('sendDataStructure', function(event, data) {
        $scope.data = data;
				$rootScope.$broadcast("competencyScale",data.competencyScale);
        $scope.filterApprovals();
    });

    $rootScope.$on('toggleDelegated', function(event, data) {
        $scope.removeChecks();
        $scope.currentPage = 0;
        $scope.showDelegated = !$scope.showDelegated;
        $scope.filterApprovals();
    });

    $rootScope.$on('showPieTablet', function(event, data) {
        showPieChartTablet();
    });

    $scope.nextPage = function() {
        $scope.removeChecks();
        $scope.currentPage++;
				$scope.filterApprovals();
    };

    $scope.drawNextButton = function() {
        return ($scope.currentPage < $scope.allApprovals.length / $scope.pageSize - 1);
    };

    $scope.backPage = function() {
        $scope.removeChecks();
        $scope.currentPage--;
				$scope.filterApprovals();
    };

    $scope.removeChecks = function() {
        $("#masterCheckBox").prop("checked", false);
        $rootScope.$broadcast("disableChecks");
        var checkBoxes = document.getElementsByClassName("approvalCheckBox");
        for (i = 0; i < checkBoxes.length; i++) {
            var approvalId = $(checkBoxes[i]).attr("id");
            $scope.data.myApprovals[approvalId].checked = false;
        }
    };


    $rootScope.$on("setChecks", function(event, newState) {
        $("#masterCheckBox").prop("checked", newState);
        $scope.checkAll();
    });

    //dictionary to save each approval delegator
    $scope.delegators = {};

    //open popover
    $scope.openPopover = function() {
        return;
    };

    $scope.checkAll = function() {

        var checkBoxes = document.getElementsByClassName("approvalCheckBox");
        for (i = 0; i < checkBoxes.length; i++) {
            var approvalId = $(checkBoxes[i]).attr("id");
            $scope.data.myApprovals[approvalId].checked = $("#masterCheckBox").prop("checked");
        }

			if($("#masterCheckBox").prop("checked")) {
				$scope.allowSubmit();
			}

    };

    $scope.filterApprovals = function() {
        if(!$scope.data.myApprovals){
          $scope.currentApprovals = [];
					$scope.allApprovals = [];
					return;
        }
        $scope.allApprovals = $scope.data.myApprovals.filter(function(ap) {
						if (ap.u_state === 'approved') return false;
            if (!scope.viewAll && ap.label !== $scope.selectedCompetency) return false;
            if ($scope.showDelegated) {
                return ap.u_primary_approval === $scope.data.myUserSysID && ap.u_current_delegate_to !== '';
            } else {
                return ap.u_primary_approval === $scope.data.myUserSysID && ap.u_current_delegate_to === '' || ap.u_current_delegate_to === $scope.data.myUserSysID;
            }

        });
				$scope.currentApprovals = $scope.allApprovals.filter(function(ap, apIndex) {
					return (apIndex >= $scope.pageSize * $scope.currentPage && apIndex < $scope.pageSize * ($scope.currentPage + 1)) || isMobile();
				});

    };


		$rootScope.$on("submitAll", function() {
			$scope.currentPage = 0;
			$scope.filterApprovals();
		});

    //http request to submit an approval
    $scope.submit = function(approval) {

        $rootScope.$broadcast("showNotification", {message: "Approval Submitted", image:"glyphicon-ok"});
        var submitApproval = {};
        submitApproval.sys_id = approval.sys_id;
        if (approval.u_final_proficiency === '') {
            submitApproval.u_final_proficiency = approval.u_proposed_proficiency;
        } else {
            submitApproval.u_final_proficiency = approval.u_final_proficiency;
        }
        submitApproval.u_state = 'approved';
        approval.u_state = 'approved';

        $scope.data.viewAllInfo.totalApprovals--;
        $scope.data.competenciesList[approval.listIndex].totalApprovals--;

        if (approval.u_due_date <= 1) {
            $scope.data.viewAllInfo.urgentsCounter--;
            $scope.data.competenciesList[approval.listIndex].urgentsCounter--;
        }

        $http({
            method: 'PUT',
            url: 'api/now/table/sc_request/' + submitApproval.sys_id,
            headers: {
                'Content-Type': 'application/json'
            },
            data: submitApproval
        });

				$scope.currentPage = 0;
				$scope.filterApprovals();
    };

    //http request to submit a delegation
    $scope.submitDelegation = function(approval) {
        $rootScope.$broadcast("showNotification", {message: "Approval Delegated", image:"glyphicon-ok"});
        if (approval.u_delegate_to !== '') {
            var approvalObj = {};
            approvalObj.sys_id = approval.sys_id;
            approvalObj.u_delegate_to = approval.u_delegate_to;
            approvalObj.u_delegator_name = approval.u_delegator_name;
            approvalObj.u_previous_delegator = approval.u_current_delegate_to;
            approval.u_current_delegate_to = approval.u_delegate_to;
            $http({
                method: 'PUT',
                url: 'api/now/table/sc_request/' + approvalObj.sys_id,
                headers: {
                    'Content-Type': 'application/json'
                },
                data: approvalObj
            });
            $scope.filterApprovals();
        }
    };


    //assigned the delegator to the approval
    $scope.delegate = function(employee, competency) {
        employee.u_final_proficiency = ''; //reset the radio button
        employee.u_delegate_to = $scope.delegators[competency.label][employee.sys_id].value;
        employee.u_delegator_name = $scope.delegators[competency.label][employee.sys_id].displayValue;
    };

    $scope.submitCommentMobile = function() {
        if (!$scope.canSubmitComment) return;
        $scope.commentsForDetailedUser.push($scope.data.myUserName + ": " + $scope.currentMessage);
        $scope.approvalForDetails.u_comments = $scope.commentsForDetailedUser.join("||");
        $http({
            method: 'PUT',
            url: 'api/now/table/sc_request/' + $scope.approvalForDetails.sys_id,
            headers: {
                'Content-Type': 'application/json'
            },
            data: $scope.approvalForDetails
        });
        $scope.currentMessage = "";
    };

    //open modal window with comments
    c.openModalComments = function(approvalObject) {
        $scope.approvalObject = approvalObject;
        $scope.comments = approvalObject.u_comments.split('||');
        c.modalInstance = $uibModal.open({
            templateUrl: 'modalComments',
            scope: $scope
        });
    };


    //close modal window and save new comment
    c.closeModalAndAdd = function() {
        if (scope.approvalObject.u_newComments !== '') {
            if (scope.approvalObject.u_comments !== '') {
                scope.approvalObject.u_comments += '||' + $scope.data.myUserName + ': ' + scope.approvalObject.u_newComments;
            } else {
                scope.approvalObject.u_comments = $scope.data.myUserName + ': ' + scope.approvalObject.u_newComments;
            }
            $http({
                method: 'PUT',
                url: 'api/now/table/sc_request/' + scope.approvalObject.sys_id,
                headers: {
                    'Content-Type': 'application/json'
                },
                data: scope.approvalObject
            });
            scope.approvalObject.u_newComments = '';
        }
        c.modalInstance.close();
    };

    //close modal window and cancel new comments
    c.cancelModal = function() {
        scope.approvalObject.u_newComments = '';
        c.modalInstance.close();
    };

    $scope.formatDate = function(date) {
        var newDate = Date.parse(date);
        return newDate;
    };


    pieShown = false;



    function stylizePieSideMenu() {

        $(".sideButton").css({
            top: $("header").css('height'),
            left: $(window).width() - 44
        });

        $(".pieTablet").css({
            left: $(window).width() - 10,
            height: "calc("+$(window).height()+"px - "+$("header").css("height")+" - "+$(".bottomBar").css("height")+")",
            top: $("header").css('height')
        });

    }

		$timeout(function(){
			stylizePieSideMenu();
		});

    $(window).on("orientationchange", function(event) {

      stylizePieSideMenu();

    });

    $scope.togglePieChartTablet = function() {
        if (!pieShown) {
            $(".pieTablet").animate({
                "left": $(window).width() - 522
            });
            $(".sideButton").animate({
                "left": $(window).width() - 557
            });
        } else {
            $(".pieTablet").animate({
                "left": $(window).width() - 9
            });
            $(".sideButton").animate({
                "left": $(window).width() - 44
            });
        }
        pieShown = !pieShown;
    };

		$('#commentsModal').on('hidden.bs.modal', function () {
			$rootScope.onComment = false;
		})

		$('#commentsModal').on('shown.bs.modal', function () {
			$rootScope.onComment = true;
		})

}








//show delegate icon when dropdown is visible
$(document).on("click", ".delegateButton", function() {

    for (var idk = 0; idk < jQuery('.hasDelegator').length; idk++) {
        jQuery('delegateButton').toggleClass('hasDelegator');
    }
    jQuery(this).toggleClass('hasDelegator');

});

//# sourceURL=approvalsWorkspace.js
